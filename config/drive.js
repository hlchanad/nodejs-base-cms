"use strict";

const Helpers = use("Helpers");
const Env = use("Env");

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Default disk
  |--------------------------------------------------------------------------
  |
  | The default disk is used when you interact with the file system without
  | defining a disk name
  |
  */
  default: Env.get("STORAGE_DRIVER", "local"),

  disks: {
    /*
    |--------------------------------------------------------------------------
    | Local
    |--------------------------------------------------------------------------
    |
    | Local disk interacts with the a local folder inside your application
    |
    */
    local: {
      driver: "local",
      root: Env.get("STORAGE_LOCAL_ROOT_PRIVATE", Helpers.tmpPath()),
    },

    /*
    |--------------------------------------------------------------------------
    | S3
    |--------------------------------------------------------------------------
    |
    | S3 disk interacts with a bucket on aws s3
    |
    */
    s3: {
      driver: "s3",
      key: Env.get("STORAGE_S3_KEY"),
      secret: Env.get("STORAGE_S3_SECRET"),
      bucket: Env.get("STORAGE_S3_BUCKET"),
      region: Env.get("STORAGE_S3_REGION"),
    },
  },
};
