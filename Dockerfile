FROM node:13.7.0-alpine3.10

WORKDIR /cms

RUN npm i -g @adonisjs/cli

COPY ./package.json ./
RUN npm i

COPY . .

EXPOSE 3333

CMD npm start
