"use strict";

const _ = require("lodash");

const Model = use("App/Base/Models/BaseModel");

class CmsUser extends Model {

  cmsUserRoles() {
    return this.hasMany("App/Base/Models/CmsUserRole");
  }

  cmsUserPreferences() {
    return this.hasMany("App/Base/Models/CmsUserPreference");
  }

  async preferences(keys) {
    if (keys != null && _.isString(keys)) {
      keys = [keys];
    }

    return (await this.cmsUserPreferences().fetch()).rows
      .filter(preference => keys == null || keys.includes(preference.key));
  }

  async cmsRoles() {
    const cmsRoles = await this.cmsUserRoles().fetch();
    return Promise.all(cmsRoles.rows.map(cmsUserRole => cmsUserRole.cmsRole().first()));
  }

  static validationRules(id) {
    return {
      username: "string|required",
      password: `string${id == null ? "|required" : ""}`,
      email: "string|email|required",
      role_ids: "required|databaseRecord:cms_roles",
    };
  }

}

module.exports = CmsUser;
