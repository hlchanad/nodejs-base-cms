"use strict";

const Model = use("App/Base/Models/BaseModel");

class CmsRoleMenu extends Model {

  cmsRole() {
    return this.hasOne("App/Base/Models/CmsRole", "cms_role_id", "id");
  }

  cmsMenu() {
    return this.hasOne("App/Base/Models/CmsMenu", "cms_menu_id", "id");
  }


}

module.exports = CmsRoleMenu;
