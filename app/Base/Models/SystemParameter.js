"use strict";

const _ = require("lodash");

const Model = use("App/Base/Models/BaseModel");

class SystemParameter extends Model {

  static validationRules(id) {
    return {
      key: "string|required",
      value: "string|required",
      description: "string",
    };
  }

  static async getConfig(key, _default) {
    const systemParameter = await this.query()
      .where("key", key)
      .where("is_deleted", false)
      .first();

    return _.get(systemParameter, "value", _default);
  }

}

module.exports = SystemParameter;
