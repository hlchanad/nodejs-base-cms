class SelectOptionGroup {

  constructor(id, title, selectOptions) {
    this.id = id;
    this.title = title;
    this.selectOptions = selectOptions;
  }

  static builder() {
    return new SelectOptionGroupBuilder();
  }

}

class SelectOptionGroupBuilder {

  constructor() {}

  id(id) {
    this._id = id;
    return this;
  }

  title(title) {
    this._title = title;
    return this;
  }

  selectOptions(selectOptions) {
    this._selectOptions = selectOptions;
    return this;
  }

  build() {
    return new SelectOptionGroup(this._id, this._title, this._selectOptions);
  }

}

module.exports = SelectOptionGroup;
