class DatatableColumn {

  constructor(data, title, orderable) {
    this.data = data;
    this.title = title;
    this.orderable = orderable == null ? true : orderable;
  }

}

module.exports = DatatableColumn;
