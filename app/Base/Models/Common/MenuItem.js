class MenuItem {

  constructor(cmsMenu, path, children, active) {
    this.title = cmsMenu.title;
    this.iconType = cmsMenu.icon_type;
    this.icon = cmsMenu.icon;
    this.url = path;
    this.children = children;
    this.active = !!active;
  }

}

module.exports = MenuItem;
