class ApiResponse {

  constructor(status, data) {
    this.status = status;
    this.data = !(data instanceof Error) ? data
      : Object.getOwnPropertyNames(data)
        .reduce((acc, key) => {
          return {
            ...acc,
            [key]: data[key],
          };
        }, {});
  }
}

module.exports = ApiResponse;
