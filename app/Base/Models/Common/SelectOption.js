class SelectOption {

  constructor(id, title, value, disabled = false) {
    this.id = id;
    this.title = title;
    this.value = value;
    this.disabled = disabled;
    this.selected = false;
  }

  static builder() {
    return new SelectOptionBuilder();
  }

}

class SelectOptionBuilder {

  constructor() {}

  id(id) {
    this._id = id;
    return this;
  }

  title(title) {
    this._title = title;
    return this;
  }

  value(value) {
    this._value = value;
    return this;
  }

  disabled(disabled) {
    this._disabled = disabled;
    return this;
  }

  build() {
    return new SelectOption(this._id, this._title, this._value, this._disabled);
  }

}

module.exports = SelectOption;
