
const Env = use('Env');

class Breadcrumb {

  constructor(title, url) {
    this.title = title;
    this.url = url;
  }

}

Breadcrumb.Root = new Breadcrumb('general.home', Env.get('APP_URL'));

module.exports = Breadcrumb;
