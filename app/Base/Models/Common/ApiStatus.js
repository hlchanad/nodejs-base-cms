const I18nHelper = use("App/Base/Helpers/I18nHelper");

class ApiStatus {

  constructor(code, message) {
    this.code = code;
    this.message = message;
  }

  static of(code, message) {
    return new ApiStatus(code, message);
  }

  toMultiLanguage() {
    return ApiStatus.of(this.code, I18nHelper.formatMessage(this.message));
  }

}

// 2xx
ApiStatus.OK = ApiStatus.of("200.0000", "api-status.ok.message");

// 4xx
ApiStatus.BAD_REQUEST = ApiStatus.of("400.0000", "api-status.bad-request.message");
ApiStatus.FORBIDDEN = ApiStatus.of("403.0000", "api-status.forbidden.message");
ApiStatus.NOT_FOUND = ApiStatus.of("404.0000", "api-status.not-found.message");

// 5xx
ApiStatus.INTERNAL_SERVER = ApiStatus.of("500.0000", "api-status.internal-server-error.message");
ApiStatus.DATABASE_ERROR = ApiStatus.of("500.0001", "api-status.database-error.message");

module.exports = ApiStatus;
