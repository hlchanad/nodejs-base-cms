const Env = use("Env");

class Language {

  constructor(localeCode, awsCode, dbCode, title) {
    this.localeCode = localeCode;
    this.awsCode = awsCode;
    this.dbCode = dbCode;
    this.title = title;
  }

  static values() {
    return [
      Language.English,
      Language.HongKong,
      Language.Japanese,
    ];
  }

  static getSupportedLanguages() {
    const languageCodes = Env.get("APP_SUPPORTED_LANGUAGES", Language.English.localeCode);

    const supportedLanguages = languageCodes
      .split(",")
      .map(code => code.trim())
      .map(code => Language.values().find(language => language.localeCode === code))
      .filter(language => language != null);

    return supportedLanguages.length === 0 ? [ Language.English ] : supportedLanguages;
  }

  static of(field, value) {
    return this.getSupportedLanguages()
      .find(language => language[field] === value);
  }

  equals(another) {
    return this.localeCode === another.localeCode;
  }

}

Language.English = new Language('en', 'en', 'en-US', 'languages.en-us');
Language.Japanese = new Language('ja', 'ja', 'ja-JP', 'languages.ja-jp');
Language.HongKong = new Language('hk', 'zh-TW', 'zh-HK', 'languages.zh-hk');

module.exports = Language;
