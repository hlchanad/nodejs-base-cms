const _ = require("lodash");

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");
const Database = use("Database");

const Status = use("App/Base/Constants/Status");
const Language = use("App/Base/Models/Common/Language");

class BaseModel extends Model {

  static boot() {
    super.boot();
    this.addHook("beforeCreate", "App/Base/Models/Hooks/BaseModelHook.beforeCreate");
    this.addHook("beforeUpdate", "App/Base/Models/Hooks/BaseModelHook.beforeUpdate");
    this.addHook("beforeDelete", "App/Base/Models/Hooks/BaseModelHook.beforeDelete");
    this.addHook("afterFind", "App/Base/Models/Hooks/BaseModelHook.afterFind");
    this.addHook("afterFetch", "App/Base/Models/Hooks/BaseModelHook.afterFetch");
  }

  static async get(id) {
    return this.query()
      .where("id", id)
      .where("is_visible", true)
      .where("is_deleted", false)
      .first();
  }

  static async getWithMutator(id) {
    const model = await this.get(id);

    if (model == null) return null;

    const getCmsUserName = async (cmsUserId) => {
      const cmsUser = await Database
        .from("cms_users")
        .select("username")
        .where("id", cmsUserId)
        .first();

      return cmsUser == null ? null : cmsUser.username;
    };

    model.created_by_name = await getCmsUserName(model.created_by);
    model.updated_by_name = await getCmsUserName(model.updated_by);

    return model;
  }

  static async getAll() {
    return this.query()
      .where("is_visible", true)
      .where("is_deleted", false)
      .fetch();
  }

  async save(user) {

    if (this.id == null) {
      this.status = _.get(this, "status", Status.NORMAL.key);
      this.is_visible = _.get(this, "is_visible", true);
      this.is_deleted = _.get(this, "is_deleted", false);
      this.created_by = _.get(user, "id", 1);
    }

    this.updated_by = _.get(user, "id", 1);

    return super.save();
  }

  async delete(user, force) {
    if (force) return super.delete();

    await this.constructor.$hooks.before.exec("delete", this);

    this.is_deleted = true;
    this.deleted_by = _.get(user, "id", 1);
    this.deleted_at = new Date();

    const affected = await this.constructor.query()
      .where(this.constructor.primaryKey, this.primaryKeyValue)
      .ignoreScopes()
      .update(this);

    await this.constructor.$hooks.after.exec("delete", this);

    return !!affected;
  }

  static async batchDelete(ids, user, force) {
    const query = this.query()
      .whereIn(this.primaryKey, ids)
      .ignoreScopes();

    const affected = !force
      ? await query.update({
        is_deleted: true,
        deleted_by: _.get(user, "id", 1),
        deleted_at: new Date(),
      })
      : await query.delete();

    return affected === ids.length;
  }

  hasOne(relatedModel, primaryKey, foreignKey, defaultFilters = true) {
    let relation = super.hasOne(relatedModel, primaryKey, foreignKey);

    if (defaultFilters) {
      relation = relation
        .where("is_deleted", false)
        .where("is_visible", true);
    }

    return relation;
  }

  hasMany(relatedModel, primaryKey, foreignKey, defaultFilters = true) {
    let relation = super.hasMany(relatedModel, primaryKey, foreignKey);

    if (defaultFilters) {
      relation = relation
        .where("is_deleted", false)
        .where("is_visible", true);
    }

    return relation;
  }

  async detail(language) {
    if (typeof this.details !== "function") {
      return null;
    } else {
      return await this.details()
        .where("language", language)
        .first();
    }
  }

  async getDetailsMap() {
    if (typeof this.details !== "function") {
      return {};
    } else {
      return (await this.details().fetch()).rows
        .filter(detail => Language.getSupportedLanguages()
          .map(language => language.dbCode)
          .includes(detail.language))
        .reduce((acc, detail) => {
          acc[detail.language] = detail;
          return acc;
        }, {});
    }
  }

}

module.exports = BaseModel;
