"use strict";

const Model = use("App/Base/Models/BaseModel");

class CmsUserPreference extends Model {

  cmsUser() {
    return this.hasOne("App/Base/Models/CmsUser", "cms_user_id", "id");
  }

}

module.exports = CmsUserPreference;
