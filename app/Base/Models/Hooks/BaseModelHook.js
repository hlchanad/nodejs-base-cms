const BaseModelHook = exports = module.exports = {};

BaseModelHook.beforeCreate = async (model) => {
};

BaseModelHook.beforeUpdate = async (model) => {
};

BaseModelHook.beforeDelete = async (model) => {
};

BaseModelHook.afterFind = async (model) => {
  convertBufferToBoolean(model);
};

BaseModelHook.afterFetch = async (models) => {
  models.forEach(model => convertBufferToBoolean(model));
};

const convertBufferToBoolean = (object) => {
  Object.entries(object.toObject())
    .filter(entry => entry[1] != null)
    .filter(entry => entry[1].constructor.name === "Buffer")
    .forEach(entry => {
      object[entry[0]] = entry[1][0] === 1;
    });
};

module.exports = BaseModelHook;
