"use strict";

const Model = use("App/Base/Models/BaseModel");

class CmsPermission extends Model {

  static validationRules(id) {
    return {
      title: "string|required",
      action: "string|required",
    };
  }

}

module.exports = CmsPermission;
