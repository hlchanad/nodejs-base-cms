"use strict";

const Model = use("App/Base/Models/BaseModel");

class CmsRolePermission extends Model {

  cmsRole() {
    return this.hasOne("App/Base/Models/CmsRole", "cms_role_id", "id");
  }

  cmsPermission() {
    return this.hasOne("App/Base/Models/CmsPermission", "cms_permission_id", "id");
  }

}

module.exports = CmsRolePermission;
