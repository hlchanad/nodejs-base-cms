"use strict";

const Model = use("App/Base/Models/BaseModel");

class Locale extends Model {

  static validationRules(id) {
    return {
      locale: "string|required",
      group: "string|required",
      item: "string|required",
      "details.*.text": "string|required",
    };
  }

  static async findByGroupAndItem(group, item) {
    return this.query()
      .where("group", group)
      .where("item", item)
      .fetch();
  }

  static async findByLocaleAndGroup(locale, group) {
    return this.query()
      .where("locale",locale)
      .where("group", group)
      .fetch();
  }

}

module.exports = Locale;
