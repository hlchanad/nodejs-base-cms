'use strict';

const Model = use("App/Base/Models/BaseModel");

class CmsRole extends Model {

  cmsRolePermissions() {
    return this.hasMany("App/Base/Models/CmsRolePermission");
  }

  async cmsPermissions() {
    const cmsRolePermissions = await this.cmsRolePermissions().fetch();
    return Promise.all(cmsRolePermissions.rows.map(cmsRolePermission => cmsRolePermission.cmsPermission().first()));
  }

  static validationRules(id) {
    return {
      code: `string|required|unique:cms_roles,code,id,${id}`,
      title: 'string|required',
      description: 'string',
      permission_ids: 'required|databaseRecord:cms_permissions',
    };
  }

  static async getAll() {
    return this.query()
      .where("selectable", true)
      .where("is_visible", true)
      .where("is_deleted", false)
      .fetch();
  }

  static async findByIdIn(ids) {
    return this.query()
      .whereIn(this.primaryKey, ids)
      .where("is_visible", true)
      .where("is_deleted", false)
      .fetch();
  }

}

module.exports = CmsRole;
