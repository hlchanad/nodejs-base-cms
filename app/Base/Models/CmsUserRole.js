"use strict";

const Model = use("App/Base/Models/BaseModel");

class CmsUserRole extends Model {

  cmsUser() {
    return this.hasOne("App/Base/Models/CmsUser", "cms_user_id", "id");
  }

  cmsRole() {
    return this.hasOne("App/Base/Models/CmsRole", "cms_role_id", "id");
  }

}

module.exports = CmsUserRole;
