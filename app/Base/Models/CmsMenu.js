"use strict";

const Model = use("App/Base/Models/BaseModel");

const CmsMenuIconType = use("App/Base/Constants/CmsMenuIconType");

class CmsMenu extends Model {

  cmsRoleMenus() {
    return this.hasMany("App/Base/Models/CmsRoleMenu");
  }

  async cmsRoles() {
    const cmsRoleMenus = await this.cmsRoleMenus().fetch();
    return Promise.all(cmsRoleMenus.rows.map(cmsRoleMenu => cmsRoleMenu.cmsRole().first()));
  }

  parent() {
    return this.hasOne("App/Base/Models/CmsMenu", "parent_id", "id");
  }

  children() {
    return this.hasMany("App/Base/Models/CmsMenu", "id", "parent_id");
  }

  static validationRules(id) {
    const iconTypes = CmsMenuIconType.enums.map(e => e.key).join(",");
    return {
      parent_id: "integer",
      title: "string|required",
      endpoint: "string",
      icon_type: `string|required|in:${iconTypes}`,
      icon: "string|required",
      sequence: "integer|required",
      role_ids: "databaseRecord:cms_roles",
    };
  }

  static async findByIdNotIn(ids = []) {
    return CmsMenu.query()
      .where("is_deleted", false)
      .whereNotIn("id", Array.isArray(ids) ? ids : [ids])
      .fetch();
  }

}

module.exports = CmsMenu;
