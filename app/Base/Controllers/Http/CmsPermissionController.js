"use strict";

const moment = require("moment");

const BaseController = use("App/Base/Controllers/Http/BaseController");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const DetailPageHelper = use("App/Base/Helpers/DetailPageHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const RecordNotFoundException = use("App/Base/Exceptions/RecordNotFoundException");
const LastUpdatedAtNotMatchException = use("App/Base/Exceptions/LastUpdatedAtNotMatchException");
const FailedToUpdateRecordException = use("App/Base/Exceptions/FailedToUpdateRecordException");
const ValidationException = use("App/Base/Exceptions/ValidationException");
const ApiStatus = use("App/Base/Models/Common/ApiStatus");
const CmsPermissionService = use("App/Base/Services/CmsPermissionService");
const CmsPermissionDatatableService = use("App/Base/Services/Datatable/CmsPermissionDatatableService");
const CmsPermission = use("App/Base/Models/CmsPermission");

class CmsPermissionController extends BaseController {

  async index({ request, view, auth }) {
    const columns = CmsPermissionDatatableService.datatableColumns();

    if (request.ajax()) {
      return CmsPermissionDatatableService.datatableResponse(request.get(), auth.user);
    }

    return this.render(view, "base.cms-permissions.index", {
      title: I18nHelper.formatMessage("cms-permissions.title"),
      allowCreate: true,
      createUrl: RouteHelper.getUrl("/cms-permissions/create"),
      allowBatchDelete: true,
      batchDeleteMethod: "POST",
      batchDeleteUrl: RouteHelper.getUrl("/cms-permissions/delete-requests"),
      datatable: {
        tableId: "cms-permissions",
        columns: columns,
      },
    });
  }

  async show({ request, view, auth }) {
    const id = request.params.id;

    const cmsPermission = await CmsPermission.getWithMutator(id);

    if (cmsPermission == null) {
      throw new RecordNotFoundException("cms-permissions", id);
    }

    return this.render(view, "base.cms-permissions.show", {
      title: I18nHelper.formatMessage("cms-permissions.title") + " - " + id,
      module: "cms-permissions",
      model: cmsPermission,
      detailButtonsHtml: await DetailPageHelper.getDefaultDetailButtons(auth.user, "CmsPermissionController", "cms-permissions", cmsPermission),
    });
  }

  async create({ view }) {
    return this.render(view, "base.cms-permissions.create", {
      title: I18nHelper.formatMessage("cms-permissions.title") + " - " + I18nHelper.formatMessage("general.create"),
      module: "cms-permissions",
      formConfig: {
        method: "POST",
        action: RouteHelper.getUrl("/cms-permissions"),
        cancelUrl: RouteHelper.getUrl("/cms-permissions"),
      },
      model: null,
      actionOptions: await CmsPermissionService.getActionOptions(),
    });
  }

  async store({ request, response, auth }) {
    const cmsPermission = await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/cms-permissions/${ cmsPermission.id }`));
  }

  async edit({ request, view }) {
    const id = parseInt(request.params.id);

    const cmsPermission = await CmsPermission.get(id);

    if (cmsPermission == null) {
      throw new RecordNotFoundException("cms-permissions", id);
    }

    return this.render(view, "base.cms-permissions.edit", {
      title: I18nHelper.formatMessage("cms-permissions.title") + " - " + id,
      module: "cms-permissions",
      formConfig: {
        method: "PUT",
        action: RouteHelper.getUrl(`/cms-permissions/${id}`),
        cancelUrl: RouteHelper.getUrl("/cms-permissions"),
      },
      model: cmsPermission,
      actionOptions: await CmsPermissionService.getActionOptions(id),
    });
  }

  async update({ request, response, auth }) {
    const id = request.all().id;

    const cmsPermission = await CmsPermission.get(id);

    if (cmsPermission == null) {
      throw new RecordNotFoundException("cms-permissions", id);
    }

    if (cmsPermission.updated_at != null && request.all().updated_at == null
      || !moment(cmsPermission.updated_at).isSame(moment(request.all().updated_at))) {
      throw new LastUpdatedAtNotMatchException("cms-permissions", id);
    }

    await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/cms-permissions/${id}`));
  }

  async destroy({ request, response, auth }) {
    const id = request.params.id;

    const cmsPermission = await CmsPermission.get(id);

    if (cmsPermission == null) {
      return response.notFound(this.api(ApiStatus.NOT_FOUND));
    }

    const success = await cmsPermission.delete(auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    return this.api(ApiStatus.OK);
  }

  async batchDelete({ request, response, auth }) {
    const validation = await ValidationHelper.validate(request.all(), {
      ids: "array|required|databaseRecord:cms_permissions",
    });

    if (validation.fails()) {
      return response.badRequest(this.api(ApiStatus.BAD_REQUEST), validation.messages());
    }

    const success = await CmsPermission.batchDelete(request.all().ids, auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    return this.api(ApiStatus.OK);
  }

  async upsert({ request, auth }) {
    const id = request.all().id;
    const cmsPermission = id == null ? new CmsPermission : await CmsPermission.get(id);

    const validation = await ValidationHelper.validate(request.all(), CmsPermission.validationRules(id));

    if (validation.fails()) {
      throw new ValidationException(validation, []);
    }

    try {
      await CmsPermissionService.upsertCmsPermission(cmsPermission, request.all(), auth.user);
    } catch (e) {
      throw new FailedToUpdateRecordException(e);
    }

    return cmsPermission;
  }

}

module.exports = CmsPermissionController;
