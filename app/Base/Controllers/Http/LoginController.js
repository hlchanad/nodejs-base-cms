"use strict";

const BaseController = use("App/Base/Controllers/Http/BaseController");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const AuthException = use("App/Base/Exceptions/AuthException");
const UserNotFoundException = use("App/Base/Exceptions/UserNotFoundException");
const ValidationException = use("App/Base/Exceptions/ValidationException");
const LoginService = use("App/Base/Services/LoginService");
const BaseSessionKey = use("App/Base/Constants/BaseSessionKey");

class LoginController extends BaseController {

  async index({ view, response, auth }) {
    try {
      await auth.check();

      return response.redirect(RouteHelper.getUrl("/cms-users"));

    } catch (ignored) {}

    return this.render(view, "base.login.index");
  }

  async login({ request, response, session, auth }) {

    const validation = await ValidationHelper.validate(request.all(), {
      username: "string|required",
      password: "string|required",
    });

    if (validation.fails()) {
      throw new ValidationException(validation, ["password"]);
    }

    let user;
    try {
      user = await auth.attempt(request.all().username, request.all().password);
    } catch (error) {
      if (["E_USER_NOT_FOUND", "E_PASSWORD_MISMATCH"].includes(error.code)) {
        throw new UserNotFoundException();
      }

      throw new AuthException();
    }

    const lastRoute = session.get(BaseSessionKey.LAST_ROUTE);

    return response.redirect(await LoginService.getRedirectRoute(user, lastRoute));
  }

  async logout({ response, auth}) {
    try{
      await auth.logout();
    } catch (ignored) {}

    return response.redirect(RouteHelper.getUrl("/login"));
  }

}

module.exports = LoginController;
