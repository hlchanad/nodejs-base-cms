'use strict';

const BaseController = use("App/Base/Controllers/Http/BaseController");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const ImageService = use("App/Base/Services/ImageService");
const ApiStatus = use("App/Base/Models/Common/ApiStatus");

class ImageController extends BaseController {

  async store({ request, response, auth }) {

    const validation = await ValidationHelper.validate({
      ...request.all(),
      ...request.files(),
    }, {
      isPublic: "boolean",
      folder: "string|required",
      image: "idOrImage:true",
    });

    if (validation.fails()) {
      return response.badRequest(this.api(ApiStatus.BAD_REQUEST, validation.messages()));
    }

    const folder = request.all().folder;
    const isPublic = request.all().isPublic === "true";

    let image;
    try {
      image = await ImageService.uploadImage(request, "image", folder, auth.user, { isPublic });
    } catch (e) {
      return response.badRequest(this.api(ApiStatus.BAD_REQUEST, e));
    }

    return this.api(ApiStatus.OK, {
      imageId: image.id,
    });
  }

}

module.exports = ImageController;
