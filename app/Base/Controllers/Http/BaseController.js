const _ = require("lodash");

const ImageService = use("App/Base/Services/ImageService");
const FailedToUploadImageException = use("App/Base/Exceptions/FailedToUploadImageException");
const ApiResponse = use("App/Base/Models/Common/ApiResponse");

class BaseController {

  render(view, viewName, props = {}) {
    if (props.breadcrumbs == null) {
      props.breadcrumbs = _.get(view.locals, "breadcrumbs", []);
    }

    if (props.sidebarMenu == null) {
      props.sidebarMenu = _.get(view.locals, "sidebarMenu", []);
    }

    return view.render(viewName, props);
  }

   api(status, data) {
     return new ApiResponse(status.toMultiLanguage(), data);
   }

   async handleImage(request, field, folder, user, exceptFields) {
     let image;
     try {
       image = await ImageService.uploadImage(request, field, folder, user);
     } catch (e) {
       throw new FailedToUploadImageException(e, exceptFields);
     }

     const oldId = _.get(request.all(), `${field}_old`, null);

     return _.get(image, "id", oldId);
   }

}

module.exports = BaseController;
