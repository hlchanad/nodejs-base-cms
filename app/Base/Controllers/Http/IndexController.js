'use strict';

const BaseController = use("App/Base/Controllers/Http/BaseController");
const RouteHelper = use("App/Base/Helpers/RouteHelper");

class IndexController extends BaseController {

  async index({ response }) {
    return response.redirect(RouteHelper.getUrl("/dashboard"));
  }

}

module.exports = IndexController;
