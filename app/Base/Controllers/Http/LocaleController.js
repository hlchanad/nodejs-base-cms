"use strict";

const _ = require("lodash");
const moment = require("moment");

const BaseController = use("App/Base/Controllers/Http/BaseController");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const RecordNotFoundException = use("App/Base/Exceptions/RecordNotFoundException");
const LastUpdatedAtNotMatchException = use("App/Base/Exceptions/LastUpdatedAtNotMatchException");
const FailedToUpdateRecordException = use("App/Base/Exceptions/FailedToUpdateRecordException");
const ValidationException = use("App/Base/Exceptions/ValidationException");
const ApiStatus = use("App/Base/Models/Common/ApiStatus");
const LocaleService = use("App/Base/Services/LocaleService");
const LocaleDatatableService = use("App/Base/Services/Datatable/LocaleDatatableService");
const Locale = use("App/Base/Models/Locale");

class LocaleController extends BaseController {

  async index({ request, view, auth }) {
    const columns = LocaleDatatableService.datatableColumns();

    if (request.ajax()) {
      return LocaleDatatableService.datatableResponse(request.get(), auth.user);
    }

    return this.render(view, "base.locales.index", {
      title: I18nHelper.formatMessage("locales.title"),
      allowCreate: false,
      createUrl: RouteHelper.getUrl("/locales/create"),
      allowBatchDelete: false,
      batchDeleteMethod: "POST",
      batchDeleteUrl: RouteHelper.getUrl("/locales/delete-requests"),
      datatable: {
        tableId: "locales",
        columns: columns,
      },
    });
  }

  async show({ request, view, auth }) {
    const id = request.params.id;

    const locale = await Locale.getWithMutator(id);

    if (locale == null) {
      throw new RecordNotFoundException("locales", id);
    }

    return this.render(view, "base.locales.show", {
      title: I18nHelper.formatMessage("locales.title") + " - " + id,
      module: "locales",
      model: locale,
      modelDetails: await LocaleService.getLocaleDetailsMap(locale.group, locale.item),
      detailButtonsHtml: await LocaleService.getDefaultDetailButtons(auth.user, "LocaleController", "locales", locale),
    });
  }

  async edit({ request, view }) {
    const id = request.params.id;

    const locale = await Locale.get(id);

    if (locale == null) {
      throw new RecordNotFoundException("locales", id);
    }

    return this.render(view, "base.locales.edit", {
      title: I18nHelper.formatMessage("locales.title") + " - " + id,
      module: "locales",
      formConfig: {
        method: "PUT",
        action: RouteHelper.getUrl(`/locales/${id}`),
        cancelUrl: RouteHelper.getUrl("/locales"),
      },
      model: locale,
      modelDetails: await LocaleService.getLocaleDetailsMap(locale.group, locale.item),
    });
  }

  async update({ request, response, auth }) {
    const id = request.all().id;

    const locale = await Locale.get(id);

    if (locale == null) {
      throw new RecordNotFoundException("locales", id);
    }

    if (locale.updated_at != null && request.all().updated_at == null
      || !moment(locale.updated_at).isSame(moment(request.all().updated_at))) {
      throw new LastUpdatedAtNotMatchException("locales", id);
    }

    await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/locales/${id}`));
  }

  async upsert({ request, auth }) {
    const id = request.all().id;
    const locale = id == null ? new Locale : await Locale.get(id);

    const validation = await ValidationHelper.validate(request.all(), Locale.validationRules(id));

    if (validation.fails()) {
      throw new ValidationException(validation, []);
    }

    try {
      await LocaleService.upsertLocale(locale, request.all(), auth.user);
    } catch (e) {
      throw new FailedToUpdateRecordException(e);
    }

    await I18nHelper.reloadLocalesCache();

    return locale;
  }

  async getByLibrary({ request, response }) {
    const validation = await ValidationHelper.validate(request.params, {
      library: "string|required|in:datatablesjs,dropzonejs,daterangepicker",
    });

    if (validation.fails()) {
      return response.badRequest(this.api(ApiStatus.BAD_REQUEST), validation.messages());
    }

    const library = request.params.library;
    const locale = I18nHelper.currentLocale();

    return await LocaleService[`get${_.capitalize(library)}Locales`](locale);
  }

}

module.exports = LocaleController;
