'use strict';

const BaseController = use("App/Base/Controllers/Http/BaseController");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const Breadcrumb = use("App/Base/Models/Common/Breadcrumb");

class DashboardController extends BaseController {

  async index({ view }) {
    const breadcrumbs = [
      new Breadcrumb(I18nHelper.formatMessage(Breadcrumb.Root.title), Breadcrumb.Root.url),
      new Breadcrumb(I18nHelper.formatMessage('general.dashboard'), RouteHelper.getUrl("/dashboard")),
    ];
    return this.render(view, "base.dashboard.index", {
      breadcrumbs: breadcrumbs,
    });
  }

}

module.exports = DashboardController;
