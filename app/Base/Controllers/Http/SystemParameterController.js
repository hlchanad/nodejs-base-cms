"use strict";

const moment = require("moment");

const Logger = use("Log");

const BaseController = use("App/Base/Controllers/Http/BaseController");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const RecordNotFoundException = use("App/Base/Exceptions/RecordNotFoundException");
const LastUpdatedAtNotMatchException = use("App/Base/Exceptions/LastUpdatedAtNotMatchException");
const FailedToUpdateRecordException = use("App/Base/Exceptions/FailedToUpdateRecordException");
const ValidationException = use("App/Base/Exceptions/ValidationException");
const SystemParameterService = use("App/Base/Services/SystemParameterService");
const SystemParameterDatatableService = use("App/Base/Services/Datatable/SystemParameterDatatableService");
const SystemParameter = use("App/Base/Models/SystemParameter");

class SystemParameterController extends BaseController {

  async index({ request, view, auth }) {
    const columns = SystemParameterDatatableService.datatableColumns();

    if (request.ajax()) {
      return SystemParameterDatatableService.datatableResponse(request.get(), auth.user);
    }

    return this.render(view, "base.system-parameters.index", {
      title: I18nHelper.formatMessage("system-parameters.title"),
      allowCreate: false,
      createUrl: RouteHelper.getUrl("/system-parameters/create"),
      allowBatchDelete: false,
      batchDeleteMethod: "POST",
      batchDeleteUrl: RouteHelper.getUrl("/system-parameters/delete-requests"),
      datatable: {
        tableId: "system-parameters",
        columns: columns,
      },
    });
  }

  async show({ request, view, auth }) {
    const id = request.params.id;

    const systemParameter = await SystemParameter.getWithMutator(id);

    if (systemParameter == null) {
      throw new RecordNotFoundException("system-parameters", id);
    }

    return this.render(view, "base.system-parameters.show", {
      title: I18nHelper.formatMessage("system-parameters.title") + " - " + id,
      module: "system-parameters",
      model: systemParameter,
      detailButtonsHtml: await SystemParameterService.getDefaultDetailButtons(auth.user, "SystemParameterController", "system-parameters", systemParameter),
    });
  }

  async edit({ request, view }) {
    const id = request.params.id;

    const systemParameter = await SystemParameter.get(id);

    if (systemParameter == null) {
      throw new RecordNotFoundException("system-parameters", id);
    }

    return this.render(view, "base.system-parameters.edit", {
      title: I18nHelper.formatMessage("system-parameters.title") + " - " + id,
      module: "system-parameters",
      formConfig: {
        method: "PUT",
        action: RouteHelper.getUrl(`/system-parameters/${id}`),
        cancelUrl: RouteHelper.getUrl("/system-parameters"),
      },
      model: systemParameter,
    });
  }

  async update({ request, response, auth }) {
    const id = request.all().id;

    const systemParameter = await SystemParameter.get(id);

    if (systemParameter == null) {
      throw new RecordNotFoundException("system-parameters", id);
    }

    if (systemParameter.updated_at != null && request.all().updated_at == null
      || !moment(systemParameter.updated_at).isSame(moment(request.all().updated_at))) {
      throw new LastUpdatedAtNotMatchException("system-parameters", id);
    }

    await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/system-parameters/${id}`));
  }

  async upsert({ request, auth }) {
    const id = request.all().id;
    const systemParameter = id == null ? new SystemParameter : await SystemParameter.get(id);

    const validation = await ValidationHelper.validate(request.all(), SystemParameter.validationRules(id));

    if (validation.fails()) {
      throw new ValidationException(validation, []);
    }

    try {
      await SystemParameterService.upsertSystemParameter(systemParameter, request.all(), auth.user);
    } catch (e) {
      throw new FailedToUpdateRecordException(e);
    }

    return systemParameter;
  }

}

module.exports = SystemParameterController;
