"use strict";

const moment = require("moment");

const BaseController = use("App/Base/Controllers/Http/BaseController");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const DetailPageHelper = use("App/Base/Helpers/DetailPageHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");
const RecordNotFoundException = use("App/Base/Exceptions/RecordNotFoundException");
const LastUpdatedAtNotMatchException = use("App/Base/Exceptions/LastUpdatedAtNotMatchException");
const FailedToUpdateRecordException = use("App/Base/Exceptions/FailedToUpdateRecordException");
const ValidationException = use("App/Base/Exceptions/ValidationException");
const NoPermissionException = use("App/Base/Exceptions/NoPermissionException");
const ApiStatus = use("App/Base/Models/Common/ApiStatus");
const CmsRoleService = use("App/Base/Services/CmsRoleService");
const CmsRoleDatatableService = use("App/Base/Services/Datatable/CmsRoleDatatableService");
const CmsRole = use("App/Base/Models/CmsRole");
const CmsRoleCode = use("App/Base/Constants/CmsRoleCode");

class CmsRoleController extends BaseController {

  async index({ request, view, auth }) {
    const columns = CmsRoleDatatableService.datatableColumns();

    if (request.ajax()) {
      return CmsRoleDatatableService.datatableResponse(request.get(), auth.user);
    }

    return this.render(view, "base.cms-roles.index", {
      title: I18nHelper.formatMessage("cms-roles.title"),
      allowCreate: true,
      createUrl: RouteHelper.getUrl("/cms-roles/create"),
      allowBatchDelete: true,
      batchDeleteMethod: "POST",
      batchDeleteUrl: RouteHelper.getUrl("/cms-roles/delete-requests"),
      datatable: {
        tableId: "cms-roles",
        columns: columns,
      },
    });
  }

  async show({ request, view, auth }) {
    const id = request.params.id;

    const cmsRole = await CmsRole.getWithMutator(id);

    if (cmsRole == null) {
      throw new RecordNotFoundException("cms-roles", id);
    }

    const cmsPermissions = await cmsRole.cmsPermissions();
    const permissionTitles = cmsPermissions
      .map(cmsPermission => cmsPermission.title)
      .sort((a, b) => a.localeCompare(b))
      .join(", ");

    return this.render(view, "base.cms-roles.show", {
      title: I18nHelper.formatMessage("cms-roles.title") + " - " + id,
      module: "cms-roles",
      model: cmsRole,
      permissionTitles: permissionTitles,
      detailButtonsHtml: await CmsRoleService.getDefaultDetailButtons(auth.user, "CmsRoleController", "cms-roles", cmsRole),
    });
  }

  async create({ view }) {
    return this.render(view, "base.cms-roles.create", {
      title: I18nHelper.formatMessage("cms-roles.title") + " - " + I18nHelper.formatMessage("general.create"),
      module: "cms-roles",
      formConfig: {
        method: "POST",
        action: RouteHelper.getUrl(`/cms-roles`),
        cancelUrl: RouteHelper.getUrl("/cms-roles"),
      },
      model: null,
      permissionOptions: await CmsRoleService.getPermissionOptions(),
    });
  }

  async store({ request, response, auth }) {
    const cmsRole = await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/cms-roles/${cmsRole.id}`));
  }

  async edit({ request, view }) {
    const id = request.params.id;

    const cmsRole = await CmsRole.get(id);

    if (cmsRole == null) {
      throw new RecordNotFoundException("cms-roles", id);
    }

    if (cmsRole.code === CmsRoleCode.ROOT) {
      throw new NoPermissionException();
    }

    const cmsRolePermissions = await cmsRole.cmsRolePermissions().fetch();

    return this.render(view, "base.cms-roles.edit", {
      title: I18nHelper.formatMessage("cms-roles.title") + " - " + id,
      module: "cms-roles",
      formConfig: {
        method: "PUT",
        action: RouteHelper.getUrl(`/cms-roles/${id}`),
        cancelUrl: RouteHelper.getUrl("/cms-roles"),
      },
      model: cmsRole,
      permissionOptions: await CmsRoleService.getPermissionOptions(),
      selectedPermissionIds: cmsRolePermissions.rows.map(cmsRolePermission => cmsRolePermission.cms_permission_id),
    });
  }

  async update({ request, response, auth }) {
    const id = request.all().id;

    const cmsRole = await CmsRole.get(id);

    if (cmsRole == null) {
      throw new RecordNotFoundException("cms-roles", id);
    }

    if (cmsRole.updated_at != null && request.all().updated_at == null
      || !moment(cmsRole.updated_at).isSame(moment(request.all().updated_at))) {
      throw new LastUpdatedAtNotMatchException("cms-roles", id);
    }

    if (cmsRole.code === CmsRoleCode.ROOT) {
      throw new NoPermissionException();
    }

    await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/cms-roles/${id}`));
  }

  async destroy({ request, response, auth }) {
    const id = request.params.id;

    const cmsRole = await CmsRole.get(id);

    if (cmsRole == null) {
      return response.notFound(this.api(ApiStatus.NOT_FOUND));
    }

    if (cmsRole.code === CmsRoleCode.ROOT) {
      return response.forbidden(this.api(ApiStatus.FORBIDDEN));
    }

    const success = await cmsRole.delete(auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    await PermissionHelper.updateRedisRolePermissions();

    return this.api(ApiStatus.OK);
  }

  async batchDelete({ request, response, auth }) {
    const validation = await ValidationHelper.validate(request.all(), {
      ids: "array|required|databaseRecord:cms_roles",
    });

    if (validation.fails()) {
      return response.badRequest(this.api(ApiStatus.BAD_REQUEST), validation.messages());
    }

    const cmsRoles = await CmsRole.findByIdIn(request.all().ids);
    if (cmsRoles.rows.some(cmsRole => cmsRole.code === CmsRoleCode.ROOT)) {
      return response.forbidden(this.api(ApiStatus.FORBIDDEN));
    }

    const success = await CmsRole.batchDelete(request.all().ids, auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    return this.api(ApiStatus.OK);
  }

  async upsert({ request, auth }) {
    const id = request.all().id;
    const cmsRole = id == null ? new CmsRole : await CmsRole.get(id);

    const validation = await ValidationHelper.validate(request.all(), CmsRole.validationRules(id));

    if (validation.fails()) {
      throw new ValidationException(validation, []);
    }

    try {
      await CmsRoleService.upsertCmsRole(cmsRole, request.all(), auth.user);
      await PermissionHelper.updateRedisRolePermissions();
    } catch (e) {
      throw new FailedToUpdateRecordException(e);
    }

    return cmsRole;
  }

}

module.exports = CmsRoleController;
