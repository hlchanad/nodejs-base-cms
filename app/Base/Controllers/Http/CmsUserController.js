"use strict";

const _ = require("lodash");
const moment = require("moment");

const BaseController = use("App/Base/Controllers/Http/BaseController");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const DetailPageHelper = use("App/Base/Helpers/DetailPageHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const RecordNotFoundException = use("App/Base/Exceptions/RecordNotFoundException");
const LastUpdatedAtNotMatchException = use("App/Base/Exceptions/LastUpdatedAtNotMatchException");
const FailedToUpdateRecordException = use("App/Base/Exceptions/FailedToUpdateRecordException");
const ValidationException = use("App/Base/Exceptions/ValidationException");
const ApiStatus = use("App/Base/Models/Common/ApiStatus");
const CmsUserService = use("App/Base/Services/CmsUserService");
const CmsRoleService = use("App/Base/Services/CmsRoleService");
const CmsUserDatatableService = use("App/Base/Services/Datatable/CmsUserDatatableService");
const CmsUser = use("App/Base/Models/CmsUser");
const CmsUserPreferenceService = use("App/Base/Services/CmsUserPreferenceService");
const CmsUserPreferenceKey = use("App/Base/Constants/CmsUserPreferenceKey");
const Language = use("App/Base/Models/Common/Language");

class CmsUserController extends BaseController {

  async index({ request, view, auth }) {
    const columns = CmsUserDatatableService.datatableColumns();

    if (request.ajax()) {
      return await CmsUserDatatableService.datatableResponse(request.get(), auth.user);
    }

    return this.render(view, "base.cms-users.index", {
      title: I18nHelper.formatMessage("cms-users.title"),
      allowCreate: true,
      createUrl: RouteHelper.getUrl("/cms-users/create"),
      allowBatchDelete: true,
      batchDeleteMethod: "POST",
      batchDeleteUrl: RouteHelper.getUrl("/cms-users/delete-requests"),
      datatable: {
        tableId: "cms-users",
        columns: columns,
      },
    });
  }

  async show({ request, view, auth }) {
    const id = request.params.id;

    const cmsUser = await CmsUser.getWithMutator(id);

    if (cmsUser == null) {
      throw new RecordNotFoundException("cms-users", id);
    }

    const cmsRoles = await cmsUser.cmsRoles();
    const roleTitles = cmsRoles
      .map(cmsRole => cmsRole.title)
      .sort((a, b) => a.localeCompare(b))
      .join(", ");

    return this.render(view, "base.cms-users.show", {
      title: I18nHelper.formatMessage("cms-users.title") + " - " + id,
      module: "cms-users",
      model: cmsUser,
      roleTitles: roleTitles,
      detailButtonsHtml: await DetailPageHelper.getDefaultDetailButtons(auth.user, "CmsUserController", "cms-users", cmsUser),
    });
  }

  async create({ view }) {
    return this.render(view, "base.cms-users.create", {
      title: I18nHelper.formatMessage("cms-users.title") + " - " + I18nHelper.formatMessage("general.create"),
      module: "cms-users",
      formConfig: {
        method: "POST",
        action: RouteHelper.getUrl("/cms-users"),
        cancelUrl: RouteHelper.getUrl("/cms-users"),
      },
      model: null,
      roleOptions: await CmsRoleService.getRoleOptions(),
    });
  }

  async store({ request, response, auth }) {
    const cmsUser = await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/cms-users/${ cmsUser.id }`));
  }

  async edit({ request, view }) {
    const id = request.params.id;

    const cmsUser = await CmsUser.get(id);

    if (cmsUser == null) {
      throw new RecordNotFoundException("cms-users", id);
    }

    const cmsUserRoles = await cmsUser.cmsUserRoles().fetch();

    return this.render(view, "base.cms-users.edit", {
      title: I18nHelper.formatMessage("cms-users.title") + " - " + id,
      module: "cms-users",
      formConfig: {
        method: "PUT",
        action: RouteHelper.getUrl(`/cms-users/${id}`),
        cancelUrl: RouteHelper.getUrl("/cms-users"),
      },
      model: cmsUser,
      roleOptions: await CmsRoleService.getRoleOptions(),
      selectedRoleIds: cmsUserRoles.rows.map(cmsUserRole => cmsUserRole.cms_role_id),
    });
  }

  async update({ request, response, auth }) {
    const id = request.all().id;

    const cmsUser = await CmsUser.get(id);

    if (cmsUser == null) {
      throw new RecordNotFoundException("cms-users", id);
    }

    if (cmsUser.updated_at != null && request.all().updated_at == null
      || !moment(cmsUser.updated_at).isSame(moment(request.all().updated_at))) {
      throw new LastUpdatedAtNotMatchException("cms-users", id);
    }

    await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/cms-users/${id}`));
  }

  async destroy({ request, response, auth }) {
    const id = request.params.id;

    const cmsUser = await CmsUser.get(id);

    if (cmsUser == null) {
      return response.notFound(this.api(ApiStatus.NOT_FOUND));
    }

    const success = await cmsUser.delete(auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    return this.api(ApiStatus.OK);
  }

  async batchDelete({ request, response, auth }) {
    const validation = await ValidationHelper.validate(request.all(), {
      ids: "array|required|databaseRecord:cms_users",
    });

    if (validation.fails()) {
      return response.badRequest(this.api(ApiStatus.BAD_REQUEST, validation.messages()));
    }

    const success = await CmsUser.batchDelete(request.all().ids, auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    return this.api(ApiStatus.OK);
  }

  async updatePreference({ request, response, auth }) {

    const validation = await ValidationHelper.validate(request.all(), {
      value: CmsUserPreferenceKey.validationRules()[request.all().key],
    });

    if (validation.fails()) {
      throw new ValidationException(validation, []);
    }

    await CmsUserPreferenceService.updateCmsUserPreference(auth.user, request.all().key, request.all().value);

    return response.redirect("back");
  }

  async upsert({ request, auth }) {
    const id = request.all().id;
    const cmsUser = id == null ? new CmsUser : await CmsUser.get(id);

    const validation = await ValidationHelper.validate(request.all(), CmsUser.validationRules(id));

    if (validation.fails()) {
      throw new ValidationException(validation, ["password"]);
    }

    try {
      await CmsUserService.upsertCmsUser(cmsUser, request.all(), auth.user);
    } catch (e) {
      throw new FailedToUpdateRecordException(e);
    }

    return cmsUser;
  }

}

module.exports = CmsUserController;
