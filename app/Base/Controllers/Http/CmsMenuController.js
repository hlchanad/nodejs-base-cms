"use strict";

const moment = require("moment");
const _ = require("lodash");

const BaseController = use("App/Base/Controllers/Http/BaseController");
const Breadcrumb = use("App/Base/Models/Common/Breadcrumb");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const DetailPageHelper = use("App/Base/Helpers/DetailPageHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const RecordNotFoundException = use("App/Base/Exceptions/RecordNotFoundException");
const LastUpdatedAtNotMatchException = use("App/Base/Exceptions/LastUpdatedAtNotMatchException");
const FailedToUpdateRecordException = use("App/Base/Exceptions/FailedToUpdateRecordException");
const ValidationException = use("App/Base/Exceptions/ValidationException");
const ApiStatus = use("App/Base/Models/Common/ApiStatus");
const CmsMenuService = use("App/Base/Services/CmsMenuService");
const CmsMenuDatatableService = use("App/Base/Services/Datatable/CmsMenuDatatableService");
const CmsMenu = use("App/Base/Models/CmsMenu");

class CmsMenuController extends BaseController {

  async index({ request, view, auth }) {
    const columns = CmsMenuDatatableService.datatableColumns();

    if (request.ajax()) {
      return CmsMenuDatatableService.datatableResponse(request.get(), auth.user);
    }

    const parentId = request.get().parent_id;
    const parent = _.isEmpty(parentId) ? null : (await CmsMenu.get(parentId));
    const parentName = parent == null ? I18nHelper.formatMessage("cms-menus.fields.parent_id.root") : parent.title;

    const breadcrumbs = [
      ...await CmsMenuService.getParentsBreadcrumbs(parent),
    ];

    return this.render(view, "base.cms-menus.index", {
      title: `${I18nHelper.formatMessage("cms-menus.title")} - ${parentName}`,
      allowCreate: !await CmsMenuService.isMaxCmsMenuLevels(parent),
      createUrl: RouteHelper.getUrl("/cms-menus/create?parent_id=" + (parentId || "")),
      allowBatchDelete: true,
      batchDeleteMethod: "POST",
      batchDeleteUrl: RouteHelper.getUrl("/cms-menus/delete-requests"),
      datatable: {
        tableId: "cms-menus",
        columns: columns,
      },
      breadcrumbs: breadcrumbs,
    });
  }

  async show({ request, view, auth }) {
    const id = request.params.id;

    const cmsMenu = await CmsMenu.getWithMutator(id);

    if (cmsMenu == null) {
      throw new RecordNotFoundException("cms-menus", id);
    }

    const cmsRoles = await cmsMenu.cmsRoles();
    const roleTitles = cmsRoles
      .map(cmsRole => cmsRole.title)
      .sort((a, b) => a.localeCompare(b))
      .join(", ");

    const parent = await CmsMenu.get(cmsMenu.parent_id);
    const breadcrumbs = [
      ...await CmsMenuService.getParentsBreadcrumbs(parent),
      new Breadcrumb(I18nHelper.formatMessage("general.detail"), RouteHelper.getUrl(`/cms-menus/${id}`)),
    ];

    return this.render(view, "base.cms-menus.show", {
      title: I18nHelper.formatMessage("cms-menus.title") + " - " + id,
      module: "cms-menus",
      model: cmsMenu,
      roleTitles: roleTitles,
      detailButtonsHtml: await DetailPageHelper.getDefaultDetailButtons(auth.user, "CmsMenuController", "cms-menus", cmsMenu),
      breadcrumbs: breadcrumbs,
    });
  }

  async create({ request, view }) {
    const parentId = request.get().parent_id;
    const parent = _.isEmpty(parentId) ? null : (await CmsMenu.get(parentId));

    const breadcrumbs = [
      ...await CmsMenuService.getParentsBreadcrumbs(parent),
      new Breadcrumb(I18nHelper.formatMessage("general.create"), RouteHelper.getUrl(`/cms-menus/create?parent_id=${parentId}`)),
    ];

    return this.render(view, "base.cms-menus.create", {
      title: I18nHelper.formatMessage("cms-menus.title") + " - " + I18nHelper.formatMessage("general.create"),
      module: "cms-menus",
      formConfig: {
        method: "POST",
        action: RouteHelper.getUrl("/cms-menus"),
        cancelUrl: RouteHelper.getUrl("/cms-menus"),
      },
      model: null,
      parentId: request.get().parent_id,
      iconTypeOptions: CmsMenuService.getIconTypeOptions(),
      parentIdOptions: await CmsMenuService.getParentIdOptions(),
      roleOptions: await CmsMenuService.getRolesOptions(),
      breadcrumbs: breadcrumbs,
    });
  }

  async store({ request, response, auth }) {
    const cmsMenu = await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/cms-menus/${ cmsMenu.id }`));
  }

  async edit({ request, view }) {
    const id = request.params.id;

    const cmsMenu = await CmsMenu.get(id);

    if (cmsMenu == null) {
      throw new RecordNotFoundException("cms-menus", id);
    }

    const cmsRoleMenus = await cmsMenu.cmsRoleMenus().fetch();

    const parent = await CmsMenu.get(cmsMenu.parent_id);
    const breadcrumbs = [
      ...await CmsMenuService.getParentsBreadcrumbs(parent),
      new Breadcrumb(I18nHelper.formatMessage("general.edit"), RouteHelper.getUrl(`/cms-menus/${id}/edit`)),
    ];

    return this.render(view, "base.cms-menus.edit", {
      title: I18nHelper.formatMessage("cms-menus.title") + " - " + id,
      module: "cms-menus",
      formConfig: {
        method: "PUT",
        action: RouteHelper.getUrl(`/cms-menus/${id}`),
        cancelUrl: RouteHelper.getUrl("/cms-menus"),
      },
      model: cmsMenu,
      iconTypeOptions: CmsMenuService.getIconTypeOptions(),
      parentIdOptions: await CmsMenuService.getParentIdOptions(id),
      roleOptions: await CmsMenuService.getRolesOptions(id),
      selectedRoleIds: cmsRoleMenus.rows.map(cmsRoleMenu => cmsRoleMenu.cms_role_id),
      breadcrumbs: breadcrumbs,
    });
  }

  async update({ request, response, auth }) {
    const id = request.all().id;

    const cmsMenu = await CmsMenu.get(id);

    if (cmsMenu == null) {
      throw new RecordNotFoundException("cms-menus", id);
    }

    if (cmsMenu.updated_at != null && request.all().updated_at == null
      || !moment(cmsMenu.updated_at).isSame(moment(request.all().updated_at))) {
      throw new LastUpdatedAtNotMatchException("cms-menus", id);
    }

    await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/cms-menus/${id}`));
  }

  async destroy({ request, response, auth }) {
    const id = request.params.id;

    const cmsMenu = await CmsMenu.get(id);

    if (cmsMenu == null) {
      return response.notFound(this.api(ApiStatus.NOT_FOUND));
    }

    const success = await CmsMenuService.deleteNestedCmsMenu(cmsMenu, auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    await CmsMenuService.removeRedisMenuItems();

    return this.api(ApiStatus.OK, {
      refreshPage: true,
    });
  }

  async batchDelete({ request, response, auth }) {
    const validation = await ValidationHelper.validate(request.all(), {
      ids: "array|required|databaseRecord:cms_menus",
    });

    if (validation.fails()) {
      return response.badRequest(this.api(ApiStatus.BAD_REQUEST, validation.messages()));
    }

    const success = await CmsMenu.batchDelete(request.all().ids, auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    return this.api(ApiStatus.OK);
  }

  async upsert({ request, auth }) {
    const id = request.all().id;
    const cmsMenu = id == null ? new CmsMenu : await CmsMenu.get(id);

    const validation = await ValidationHelper.validate(request.all(), CmsMenu.validationRules(id));

    if (validation.fails()) {
      throw new ValidationException(validation, []);
    }

    try {
      await CmsMenuService.upsertCmsMenu(cmsMenu, request.all(), auth.user);
      await CmsMenuService.removeRedisMenuItems();
    } catch (e) {
      throw new FailedToUpdateRecordException(e);
    }

    return cmsMenu;
  }

}

module.exports = CmsMenuController;
