const Enum = require('enum');

const Status = new Enum(['NORMAL', 'ABNORMAL']);

module.exports = Status;
