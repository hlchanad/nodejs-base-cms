class ContextKey {}

ContextKey.Request_ID = "request-id";
ContextKey.CMS_User_Preferences = "cms-user-preferences";

module.exports = ContextKey;
