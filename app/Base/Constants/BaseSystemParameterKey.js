class BaseSystemParameterKey {}

BaseSystemParameterKey.CMS_MENUS_MAX_LEVELS = "cms-settings.cms-menus.max-levels";
BaseSystemParameterKey.CMS_DEFAULT_HOME_PAGE = "cms-settings.default-home-page";

module.exports = BaseSystemParameterKey;
