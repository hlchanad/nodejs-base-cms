class BaseLocaleGroupKey {}

BaseLocaleGroupKey.DatatableJS = "datatablesjs";
BaseLocaleGroupKey.DropzoneJS = "dropzonejs";
BaseLocaleGroupKey.Daterangepicker = "daterangepicker";

module.exports = BaseLocaleGroupKey;
