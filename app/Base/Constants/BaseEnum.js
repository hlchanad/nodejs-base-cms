class BaseEnum {

  constructor(_value, _localeKey) {
    this._value = _value;
    this._localeKey = _localeKey;
  }

  static values() {
    return [];
  }

  static valuesInString(delimiter = ",") {
    return this.values().map(value => value._value).join(delimiter);
  }

  static valueOf(_value) {
    return this.values().find(value => value._value === _value);
  }

}

module.exports = BaseEnum;
