const Language = use("App/Base/Models/Common/Language");

class CmsUserPreferenceKey {}

CmsUserPreferenceKey.CMS_Language = "cms.language";
CmsUserPreferenceKey.CMS_HOMEPAGE = "cms.homepage";

CmsUserPreferenceKey.validationRules = () => {
  const languageCodes = Language.getSupportedLanguages().map(language => language.dbCode).join(",");
  return {
    [CmsUserPreferenceKey.CMS_Language]: `string|in:${languageCodes}`,
    [CmsUserPreferenceKey.CMS_HOMEPAGE]: `string`,
  };
};

module.exports = CmsUserPreferenceKey;
