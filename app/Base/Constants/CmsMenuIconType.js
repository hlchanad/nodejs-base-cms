const Enum = require('enum');

const CmsMenuIconType = new Enum(['FONT_AWESOME', 'PAGES', 'TEXT']);

module.exports = CmsMenuIconType;
