class BaseRedisKey {}

BaseRedisKey.CMS_SIDEBAR_MENU = "cms-sidebar-menu";
BaseRedisKey.CMS_ROLE_PERMISSIONS = "cms-role-permissions";
BaseRedisKey.CMS_USER_PREFERENCES = "cms-user-preferences";

module.exports = BaseRedisKey;
