const Validator = use('Validator');

const I18nHelper = use("App/Base/Helpers/I18nHelper");

class ValidationHelper {

  static async validate (request, rules) {
    return Validator.validate(request, rules, this.getErrorMessages());
  }

  static getErrorMessages() {
    return this.getSupportedValidation()
      .reduce((acc, validation) => {
        acc[validation] = this.getErrorMessage;
        return acc;
      }, {});
  }

  static getSupportedValidation() {
    return [
      // default
      'email',
      'required',

      // custom
      'unique',
      'databaseRecord',
      'idOrImage',
      'latitude',
      'longitude',
      'dateTime',
      'date',
      'time',
    ];
  }

  static getErrorMessage(field, validation, args) {
    const props = { field: field };
    args.forEach((arg, index) => props[`arg${index}`] = arg);

    return I18nHelper.formatMessage(`validator.error-messages.${validation}`, props);
  }

  static getMockFailedValidation(rule, field, message) {
    message = message || I18nHelper.formatMessage(`validator.error-messages.${rule}`);
    return {
      messages: () => {
        return [ {
          message: message,
          field: field,
          validation: rule,
        } ];
      },
    };
  }

}

module.exports = ValidationHelper;
