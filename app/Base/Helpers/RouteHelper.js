
const Env = use('Env');

class RouteHelper {

  static getUrl(endpoint) {
    if (endpoint == null) return Env.get("APP_URL");

    if (endpoint.startsWith(Env.get("APP_CONTEXT_PATH"))) return Env.get("APP_DOMAIN") + endpoint;

    return Env.get("APP_URL") + endpoint;
  }

}

module.exports = RouteHelper;
