const randomString = require("randomstring");

const Env = use("Env");

class RequestIdHelper {

  static get length() {
    return parseInt(Env.get("REQUEST_ID_LENGTH", 8));
  }

  static generate() {
    return randomString.generate(this.length);
  }

}

module.exports = RequestIdHelper;
