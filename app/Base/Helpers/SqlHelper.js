const I18nHelper = use("App/Base/Helpers/I18nHelper");

class SqlHelper {

  static selectBoolean(field, schema = "t") {
    const trueString = I18nHelper.formatMessage("general.boolean.true");
    const falseString = I18nHelper.formatMessage("general.boolean.false");

    return `IF (${schema}.${field}, "${trueString}", "${falseString}")`;
  }

  static selectEnum(field, _enum, schema = "t") {
    const whens = _enum.values()
      .map(e => `WHEN '${e._value}' THEN '${I18nHelper.formatMessage(e._localeKey)}'`)
      .join(" ");

    return `CASE ${schema}.${field} ${whens} END`;
  }

}

module.exports = SqlHelper;
