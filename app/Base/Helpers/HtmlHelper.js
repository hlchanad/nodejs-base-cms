const I18nHelper = use("App/Base/Helpers/I18nHelper");

class HtmlHelper {

  static createRedirectActionButton(color, text, url) {
    return `<a class="btn btn-${color}" href="${url}">
              <span>${text}</span>
            </a>`;
  }

  static createAjaxActionButton(color, text, method, url) {
    return `<a class="btn btn-${color} action-ajax" href="#" data-method="${method}" data-href="${url}">
              <span>${text}</span>
            </a>`;
  }

  static createDeleteActionButton(url) {
    return `<a class="btn btn-danger action-delete" href="#" data-href="${url}">
              <span>${I18nHelper.formatMessage("general.delete")}</span>
            </a>`;
  }

  static createRedirectDetailButton(color, icon, url) {
    return `<a class="btn btn-${color}" href="${url}">
              <i class="${icon}"></i>
            </a>`;
  }

  static createDeleteDetailButton(deleteUrl, redirectUrl) {
    return `<a href="#" class="btn btn-danger detail-delete-button"
               data-href="${deleteUrl}" data-redirect="${redirectUrl}">
              <i class="fa fa-trash-o"></i>
            </a>`;
  }

}

module.exports = HtmlHelper;
