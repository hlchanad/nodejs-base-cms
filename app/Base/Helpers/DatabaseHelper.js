const Database = use("Database");
const Env = use("Env");

class DatabaseHelper {

  static async isTableExists(table) {
    const database = Env.get("DB_DATABASE");
    const result = await Database.raw(`
        SELECT EXISTS(
            SELECT 1
            FROM information_schema.tables
            WHERE table_schema='${database}'
              AND table_name='${table}') exist;`);

    return result[0][0]["exist"] === 1;
  }

  static async getTableColumns(table) {
    const database = Env.get("DB_DATABASE");
    const result = await Database.raw(`SHOW COLUMNS FROM ${database}.${table};`);
    return result[0];
  }

}

module.exports = DatabaseHelper;
