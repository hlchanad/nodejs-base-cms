const _ = require("lodash");

class CamsMenuOrganizer {

  constructor(cmsMenus) {
    this._cmsMenus = cmsMenus;
    this._levelDP = {}; // cmsMenuId -> level
    this._childrenLevelsDP = {}; // cmsMenuId -> number of level of children

    this._result = this._cmsMenus.map(cmsMenu => {
      return {
        cmsMenu: cmsMenu,
        level: null,
        childrenLevels: null,
        parent: null,
        children: null,
      };
    });

    this._cmsMenus.forEach((cmsMenu, index) => {
      this._result[index].level = this.getLevel(cmsMenu);
      this._result[index].childrenLevels = this.getChildrenLevels(cmsMenu);
      this._result[index].parent = this.findParent(cmsMenu);
      this._result[index].children = this.getChildren(cmsMenu);
    });
  }

  getLevel(cmsMenu) {
    if (this._levelDP[cmsMenu.id] != null) {
      return this._levelDP[cmsMenu.id];
    }

    const parent = this.findParent(cmsMenu);

    if (parent == null) {
      return this._levelDP[cmsMenu.id] = 1;
    }

    return this._levelDP[cmsMenu.id] = this.getLevel(parent) + 1;
  }

  getChildrenLevels(cmsMenu) {
    if (this._childrenLevelsDP[cmsMenu.id] != null) {
      return this._childrenLevelsDP[cmsMenu.id];
    }

    const children = this.findChildren(cmsMenu);

    if (children.length <= 0) {
      return this._childrenLevelsDP[cmsMenu.id] = 0;
    }

    const childrenLevels = children.map(child => this.getChildrenLevels(child));

    return this._childrenLevelsDP[cmsMenu.id] = _.maxBy(childrenLevels) + 1;
  }

  getChildren(cmsMenu) {
    const children = this.findChildren(cmsMenu);

    if (children.length <= 0) {
      return [];
    }

    return [
      ...children,
      ...children.map(child => this.getChildren(child)).flatten(),
    ];
  }


  findParent(target) {
    return this._cmsMenus.find(cmsMenu => cmsMenu.id === target.parent_id);
  }

  findChildren(target) {
    return this._cmsMenus.filter(cmsMenu => cmsMenu.parent_id === target.id);
  }

  getResult() {
    return this._result;
  }
}

module.exports = CamsMenuOrganizer;

