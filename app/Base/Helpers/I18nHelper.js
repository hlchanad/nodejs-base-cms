const _ = require("lodash");

const Antl = use("Antl");

const ContextKey = use("App/Base/Constants/ContextKey");
const CmsUserPreferenceKey = use("App/Base/Constants/CmsUserPreferenceKey");
const Language = use("App/Base/Models/Common/Language");

class I18nHelper {

  static formatMessage(key, props, forLocale) {
    return new I18n().formatMessage(key, props, forLocale);
  }

  static currentLocale() {
    return new I18n().currentLocale();
  }

  static currentLocaleDbCode() {
    return new I18n().currentLocaleDbCode();
  }

  static currentLocaleTitle() {
    return new I18n().currentLocaleTitle();
  }

  static async reloadLocalesCache() {
    await Antl.bootLoader();
  }

}

class I18n {

  constructor() {
    const context = use("Context");
    const cmsUserPreferences = context == null ? null : context.get(ContextKey.CMS_User_Preferences);

    this._languageCode = _.get(cmsUserPreferences, CmsUserPreferenceKey.CMS_Language, Language.English.dbCode);

    this._language = Language.of("dbCode", this._languageCode);

    this._locale = this._language.localeCode;
  }

  formatMessage(key, props, forLocale) {
    let locale = _.isEmpty(forLocale) ? this._locale : forLocale;

    try {
      return Antl.forLocale(locale).formatMessage(key, props);
    } catch (error) {
      return key;
    }
  }

  currentLocale() {
    return this._locale;
  }

  currentLocaleDbCode() {
    return this._languageCode;
  }

  currentLocaleTitle() {
    return this.formatMessage(this._language.title);
  }

}

module.exports = I18nHelper;
