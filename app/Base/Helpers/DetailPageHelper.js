const HtmlHelper = use("App/Base/Helpers/HtmlHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");

class DetailPageHelper {

  static async getDefaultDetailButtons (user, controller, section, model) {
    let detailButtonsHtml = "";

    if (await PermissionHelper.canAccess(user, controller, "index")) {
      detailButtonsHtml += HtmlHelper.createRedirectDetailButton("complete", "fa fa-list-ul",
        RouteHelper.getUrl(`/${section}`));
    }

    if (await PermissionHelper.canAccess(user, controller, "edit")) {
      detailButtonsHtml += HtmlHelper.createRedirectDetailButton("primary", "fa fa-pencil",
        RouteHelper.getUrl(`/${section}/${model.id}/edit`));
    }

    if (await PermissionHelper.canAccess(user, controller, "delete")) {
      detailButtonsHtml += HtmlHelper.createDeleteDetailButton(
        RouteHelper.getUrl(`/${section}/${model.id}`),
        RouteHelper.getUrl(`/${section}`));
    }

    return detailButtonsHtml;
  }

}

module.exports = DetailPageHelper;
