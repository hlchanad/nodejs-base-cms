const _ = require("lodash");

const Redis = use("Redis");

const RouteHelper = use("App/Base/Helpers/RouteHelper");
const BaseRedisKey = use("App/Base/Constants/BaseRedisKey");
const MenuItem = use("App/Base/Models/Common/MenuItem");
const CmsMenu = use("App/Base/Models/CmsMenu");
const CmsRoleMenu = use("App/Base/Models/CmsRoleMenu");
const CmsRoleCode = use("App/Base/Constants/CmsRoleCode");

class SidebarMenuHelper {

  static async getSidebarMenu(user, url) {

    if (user == null) return [];

    let menuItems = await Redis.hget(BaseRedisKey.CMS_SIDEBAR_MENU, user.id);

    if (menuItems != null) {
      menuItems = JSON.parse(menuItems);
    }
    else {
      const cmsMenus = await CmsMenu.getAll();
      const cmsRoleMenus = await CmsRoleMenu.getAll();
      const userRoles = await user.cmsRoles();

      const roleFilteredCmsMenus = this.filterByUserRole(cmsMenus, cmsRoleMenus, userRoles);

      menuItems = await this.getMenuItems(user, roleFilteredCmsMenus);

      await Redis.hset(BaseRedisKey.CMS_SIDEBAR_MENU, user.id, JSON.stringify(menuItems));
    }

    menuItems
      .filter(menuItem => this.checkIfActiveRoute(menuItem, url))
      .forEach(menuItem => menuItem.active = true);

    return menuItems;
  }

  static async getMenuItems(user, cmsMenus) {
    return Promise.all(cmsMenus
      .filter(cmsMenu => cmsMenu.parent_id == null) // Root level
      .sort((cmsMenu1, cmsMenu2) => cmsMenu1.sequence - cmsMenu2.sequence)
      .map(async cmsMenu => this.getMenuItem(user, cmsMenus, cmsMenu)));
  }

  static async getChildrenMenuItems(user, cmsMenus, parentId) {
    return Promise.all(cmsMenus
      .filter(cmsMenu => cmsMenu.parent_id != null) // Non-root level
      .filter(cmsMenu => cmsMenu.parent_id === parentId)
      .sort((cmsMenu1, cmsMenu2) => cmsMenu1.sequence - cmsMenu2.sequence)
      .map(async cmsMenu => this.getMenuItem(user, cmsMenus, cmsMenu)));
  }

  static async getMenuItem(user, cmsMenus, cmsMenu) {
    const url = cmsMenu.endpoint == null ? null : RouteHelper.getUrl(cmsMenu.endpoint);
    return new MenuItem(cmsMenu, url, await this.getChildrenMenuItems(user, cmsMenus, cmsMenu.id))
  }

  static checkIfActiveRoute(menuItem, url) {

    if (!_.isEmpty(menuItem.url)) {
      const fullUrl = RouteHelper.getUrl(url);

      const match = fullUrl.match(new RegExp(`^${menuItem.url}((\\/.+?)|(\\?.*?)|(#.*?))?$`));

      return !!match;
    }

    for (let i = 0; i < menuItem.children.length; i++) {
      const child = menuItem.children[i];
      if (this.checkIfActiveRoute(child, url)) {
        child.active = true;
        return true;
      }
    }

    return false;
  }

  static filterByUserRole(cmsMenus, cmsRoleMenus, userRoles) {
    const isRootRole = userRoles.find(role => role.code === CmsRoleCode.ROOT);

    if (isRootRole) return cmsMenus.rows;

    return cmsMenus.rows
      .filter(cmsMenu => {
        const userRoleIds = userRoles.map(role => role.id);

        const allowedRoleIds = cmsRoleMenus.rows
          .filter(cmsRoleMenu => cmsRoleMenu.cms_menu_id === cmsMenu.id)
          .map(cmsRoleMenu => cmsRoleMenu.cms_role_id);

        return allowedRoleIds.some(allowedRoleId => userRoleIds.includes(allowedRoleId));
      });
  }

}

module.exports = SidebarMenuHelper;
