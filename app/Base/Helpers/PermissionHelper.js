const Redis = use("Redis");

const BaseRedisKey = use("App/Base/Constants/BaseRedisKey");
const CmsRoleCode = use("App/Base/Constants/CmsRoleCode");
const CmsRole = use("App/Base/Models/CmsRole");
const CmsUser = use("App/Base/Models/CmsUser");

class PermissionHelper {

  static async canAccess(user, controller, method) {
    const roles = await user.cmsRoles();

    if (await this.isRootUser(roles)) {
      return true;
    }

    const roleIds = roles.map(role => role.id);
    const permissions = await this.getPermissions(roleIds);

    return permissions.some(permission => {
      const [ allowedController, allowedMethod ] = permission.split(".");
      return allowedController === controller
        && (allowedMethod === "*" || allowedMethod === method);
    });
  }

  static async isRootUser(userOrRoles) {
    let roles;

    if (userOrRoles instanceof CmsUser) {
      roles = await userOrRoles.cmsRoles();
    } else if (Array.isArray(userOrRoles) && userOrRoles.every(role => role instanceof CmsRole)) {
      roles = userOrRoles;
    } else {
      throw new Error("invalid argument, should be CmsUser or CmsRole[]");
    }

    return roles.some(role => role.code === CmsRoleCode.ROOT);
  }

  static async getPermissions(roleIds, updated = false) {
    const keys = await Redis.hkeys(BaseRedisKey.CMS_ROLE_PERMISSIONS);

    if (keys.length <= 0 && !updated) {
      await this.updateRedisRolePermissions();
      return this.getPermissions(roleIds, true);
    }

    const actionJSONs = await Promise.all(keys
      .filter(key => roleIds.map(roleId => roleId + "").includes(key))
      .map(async key => Redis.hget(BaseRedisKey.CMS_ROLE_PERMISSIONS, key)));

    return actionJSONs.map(json => JSON.parse(json)).flatten();
  }

  static async updateRedisRolePermissions() {
    await this.removeRedisRolePermissions();

    const rolePermissions = await this.getRolePermissions();

    const args = Object.entries(rolePermissions)
      .map(entry => [ entry[0], JSON.stringify(entry[1]) ])
      .flatten();

    if (args.length <= 0) return ;

    await Redis.hset(BaseRedisKey.CMS_ROLE_PERMISSIONS, ...args);
  }

  static async removeRedisRolePermissions() {
    const keys = await Redis.hkeys(BaseRedisKey.CMS_ROLE_PERMISSIONS);
    await Promise.all(keys.map(key => Redis.hdel(BaseRedisKey.CMS_ROLE_PERMISSIONS, key)));
  }

  static async getRolePermissions() {
    const cmsRoles = await CmsRole.getAll();

    const rolePermissions = await Promise.all(cmsRoles.rows.map(async cmsRole => {
      return {
        role: cmsRole,
        permissions: await cmsRole.cmsPermissions(),
      };
    }));

    return rolePermissions
      .map(rolePermission => {
        return {
          roleId: rolePermission.role.id,
          actions: rolePermission.permissions.map(permission => permission.action),
        };
      })
      .reduce((acc, rolePermission) => {
        acc[rolePermission.roleId] = rolePermission.actions;
        return acc;
      }, {});
  }

}

module.exports = PermissionHelper;
