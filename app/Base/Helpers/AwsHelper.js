const AWS = require("aws-sdk");

const Env = use("Env");

class AwsHelper {

  static get Translate() {
    return new AWS.Translate({
      accessKeyId: Env.get("AWS_TRANSLATE_ACCESS_KEY"),
      secretAccessKey: Env.get("AWS_TRANSLATE_SECRET_KEY"),
      region: Env.get("AWS_REGION"),
    });
  }

  static async translate(sourceLocale, targetLocale, text, terminologies = []) {
    return new Promise(((resolve, reject) => {
      this.Translate.translateText({
        SourceLanguageCode: sourceLocale, /* required */
        TargetLanguageCode: targetLocale, /* required */
        Text: text,                       /* required */
        TerminologyNames: terminologies,
      }, function(err,  data) {
        return err ? reject(err) : resolve(data.TranslatedText);
      });
    }));
  }

}

module.exports = AwsHelper;
