const SqlString = require("sqlstring");

const Database = use("Database");

class DatatableHelper {

  constructor(columns, sql, request) {
    this.columns = columns;
    this.sql = sql;
    this.request = request;
  }

  async getRecordsTotal() {
    const total = await Database.raw(`
      SELECT count(1) AS count FROM (${this.sql}) wrap`);
    return total[0][0].count;
  }

  async getRecordsFiltered() {
    const filtered = await Database.raw(`
      SELECT count(1) AS count FROM (${this.sql}) wrap WHERE ${this.like()}`);
    return filtered[0][0].count;
  }

  async getData() {
    const data = await Database.raw(`
      SELECT *
      FROM (${this.sql}) wrap
      WHERE ${this.like()}
      ORDER BY ${this.orderBy()}
      LIMIT ${this.limit()}
      OFFSET ${this.offset()}
    `);
    return data[0];
  }

  like() {
    const likeString = SqlString.escape("%" + this.request.search.value + "%");
    return this.columns
      .map(column => `LOWER(\`${column}\`) LIKE BINARY LOWER(${likeString})`)
      .join(" OR ");
  }

  orderBy() {
    const column = this.columns[this.request.order[0].column - 1]; // -1 is for excluding the checkbox column
    const direction = this.request.order[0].dir;
    return `\`${column}\` ${direction}`;
  }

  limit() {
    return this.request.length;
  }

  offset() {
    return this.request.start;
  }

}

module.exports = DatatableHelper;
