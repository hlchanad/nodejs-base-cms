
/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

const Status = use("App/Base/Constants/Status");

class BaseSchema extends Schema {

  modifyTableFunctions(table) {
    const knex = this.db.knex;

    table.id = function() {
      this.bigIncrements('id').primary();
    };

    table.boolean = function(name) {
      return this.bit(name, 1).defaultTo(knex.raw('0'));
    };

    table.refId = function(mainTable) {
      return this.foreignKey("ref_id", mainTable, "id").notNullable();
    };

    table.language = function() {
      return this.string("language").notNullable();
    };

    table.latitude = function(name) {
      return this.decimal(name, 10, 8);
    };

    table.longitude = function(name) {
      return this.decimal(name, 11, 8);
    };

    table.money = function(name) {
      // according to https://en.wikipedia.org/wiki/Generally_Accepted_Accounting_Principles_(United_States)
      return this.decimal(name, 13, 4);
    };

    /**
     *
     * @param localField
     * @param foreignTable
     * @param foreignField
     * @param onDelete: RESTRICT, CASCADE, SET NULL, NO ACTION
     * @param onUpdate: RESTRICT, CASCADE, SET NULL, NO ACTION
     * @returns {*}
     */
    table.foreignKey = function(localField,
                                foreignTable,
                                foreignField,
                                onDelete = 'NO ACTION',
                                onUpdate = 'NO ACTION') {
      return this
        .bigInteger(localField)
        .unsigned()
        .references(foreignField)
        .inTable(foreignTable)
        .onDelete(onDelete)
        .onUpdate(onUpdate);
    };

    table.imageId = function(name) {
      return this.foreignKey(name, 'images', 'id');
    };

    table.manageable = function(statusEnums= Status.enums, foreignKeyToCmsUser = true) {
      this.enu('status', statusEnums).notNullable();
      this.boolean('is_visible').notNullable();
      this.boolean('is_deleted').notNullable();
      this.datetime("created_at").defaultTo(knex.fn.now()).notNullable();
      this.datetime("updated_at").defaultTo(knex.fn.now()).notNullable();
      this.datetime("deleted_at");

      if (foreignKeyToCmsUser) {
        this.foreignKey('created_by', 'cms_users', 'id');
        this.foreignKey('updated_by', 'cms_users', 'id');
        this.foreignKey("deleted_by", "cms_users", "id");
      } else {
        this.bigInteger('created_by').unsigned();
        this.bigInteger('updated_by').unsigned();
        this.bigInteger('deleted_by').unsigned();
      }
    };

    return table;
  }

  create(schema, tableCallback) {
    return super.create(schema, (table) => {
      this.modifyTableFunctions(table);
      tableCallback(table);
    });
  }

  table(schema, tableCallback) {
    return super.table(schema, (table) => {
      this.modifyTableFunctions(table);
      tableCallback(table);
    });
  }

}

module.exports = BaseSchema;
