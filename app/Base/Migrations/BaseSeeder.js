const kleur = require("kleur");

class BaseSeeder {

  constructor() {
    this.kleur = kleur;
    this.filename = require('path').basename(__filename);
  }

  async run () {
    await this.seed();
    console.log(this.kleur.green("seed: ") + this.filename);
  }

  async seed () {}

}

module.exports = BaseSeeder;
