"use strict";

const _ = require("lodash");
const uuid = require("uuid/v4");
const imageSizeOf = require("image-size");

const Env = use("Env");
const Helpers = use("Helpers");

const BaseService = use("App/Base/Services/BaseService");
const StorageService = use("App/Base/Services/StorageService");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const Image = use("App/Base/Models/Image");

class ImageService extends BaseService {

  static get MAX_SIZE() {
    return Env.get("IMAGE_UPLOAD_MAX_SIZE");
  }

  static validationRules() {
    return {
      types: [ "image" ],
      size: this.MAX_SIZE,
      // extnames: [ "png", "gif" ],
    };
  }

  static async uploadImage(request, field, folder, user, options) {
    options = {
      required: _.get(options, "required", true),
      isPublic: _.get(options, "isPublic", true),
      overwrite: _.get(options, "overwrite", false),
      name: _.get(options, "name"),
    };

    const file = request.file(field, this.validationRules());

    if (file == null) {
      return null;
    }

    await this.moveToTempFolder(file, field, folder, options);

    const dimensions = imageSizeOf(file._location + "/" + file.fileName);

    await this.uploadToStorageSystem(file, folder, options);

    const image = await this.insertImage(file, folder, dimensions, user, options);

    return image;
  }

  static async getImageUrl(imageIdOrImage) {
    const image = imageIdOrImage instanceof Image ? imageIdOrImage : await Image.get(imageIdOrImage);

    if (image == null) {
      return null;
    }

    return await StorageService.getFileUrl(`images/${image.path}/${image.file_name}`, image.public, image.disk);
  }

  static async moveToTempFolder(file, field, folder, options) {
    const defaultFileName = `${new Date().getTime()}-${uuid()}.${file.subtype}`;

    await file.move(Helpers.tmpPath(`uploads/images/${folder}`), {
      name: options.name || defaultFileName,
      overwrite: true,
    });

    if (!file.moved()) {
      // should not reach this line coz there should be validated before
      const rule = file.error().type;
      const message = I18nHelper.formatMessage(`validator.error-messages.file-${rule}`, { size: this.MAX_SIZE });
      throw ValidationHelper.getMockFailedValidation(`file-${rule}`, field, message);
    }

    return file;
  }

  static async uploadToStorageSystem(file, folder, options) {
    const path = StorageService.getPath(`images/${folder}/${file.fileName}`, options.isPublic);
    const exists = await StorageService.exists(path);

    if (!exists || options.overwrite) {
      const tempPath = file._location + "/" + file.fileName;
      const stream = await StorageService.local.getStream(tempPath);

      await StorageService.put(path, stream, options.isPublic);

      await StorageService.local.delete(tempPath);
    }
  }

  static async insertImage(file, folder, dimensions, user, options) {
    const image = new Image();
    image.path = folder;
    image.file_name = file.fileName;
    image.original_file_name = file.clientName;
    image.mime_type = `${file.type}/${file.subtype}`;
    image.width = dimensions.width;
    image.height = dimensions.height;
    image.file_size = file.size;
    image.disk = StorageService.disk;
    image.public = options.isPublic;
    await image.save(user);
    return image;
  }

  static async upsertImages(relationImages, requestImageIds, Model, modelIdField, modelId, user) {
    requestImageIds = (Array.isArray(requestImageIds) ? requestImageIds : [ requestImageIds ])
      .map(id => parseInt(id)).filter(id => !isNaN(id));
    const existingImageIds = relationImages.rows.map(relationImage => relationImage.image_id);

    const toBeAddedImageIds = requestImageIds.filter(imageId => !existingImageIds.includes(imageId));
    const toBeDeletedImageIds = existingImageIds.filter(imageId => !requestImageIds.includes(imageId));

    await Promise.all(toBeAddedImageIds
      .map(imageId => {
        const model = new Model();
        model[modelIdField] = modelId;
        model.image_id = imageId;
        return model.save(user);
      }));

    await Promise.all(toBeDeletedImageIds
      .map(imageId => relationImages.rows.find(relationImage => relationImage.image_id === imageId))
      .map(relationImage => relationImage.delete(user)));
  }
}

module.exports = ImageService;
