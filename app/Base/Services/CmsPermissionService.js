"use strict";

const Route = use("Route");

const BaseService = use("App/Base/Services/BaseService");
const SelectOption = use("App/Base/Models/Common/SelectOption");
const CmsPermission = use("App/Base/Models/CmsPermission");

class CmsPermissionService extends BaseService {

  static async upsertCmsPermission(cmsPermission, request, user) {
    cmsPermission.title = request.title;
    cmsPermission.action = request.action;
    await cmsPermission.save(user);
  }

  static async getActionOptions(id) {
    const cmsPermissions = await CmsPermission.getAll();
    const routes = Route.list();

    const controllerActions = routes
      .map(route => route.handler)
      .map(handler => this.getController(handler))
      .distinct()
      .map(controller => controller + ".*");

    const routeActions = routes
      .map(route => route.handler)
      .map(handler => this.simplifyHandler(handler));

    return [
      ...controllerActions,
      ...routeActions,
    ]
      .sort(this.sortActions)
      .filter(handler => !cmsPermissions.rows
        .filter(cmsPermission => cmsPermission.id !== id)
        .map(cmsPermission => cmsPermission.action)
        .some(action => action === handler))
      .map((handler, index) => SelectOption.builder()
        .id("action-" + index)
        .title(handler)
        .value(handler)
        .build());
  }

  static simplifyHandler(handler) {
    return handler.substr(handler.lastIndexOf("/") + 1);
  }

  static getController(handler) {
    const action = this.simplifyHandler(handler);
    return action.substr(0, action.indexOf("."));
  }

  static sortActions(action1, action2) {
    return action1.localeCompare(action2);
  }

}

module.exports = CmsPermissionService;
