const BaseService = use("App/Base/Services/BaseService");
const CmsRole = use("App/Base/Models/CmsRole");
const CmsRolePermission = use("App/Base/Models/CmsRolePermission");
const CmsPermission = use("App/Base/Models/CmsPermission");
const CmsRoleCode = use("App/Base/Constants/CmsRoleCode");
const SelectOption = use("App/Base/Models/Common/SelectOption");
const HtmlHelper = use("App/Base/Helpers/HtmlHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");

class CmsRoleService extends BaseService {

  static async upsertCmsRole(cmsRole, request, user) {
    cmsRole.code = request.code;
    cmsRole.title = request.title;
    cmsRole.description = request.description;
    cmsRole.selectable = true;
    await cmsRole.save(user);

    const cmsRolePermissions = await cmsRole.cmsRolePermissions().fetch();
    const requestPermissionIds = (Array.isArray(request.permission_ids) ? request.permission_ids : [request.permission_ids])
      .map(id => parseInt(id)).filter(id => !isNaN(id));
    const existingPermissionIds = cmsRolePermissions.rows.map(cmsRolePermission => cmsRolePermission.cms_permission_id);

    const toBeAddedPermissionIds = requestPermissionIds.filter(permissionId => !existingPermissionIds.includes(permissionId));
    const toBeDeletedPermissionIds = existingPermissionIds.filter(permissionId => !requestPermissionIds.includes(permissionId));

    await Promise.all(toBeAddedPermissionIds
      .map(permissionId => {
        const cmsRolePermission = new CmsRolePermission();
        cmsRolePermission.cms_role_id = cmsRole.id;
        cmsRolePermission.cms_permission_id = permissionId;
        return cmsRolePermission.save(user);
      }));

    await Promise.all(toBeDeletedPermissionIds
      .map(permissionId => cmsRolePermissions.rows.find(cmsRolePermission => cmsRolePermission.cms_permission_id === permissionId))
      .map(cmsRolePermission => cmsRolePermission.delete(user)));
  }

  static async getDefaultDetailButtons (user, controller, section, model) {
    const isRootRole = model.code === CmsRoleCode.ROOT;

    let detailButtonsHtml = "";

    if (await PermissionHelper.canAccess(user, controller, "index")) {
      detailButtonsHtml += HtmlHelper.createRedirectDetailButton("complete", "fa fa-list-ul",
        RouteHelper.getUrl(`/${section}`));
    }

    if (!isRootRole && await PermissionHelper.canAccess(user, controller, "edit")) {
      detailButtonsHtml += HtmlHelper.createRedirectDetailButton("primary", "fa fa-pencil",
        RouteHelper.getUrl(`/${section}/${model.id}/edit`));
    }

    if (!isRootRole && await PermissionHelper.canAccess(user, controller, "delete")) {
      detailButtonsHtml += HtmlHelper.createDeleteDetailButton(
        RouteHelper.getUrl(`/${section}/${model.id}`),
        RouteHelper.getUrl(`/${section}`));
    }

    return detailButtonsHtml;
  }

  static async getRoleOptions(id) {
    const cmsRoles = await CmsRole.getAll();

    return cmsRoles.rows
      .sort((a, b) => a.title.localeCompare(b.title))
      .map(cmsRole => SelectOption.builder()
        .id("roleId-" + cmsRole.id)
        .title(cmsRole.title)
        .value(cmsRole.id)
        .build());
  }

  static async getPermissionOptions(id) {
    const cmsPermissions = await CmsPermission.getAll();

    return cmsPermissions.rows
      .sort((a, b) => a.title.localeCompare(b.title))
      .map(cmsPermission => SelectOption.builder()
        .id("permissionId-" + cmsPermission.id)
        .title(cmsPermission.title)
        .value(cmsPermission.id)
        .build());
  }

  static async allRolesExist(cmsRoleIds) {
    const count = await CmsRole.query()
      .whereIn("id", cmsRoleIds)
      .where("is_deleted", false)
      .where("is_visible", true)
      .where("selectable", true)
      .getCount();

    return cmsRoleIds.length === count;
  }

}

module.exports = CmsRoleService;
