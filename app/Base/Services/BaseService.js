class BaseService {

  static async upsertRelations(options) {
    const { relations, Relation, mainIdField, foreignIdField, mainId, foreignIds, user, relationExtraFields = {} } = options;

    const requestForeignIds = (Array.isArray(foreignIds) ? foreignIds : [ foreignIds ])
      .map(id => parseInt(id)).filter(id => !isNaN(id));
    const existingForeignIds = relations.rows.map(relation => relation[foreignIdField]);

    const toBeAddedForeignIds = requestForeignIds.filter(foreignId => !existingForeignIds.includes(foreignId));
    const toBeDeletedForeignIds = existingForeignIds.filter(foreignId => !requestForeignIds.includes(foreignId));

    await Promise.all(toBeAddedForeignIds
      .map(foreignId => {
        const relation = new Relation();
        relation[mainIdField] = mainId;
        relation[foreignIdField] = foreignId;
        Object.entries(relationExtraFields).forEach(entry => relation[entry[0]] = entry[1]);
        return relation.save(user);
      }));

    await Promise.all(toBeDeletedForeignIds
      .map(foreignId => relations.rows.find(relation => relation[foreignIdField] === foreignId))
      .map(relation => relation.delete(user)));
  }

}

module.exports = BaseService;
