"use strict";

const _ = require("lodash");

const Redis = use("Redis");

const BaseService = use("App/Base/Services/BaseService");
const BaseRedisKey = use("App/Base/Constants/BaseRedisKey");
const CmsUserPreference = use("App/Base/Models/CmsUserPreference");

class CmsUserPreferenceService extends BaseService {

  static async getCmsUserPreference(cmsUser, key, defaultValue = "", setDefaultValueIfMissing = false) {
    const preferences = JSON.parse(await Redis.hget(BaseRedisKey.CMS_USER_PREFERENCES, cmsUser.id));

    let value;
    if (preferences == null || preferences.key == null) {
      const [ cmsUserPreference ] = await cmsUser.preferences([ key ]);

      if (cmsUserPreference == null) {
        value = defaultValue;

        if (setDefaultValueIfMissing && !_.isEmpty(defaultValue)) {
          await this.updateCmsUserPreference(cmsUser, key, value);
        }
      } else {
        value = cmsUserPreference.value;

        await this.updateCmsUserPreferenceToRedis(cmsUser, key, value);
      }
    } else {
      value = preferences[key];
    }

    if (value == null) {
      value = defaultValue;

      if (setDefaultValueIfMissing) {
        await this.updateCmsUserPreference(cmsUser, key, value);
      }
    }

    return value;
  }

  static async updateCmsUserPreference(cmsUser, key, value) {
    await this.updateCmsUserPreferenceToDatabase(cmsUser, key, value);

    await this.updateCmsUserPreferenceToRedis(cmsUser, key, value);
  }

  static async updateCmsUserPreferenceToDatabase(cmsUser, key, value) {
    let [ cmsUserPreference ] = await cmsUser.preferences([ key ]);

    if (cmsUserPreference == null) {
      cmsUserPreference = new CmsUserPreference();
      cmsUserPreference.cms_user_id = cmsUser.id;
      cmsUserPreference.key = key;
    }

    cmsUserPreference.value = value;
    await cmsUserPreference.save(cmsUser);
  }

  static async updateCmsUserPreferenceToRedis(cmsUser, key, value) {
    const preferences = {
      ...JSON.parse(await Redis.hget(BaseRedisKey.CMS_USER_PREFERENCES, cmsUser.id)),
      [key]: value,
    };

    await Redis.hset(BaseRedisKey.CMS_USER_PREFERENCES, cmsUser.id, JSON.stringify(preferences));
  }

}

module.exports = CmsUserPreferenceService;
