const RouteHelper = use("App/Base/Helpers/RouteHelper");

const BaseSystemParameterKey = use("App/Base/Constants/BaseSystemParameterKey");
const CmsUserPreferenceKey = use("App/Base/Constants/CmsUserPreferenceKey");
const SystemParameter = use("App/Base/Models/SystemParameter");

class LoginService {

  static get LastRouteValidTime() {
    return 5 * 60 * 1000;
  }

  static async getRedirectRoute(user, lastRoute) {
    let endpoint;

    const excludedRoutes = [
      "/",
      "/login",
    ];

    if (!excludedRoutes.includes(lastRoute.url)
      && lastRoute.t + this.LastRouteValidTime >= new Date().getTime()) {
      endpoint = lastRoute.url;
    } else {
      const [ homepage ] = await user.preferences(CmsUserPreferenceKey.CMS_HOMEPAGE);
      if (homepage != null) {
        endpoint = homepage.value;
      } else {
        endpoint = await SystemParameter.getConfig(BaseSystemParameterKey.CMS_DEFAULT_HOME_PAGE);
      }
    }

    return RouteHelper.getUrl(endpoint);
  }

}

module.exports = LoginService;
