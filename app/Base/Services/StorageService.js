"use strict";

const Env = use("Env");
const Drive = use("Drive");
const Helpers = use("Helpers");

const BaseService = use("App/Base/Services/BaseService");
const RouteHelper = use("App/Base/Helpers/RouteHelper");

class StorageService extends BaseService {

  static get Drive() {
    return Drive.disk(this.disk);
  }

  static get disk() {
    return Env.get("STORAGE_DRIVER");
  }

  static get localPublicPath() {
    const env = Env.get("STORAGE_LOCAL_ROOT_PUBLIC", Helpers.publicPath("uploads"));
    return env.startsWith("/") ? env : Helpers.appRoot(env);
  }

  static get localPrivatePath() {
    const env = Env.get("STORAGE_LOCAL_ROOT_PRIVATE", "storage");
    return env.startsWith("/") ? env : Helpers.appRoot(env);
  }

  static get localPublicUrl() {
    return Env.get("STORAGE_LOCAL_PUBLIC_URL");
  }

  static getPath(path, isPublic = true) {
    if (this.disk === "local") {
      if (isPublic) {
        return `${this.localPublicPath}/${path}`;
      } else {
        return `${this.localPrivatePath}/${path}`;
      }
    } else {
      return path;
    }
  }

  static async getFileUrl(path, isPublic = true, disk = "local") {
    if (disk === "local") {
      if (isPublic) {
        return `${this.localPublicUrl}/${path}`;
      } else {
        return RouteHelper.getUrl(`/${path}`);
      }
    } else {
      if (isPublic) {
        return this[disk].getUrl(path);
      } else {
        return this[disk].getSignedUrl(path);
      }
    }
  }

  static async exists(relativePath) {
    return this.Drive.exists(relativePath);
  }

  static async getStream(relativePath) {
    return this.Drive.getStream(relativePath);
  }

  static async put(relativePath, content, isPublic, options) {
    return this[this.disk].put(relativePath, content, isPublic, options);
  }

  static async delete(relativePath) {
    return this.Drive.delete(relativePath);
  }

  static async getUrl(location, bucket) {
    return this.Drive.getUrl(location, bucket);
  }

  static async getSignedUrl(location, expiry = 900, params) {
    return this.Drive.getSignedUrl(location, expiry, params);
  }

}

StorageService.Default = class extends StorageService {
};

StorageService.local = class extends StorageService {
  static get disk() {
    return "local";
  }

  static async put(relativePath, content, isPublic, options) {
    return this.Drive.put(relativePath, content, options);
  }
};

StorageService.s3 = class extends StorageService {
  static get disk() {
    return "s3";
  }

  static async put(relativePath, content, isPublic, options) {
    const acl = isPublic ? "public-read" : "authenticated-read";
    return this.Drive.put(relativePath, content, {
      ...options,
      ACL: acl,
    });
  }
};

module.exports = StorageService;
