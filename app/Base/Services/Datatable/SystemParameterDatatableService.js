const Env = use("Env");

const BaseDatatableService = use("App/Base/Services/Datatable/BaseDatatableService");
const DatatableColumn = use("App/Base/Models/Common/DatatableColumn");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const HtmlHelper = use("App/Base/Helpers/HtmlHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");

class SystemParameterDatatableService extends BaseDatatableService {

  static controller() {
    return "SystemParameterController";
  }

  static section() {
    return "system-parameters";
  }

  static datatableColumns() {
    return [
      new DatatableColumn("id", I18nHelper.formatMessage("system-parameters.fields.id")),
      new DatatableColumn("key", I18nHelper.formatMessage("system-parameters.fields.key")),
      new DatatableColumn("value", I18nHelper.formatMessage("system-parameters.fields.value")),
      new DatatableColumn("description", I18nHelper.formatMessage("system-parameters.fields.description")),
      new DatatableColumn("actionButtons", I18nHelper.formatMessage("general.index.datatable.action-buttons"), false),
    ];
  }

  static sql(request, user) {
    return `
      SELECT
        t.*,
        null AS actionButtons
      FROM system_parameters t
      WHERE t.is_deleted = false
        AND t.is_visible = true
    `;
  }

  static async handleDatatableResult(records, user) {
    const controller = this.controller();
    const section = this.section();

    return await Promise.all(records.map(async record => {

      const permissions = {};

      let actionButtonsHtml = `<div class="btn-group">`;

      if (await PermissionHelper.canAccess(user, controller, "show")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}`);

        permissions.show = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_DETAIL_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "success",
            I18nHelper.formatMessage("general.detail"),
            href);
        }
      }

      if (await PermissionHelper.canAccess(user, controller, "edit")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}/edit`);

        permissions.edit = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_EDIT_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "primary",
            I18nHelper.formatMessage("general.edit"),
            href);
        }
      }

      actionButtonsHtml += `</div>`;

      return {
        ...this.filterRecordKeys(record),
        _permissions: permissions,
        actionButtons: actionButtonsHtml,
      };
    }));
  }

}

module.exports = SystemParameterDatatableService;
