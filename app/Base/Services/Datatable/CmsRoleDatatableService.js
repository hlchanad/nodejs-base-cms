const Env = use("Env");

const BaseDatatableService = use("App/Base/Services/Datatable/BaseDatatableService");
const DatatableColumn = use("App/Base/Models/Common/DatatableColumn");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const HtmlHelper = use("App/Base/Helpers/HtmlHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");
const CmsRoleCode = use("App/Base/Constants/CmsRoleCode");

class CmsRoleDatatableService extends BaseDatatableService {

  static controller() {
    return "CmsRoleController";
  }

  static section() {
    return "cms-roles";
  }

  static datatableColumns() {
    return [
      new DatatableColumn("id", I18nHelper.formatMessage("cms-roles.fields.id")),
      new DatatableColumn("code", I18nHelper.formatMessage("cms-roles.fields.code")),
      new DatatableColumn("title", I18nHelper.formatMessage("cms-roles.fields.title")),
      new DatatableColumn("description", I18nHelper.formatMessage("cms-roles.fields.description")),
      new DatatableColumn("actionButtons", I18nHelper.formatMessage("general.index.datatable.action-buttons"), false),
    ];
  }

  static sql(request, user) {
    return `
      SELECT
        t.*,
        null AS actionButtons
      FROM cms_roles t
      WHERE t.is_deleted = false
        AND t.is_visible = true
    `;
  }

  static async handleDatatableResult(records, user) {
    const controller = this.controller();
    const section = this.section();

    return await Promise.all(records.map(async record => {

      const isRootRole = record.code === CmsRoleCode.ROOT;

      const permissions = {};

      let actionButtonsHtml = `<div class="btn-group">`;

      if (await PermissionHelper.canAccess(user, controller, "show")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}`);

        permissions.show = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_DETAIL_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "success",
            I18nHelper.formatMessage("general.detail"),
            href);
        }
      }

      if (!isRootRole && await PermissionHelper.canAccess(user, controller, "edit")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}/edit`);

        permissions.edit = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_EDIT_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "primary",
            I18nHelper.formatMessage("general.edit"),
            href);
        }
      }

      if (!isRootRole && await PermissionHelper.canAccess(user, controller, "delete")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}`);

        permissions.destroy = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_DELETE_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createDeleteActionButton(href);
        }
      }

      actionButtonsHtml += `</div>`;

      return {
        ...this.filterRecordKeys(record),
        _permissions: permissions,
        actionButtons: actionButtonsHtml,
      };
    }));
  }

}

module.exports = CmsRoleDatatableService;
