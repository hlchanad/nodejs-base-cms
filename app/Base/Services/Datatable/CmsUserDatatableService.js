const BaseDatatableService = use("App/Base/Services/Datatable/BaseDatatableService");
const DatatableColumn = use("App/Base/Models/Common/DatatableColumn");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class CmsUserDatatableService extends BaseDatatableService {

  static controller() {
    return "CmsUserController";
  }

  static section() {
    return "cms-users";
  }

  static datatableColumns() {
    return [
      new DatatableColumn("id", I18nHelper.formatMessage("cms-users.fields.id")),
      new DatatableColumn("username", I18nHelper.formatMessage("cms-users.fields.username")),
      new DatatableColumn("email", I18nHelper.formatMessage("cms-users.fields.email")),
      new DatatableColumn("roles", I18nHelper.formatMessage("cms-users.fields.role_ids")),
      new DatatableColumn("actionButtons", I18nHelper.formatMessage("general.index.datatable.action-buttons"), false),
    ];
  }

  static sql(request, user) {
    return `
      SELECT
        t.*,
        (
          SELECT GROUP_CONCAT(cr.title ORDER BY cr.title ASC SEPARATOR ', ' )
          FROM cms_user_roles cur
          LEFT JOIN cms_roles cr on cur.cms_role_id = cr.id
          WHERE cur.is_deleted = false
            AND cur.is_visible = true
            AND cr.is_deleted = false
            AND cr.is_visible = true
            AND cur.cms_user_id = t.id
          GROUP BY cur.cms_user_id
        ) roles,
        null AS actionButtons
      FROM cms_users t
      WHERE t.is_deleted = false
        AND t.is_visible = true
    `;
  }

}

module.exports = CmsUserDatatableService;
