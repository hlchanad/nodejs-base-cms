const _ = require("lodash");
const SqlString = use("sqlstring");

const Env = use("Env");

const BaseDatatableService = use("App/Base/Services/Datatable/BaseDatatableService");
const DatatableColumn = use("App/Base/Models/Common/DatatableColumn");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const HtmlHelper = use("App/Base/Helpers/HtmlHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");
const CmsMenuService = use("App/Base/Services/CmsMenuService");

class CmsMenuDatatableService extends BaseDatatableService {

  static controller() {
    return "CmsMenuController";
  }

  static section() {
    return "cms-menus";
  }

  static datatableColumns() {
    return [
      new DatatableColumn("id", I18nHelper.formatMessage("cms-menus.fields.id")),
      new DatatableColumn("title", I18nHelper.formatMessage("cms-menus.fields.title")),
      new DatatableColumn("endpoint", I18nHelper.formatMessage("cms-menus.fields.endpoint")),
      new DatatableColumn("icon_type", I18nHelper.formatMessage("cms-menus.fields.icon_type")),
      new DatatableColumn("icon", I18nHelper.formatMessage("cms-menus.fields.icon")),
      new DatatableColumn("sequence", I18nHelper.formatMessage("cms-menus.fields.sequence")),
      new DatatableColumn("actionButtons", I18nHelper.formatMessage("general.index.datatable.action-buttons"), false),
    ];
  }

  static sql(request, user) {
    return `
      SELECT
        t.*,
        null AS actionButtons
      FROM cms_menus t
      WHERE t.is_deleted = false
        AND t.is_visible = true
        AND t.parent_id ${_.isEmpty(request.parent_id) ? "IS null" : " = " + SqlString.escape(request.parent_id)}
    `;
  }

  static async handleDatatableResult(records, user) {
    const controller = this.controller();
    const section = this.section();

    return await Promise.all(records.map(async record => {

      const iconType = I18nHelper.formatMessage("cms-menus.fields.icon_type." + _.kebabCase(record.icon_type));

      const permissions = {};

      let actionButtonsHtml = `<div class="btn-group">`;

      if (await PermissionHelper.canAccess(user, controller, "show")) {
        if (!await CmsMenuService.isMaxCmsMenuLevels(record)) {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "warning",
            I18nHelper.formatMessage("cms-menus.view-children"),
            RouteHelper.getUrl(`/${section}?parent_id=${record.id}`));
        }
      }

      if (await PermissionHelper.canAccess(user, controller, "show")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}`);

        permissions.show = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_DETAIL_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "success",
            I18nHelper.formatMessage("general.detail"),
            href);
        }
      }

      if (await PermissionHelper.canAccess(user, controller, "edit")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}/edit`);

        permissions.edit = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_EDIT_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "primary",
            I18nHelper.formatMessage("general.edit"),
            href);
        }
      }

      if (await PermissionHelper.canAccess(user, controller, "delete")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}`);

        permissions.destroy = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_DELETE_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createDeleteActionButton(href);
        }
      }

      actionButtonsHtml += `</div>`;

      return {
        ...this.filterRecordKeys(record),
        _permissions: permissions,
        actionButtons: actionButtonsHtml,
        icon_type: iconType,
      };
    }));
  }

}

module.exports = CmsMenuDatatableService;
