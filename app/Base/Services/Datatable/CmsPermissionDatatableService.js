const BaseDatatableService = use("App/Base/Services/Datatable/BaseDatatableService");
const DatatableColumn = use("App/Base/Models/Common/DatatableColumn");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class CmsPermissionDatatableService extends BaseDatatableService {

  static controller() {
    return "CmsPermissionController";
  }

  static section() {
    return "cms-permissions";
  }

  static datatableColumns() {
    return [
      new DatatableColumn("id", I18nHelper.formatMessage("cms-permissions.fields.id")),
      new DatatableColumn("title", I18nHelper.formatMessage("cms-permissions.fields.title")),
      new DatatableColumn("action", I18nHelper.formatMessage("cms-permissions.fields.action")),
      new DatatableColumn("actionButtons", I18nHelper.formatMessage("general.index.datatable.action-buttons"), false),
    ];
  }

  static sql(request, user) {
    return `
      SELECT
        t.*,
        null AS actionButtons
      FROM cms_permissions t
      WHERE t.is_deleted = false
        AND t.is_visible = true
    `;
  }

}

module.exports = CmsPermissionDatatableService;
