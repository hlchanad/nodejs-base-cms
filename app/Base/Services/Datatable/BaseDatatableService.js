const Env = use("Env");

const SqlHelper = use("App/Base/Helpers/SqlHelper");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const DatatableHelper = use("App/Base/Helpers/DatatableHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const HtmlHelper = use("App/Base/Helpers/HtmlHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");

class BaseDatatableService {

  static get SqlHelper() {
    return SqlHelper;
  }

  static controller() {
    return "Controller";
  }

  static section() {
    return "sections";
  }

  static datatableColumns() {
    return [];
  }

  static sql(request, user) {
    return "SELECT 1";
  }

  static async datatableResponse(request, user) {
    const columns = this.datatableColumns().map(column => column.data);
    const datatableHelper = new DatatableHelper(columns, this.sql(request, user), request);

    let data = await datatableHelper.getData();
    data = this.handleBoolean(data);
    const result = await this.handleDatatableResult(data, user);

    return {
      draw: request.draw,
      recordsTotal: await datatableHelper.getRecordsTotal(),
      recordsFiltered: await datatableHelper.getRecordsFiltered(),
      data: result,
    };
  }

  static handleBoolean(records) {
    return records.map(record => {
      return {
        ...record,
        ...Object.entries(record)
          .filter(entry => entry[1] != null)
          .filter(entry => entry[1].constructor.name === "Buffer")
          .reduce((acc, entry) => {
            const bool = entry[1][0] === 1;
            return {
              ...acc,
              [entry[0]]: bool,
            };
          }, {}),
      };
    });
  }

  static async handleDatatableResult(records, user) {
    const controller = this.controller();
    const section = this.section();

    return await Promise.all(records.map(async record => {

      const permissions = {};

      let actionButtonsHtml = `<div class="btn-group">`;

      if (await PermissionHelper.canAccess(user, controller, "show")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}`);

        permissions.show = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_DETAIL_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "success",
            I18nHelper.formatMessage("general.detail"),
            href);
        }
      }

      if (await PermissionHelper.canAccess(user, controller, "edit")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}/edit`);

        permissions.edit = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_EDIT_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createRedirectActionButton(
            "primary",
            I18nHelper.formatMessage("general.edit"),
            href);
        }
      }

      if (await PermissionHelper.canAccess(user, controller, "delete")) {
        const href = RouteHelper.getUrl(`/${section}/${record.id}`);

        permissions.destroy = {
          allowed: true,
          props: { href },
        };

        if (Env.get("DATATABLE_SHOW_DELETE_BUTTON") === "true") {
          actionButtonsHtml += HtmlHelper.createDeleteActionButton(href);
        }
      }

      actionButtonsHtml += `</div>`;

      return {
        ...this.filterRecordKeys(record),
        _permissions: permissions,
        actionButtons: actionButtonsHtml,
      };
    }));
  }

  static filterRecordKeys(record) {
    const columns = [
      ...this.datatableColumns().map(column => column.data),
      "_permissions",
    ];
    return Object.entries(record)
      .filter(entry => columns.includes(entry[0]))
      .reduce((object, item) => {
        return {
          ...object,
          [item[0]]: item[1],
        };
      }, {});
  }

}

module.exports = BaseDatatableService;
