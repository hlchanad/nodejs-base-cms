"use strict";

const BaseService = use("App/Base/Services/BaseService");
const HtmlHelper = use("App/Base/Helpers/HtmlHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");

class SystemParameterService extends BaseService {

  static async upsertSystemParameter(systemParameter, request, user) {
    systemParameter.category = request.category;
    systemParameter.key = request.key;
    systemParameter.value = request.value;
    systemParameter.description = request.description;
    await systemParameter.save(user);
  }

  static async getDefaultDetailButtons (user, controller, section, model) {
    let detailButtonsHtml = "";

    if (await PermissionHelper.canAccess(user, controller, "index")) {
      detailButtonsHtml += HtmlHelper.createRedirectDetailButton("complete", "fa fa-list-ul",
        RouteHelper.getUrl(`/${section}`));
    }

    if (await PermissionHelper.canAccess(user, controller, "edit")) {
      detailButtonsHtml += HtmlHelper.createRedirectDetailButton("primary", "fa fa-pencil",
        RouteHelper.getUrl(`/${section}/${model.id}/edit`));
    }

    return detailButtonsHtml;
  }

}

module.exports = SystemParameterService;
