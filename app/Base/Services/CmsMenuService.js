"use strict";

const _ = require("lodash");

const Redis = use("Redis");

const BaseService = use("App/Base/Services/BaseService");
const Breadcrumb = use("App/Base/Models/Common/Breadcrumb");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const SelectOption = use("App/Base/Models/Common/SelectOption");
const BaseRedisKey = use("App/Base/Constants/BaseRedisKey");
const BaseSystemParameterKey = use("App/Base/Constants/BaseSystemParameterKey");

const CmsMenuOrgranizer = use("App/Base/Helpers/CmsMenuOrgranizer");
const CmsMenu = use("App/Base/Models/CmsMenu");
const CmsMenuIconType = use("App/Base/Constants/CmsMenuIconType");
const CmsRole = use("App/Base/Models/CmsRole");
const CmsRoleMenu = use("App/Base/Models/CmsRoleMenu");
const SystemParameter = use("App/Base/Models/SystemParameter");

class CmsMenuService extends BaseService {

  static async upsertCmsMenu(cmsMenu, request, user) {
    cmsMenu.parent_id = request.parent_id;
    cmsMenu.title = request.title;
    cmsMenu.endpoint = request.endpoint;
    cmsMenu.icon_type = request.icon_type;
    cmsMenu.icon = request.icon;
    cmsMenu.sequence = request.sequence;
    await cmsMenu.save(user);

    const cmsRoleMenus = await cmsMenu.cmsRoleMenus().fetch();
    const requestRoleIds = (Array.isArray(request.role_ids) ? request.role_ids : [ request.role_ids ])
      .map(id => parseInt(id)).filter(id => !isNaN(id));
    const existingRoleIds = cmsRoleMenus.rows.map(cmsRoleMenu => cmsRoleMenu.cms_role_id);

    const toBeAddedRoleIds = requestRoleIds.filter(roleId => !existingRoleIds.includes(roleId));
    const toBeDeletedRoleIds = existingRoleIds.filter(roleId => !requestRoleIds.includes(roleId));

    await Promise.all(toBeAddedRoleIds
      .map(roleId => {
        const cmsRoleMenu = new CmsRoleMenu();
        cmsRoleMenu.cms_menu_id = cmsMenu.id;
        cmsRoleMenu.cms_role_id = roleId;
        return cmsRoleMenu.save(user);
      }));

    await Promise.all(toBeDeletedRoleIds
      .map(roleId => cmsRoleMenus.rows.find(cmsRoleMenu => cmsRoleMenu.cms_role_id === roleId))
      .map(cmsRoleMenu => cmsRoleMenu.delete(user)));
  }

  static async removeRedisMenuItems() {
    const keys = await Redis.hkeys(BaseRedisKey.CMS_SIDEBAR_MENU);
    await Promise.all(keys.map(key => Redis.hdel(BaseRedisKey.CMS_SIDEBAR_MENU, key)));
  }

  static getIconTypeOptions() {
    return CmsMenuIconType.enums.map(iconType => SelectOption.builder()
      .id("iconType-" + iconType.value)
      .title(I18nHelper.formatMessage("cms-menus.fields.icon_type." + _.kebabCase(iconType.key)))
      .value(iconType.key)
      .build());
  }

  static async getParentIdOptions(id) {
    const cmsMenus = await CmsMenu.findByIdNotIn(id);

    id = id == null ? null : parseInt(id);

    const updatedCmsMenus = new CmsMenuOrgranizer((await CmsMenu.getAll()).rows).getResult();
    const updatedTargetCmsMenu = updatedCmsMenus.find(updated => updated.cmsMenu.id === id);

    const MAX = parseInt(await SystemParameter.getConfig(BaseSystemParameterKey.CMS_MENUS_MAX_LEVELS));

    const maxLevelAllowed = MAX - (_.get(updatedTargetCmsMenu, "childrenLevels", 0) + 1);

    return [
      SelectOption.builder()
        .id("parentId-null")
        .title(I18nHelper.formatMessage("cms-menus.fields.parent_id.root"))
        .value(null)
        .build(),
      ...cmsMenus.rows
        .map(cmsMenu => {
          const level = updatedCmsMenus.find(updated => updated.cmsMenu.id === cmsMenu.id).level;

          const isCurrentCmsMenu = id == null ? false : cmsMenu.id === id;
          const isOutOfMaxAllowedLevel = level > maxLevelAllowed;
          const isAnyChildOfTheUpdatingMenu = updatedTargetCmsMenu == null ? false
            : updatedTargetCmsMenu.children.map(child => child.id).includes(cmsMenu.id);

          const disabled = isOutOfMaxAllowedLevel || isCurrentCmsMenu || isAnyChildOfTheUpdatingMenu;

          return SelectOption.builder()
            .id("parentId-" + cmsMenu.id)
            .title(cmsMenu.title)
            .value(cmsMenu.id)
            .disabled(disabled)
            .build();
        }),
    ];
  }

  static async getRolesOptions(id) {
    const cmsRoles = await CmsRole.getAll();
    return cmsRoles.rows
      .map(cmsRole => SelectOption.builder()
        .id("cmsRoleId-" + cmsRole.id)
        .title(cmsRole.title)
        .value(cmsRole.id)
        .build());
  }

  static async deleteNestedCmsMenu(cmsMenu, user) {
    const children = await cmsMenu.children().fetch();

    if (children.rows.length <= 0) {
      return await cmsMenu.delete(user);
    }

    const result = await Promise.all([
      cmsMenu.delete(user),
      ...children.rows.map(async child => this.deleteNestedCmsMenu(child, user))
    ]);

    return result.every(success => success);
  }

  static async getParentsBreadcrumbs(parent) {
    const cmsMenuTitle = I18nHelper.formatMessage("cms-menus.title");
    const rootTitle = `${cmsMenuTitle} - ${I18nHelper.formatMessage("cms-menus.fields.parent_id.root")}`;
    const parents = await CmsMenuService.getAllParents(parent);
    return [
      new Breadcrumb(I18nHelper.formatMessage(Breadcrumb.Root.title), Breadcrumb.Root.url),
      new Breadcrumb(rootTitle, RouteHelper.getUrl("/cms-menus")),
      ...parents
        .map(parent => {
          const title = `${cmsMenuTitle} - ${parent.title}`;
          const route = RouteHelper.getUrl(`/cms-menus?parent_id=${parent.id}`);
          return new Breadcrumb(title, route);
        }),
    ];
  }

  static async getAllParents(cmsMenu) {
    if (cmsMenu == null) {
      return [];
    }

    const parent = await CmsMenu.get(cmsMenu.parent_id);
    const parents = await this.getAllParents(parent);

    return [
      ...parents,
      cmsMenu,
    ];
  }

  static async isMaxCmsMenuLevels(cmsMenu) {
    const parents = await this.getAllParents(cmsMenu);
    const MAX = parseInt(await SystemParameter.getConfig(BaseSystemParameterKey.CMS_MENUS_MAX_LEVELS));
    return parents.length >= MAX;
  }

}

module.exports = CmsMenuService;
