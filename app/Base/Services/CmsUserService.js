"use strict";

const _ = require("lodash");

const Hash = use("Hash");

const BaseService = use("App/Base/Services/BaseService");
const CmsUserRole = use("App/Base/Models/CmsUserRole");

class CmsUserService extends BaseService {

  static async upsertCmsUser(cmsUser, request, user) {
    cmsUser.username = request.username;
    if (request.password != null) {
      cmsUser.password = await Hash.make(request.password);
    }
    cmsUser.email = request.email;

    await cmsUser.save(user);

    const cmsUserRoles = await cmsUser.cmsUserRoles().fetch();
    const requestRoleIds = (Array.isArray(request.role_ids) ? request.role_ids : [request.role_ids])
      .map(id => parseInt(id)).filter(id => !isNaN(id));
    const existingRoleIds = cmsUserRoles.rows.map(cmsUserRole => cmsUserRole.cms_role_id);

    const toBeAddedRoleIds = requestRoleIds.filter(roleId => !existingRoleIds.includes(roleId));
    const toBeDeletedRoleIds = existingRoleIds.filter(roleId => !requestRoleIds.includes(roleId));

    await Promise.all(toBeAddedRoleIds
      .map(roleId => {
        const cmsUserRole = new CmsUserRole();
        cmsUserRole.cms_user_id = cmsUser.id;
        cmsUserRole.cms_role_id = roleId;
        return cmsUserRole.save(user);
      }));

    await Promise.all(toBeDeletedRoleIds
      .map(roleId => cmsUserRoles.rows.find(cmsUserRole => cmsUserRole.cms_role_id === roleId))
      .map(cmsUserRole => cmsUserRole.delete(user)));
  }

}

module.exports = CmsUserService;
