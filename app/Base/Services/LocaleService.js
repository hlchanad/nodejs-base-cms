"use strict";

const _ = require("lodash");

const BaseService = use("App/Base/Services/BaseService");
const HtmlHelper = use("App/Base/Helpers/HtmlHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const PermissionHelper = use("App/Base/Helpers/PermissionHelper");
const BaseLocaleGroupKey = use("App/Base/Constants/BaseLocaleGroupKey");
const Locale = use("App/Base/Models/Locale");
const Language = use("App/Base/Models/Common/Language");

class LocaleService extends BaseService {

  static async upsertLocale(locale, request, user) {

    const group = locale.group;
    const item = locale.item;

    const locales = await Locale.findByGroupAndItem(group, item);

    for (let i = 0; i < Language.getSupportedLanguages().length; i++) {
      const language = Language.getSupportedLanguages()[i];

      const locale = locales.rows.find(locale => locale.locale === language.localeCode) || new Locale();
      locale.locale = language.localeCode;
      locale.group = group;
      locale.item = item;
      locale.text = request.details[language.dbCode].text;
      await locale.save(user);
    }
  }

  static async getDefaultDetailButtons(user, controller, section, model) {
    let detailButtonsHtml = "";

    if (await PermissionHelper.canAccess(user, controller, "index")) {
      detailButtonsHtml += HtmlHelper.createRedirectDetailButton("complete", "fa fa-list-ul",
        RouteHelper.getUrl(`/${section}`));
    }

    if (await PermissionHelper.canAccess(user, controller, "edit")) {
      detailButtonsHtml += HtmlHelper.createRedirectDetailButton("primary", "fa fa-pencil",
        RouteHelper.getUrl(`/${section}/${model.id}/edit`));
    }

    return detailButtonsHtml;
  }

  static async getLocaleDetailsMap(group, item) {
    return (await Locale.findByGroupAndItem(group, item)).rows
      .reduce((acc, detail) => {
        return {
          ...acc,
          [detail.locale]: detail,
        };
      }, {});
  }

  static async getDatatablesjsLocales(locale) {
    const locales = await Locale.findByLocaleAndGroup(locale, BaseLocaleGroupKey.DatatableJS);

    const getLocale = item => _.get(locales.rows.find(locale => locale.item === item), "text", "missing locale");

    return {
      sEmptyTable: getLocale("s-empty-table"),
      sInfo: getLocale("s-info"),
      sInfoEmpty: getLocale("s-info-empty"),
      sInfoFiltered: getLocale("s-info-filtered"),
      sInfoPostFix: getLocale("s-info-post-fix"),
      sInfoThousands: getLocale("s-info-thousands"),
      sLengthMenu: getLocale("s-length-menu"),
      sLoadingRecords: getLocale("s-loading-records"),
      sProcessing: getLocale("s-processing"),
      sSearch: getLocale("s-search"),
      sZeroRecords: getLocale("s-zero-records"),
      oPaginate: {
        sFirst: getLocale("o-paginate.s-first"),
        sLast: getLocale("o-paginate.s-last"),
        sNext: getLocale("o-paginate.s-next"),
        sPrevious: getLocale("o-paginate.s-previous"),
      },
      oAria: {
        sSortAscending: getLocale("o-aria.s-sort-ascending"),
        sSortDescending: getLocale("o-aria.s-sort-descending"),
      },
      select: {
        rows: {
          _: getLocale("select.rows._"),
          0: getLocale("select.rows.0"),
          1: getLocale("select.rows.1"),
        },
      },
    };
  }

  static async getDropzonejsLocales(locale) {
    const locales = await Locale.findByLocaleAndGroup(locale, BaseLocaleGroupKey.DropzoneJS);

    const getLocale = item => _.get(locales.rows.find(locale => locale.item === item), "text", "missing locale");

    return {
      dictDefaultMessage: getLocale("default-message"),
      dictFallbackMessage: getLocale("fallback-message"),
      dictFallbackText: getLocale("fallback-text"),
      dictFileTooBig: getLocale("file-too-big"),
      dictInvalidFileType: getLocale("invalid-file-type"),
      dictResponseError: getLocale("response-error"),
      dictCancelUpload: getLocale("cancel-upload"),
      dictUploadCanceled: getLocale("upload-cancelled"),
      dictCancelUploadConfirmation: getLocale("cancel-upload-confirmation"),
      dictRemoveFile: getLocale("remove-file"),
      dictMaxFilesExceeded: getLocale("max-files-exceeded"),
    };
  }

  static async getDaterangepickerLocales(locale) {
    const locales = await Locale.findByLocaleAndGroup(locale, BaseLocaleGroupKey.Daterangepicker);

    const getLocale = item => _.get(locales.rows.find(locale => locale.item === item), "text", "missing locale");

    return {
      separator: getLocale("separator"),
      applyLabel: getLocale("apply-label"),
      cancelLabel: getLocale("cancel-label"),
      fromLabel: getLocale("from-label"),
      toLabel: getLocale("to-label"),
      customRangeLabel: getLocale("custom-range-label"),
      weekLabel: getLocale("week-label"),
      daysOfWeek: [
        getLocale("days-of-week.sunday"),
        getLocale("days-of-week.monday"),
        getLocale("days-of-week.tuesday"),
        getLocale("days-of-week.wednesday"),
        getLocale("days-of-week.thursday"),
        getLocale("days-of-week.friday"),
        getLocale("days-of-week.sunday"),
      ],
      monthNames: [
        getLocale("month-names.january"),
        getLocale("month-names.february"),
        getLocale("month-names.march"),
        getLocale("month-names.april"),
        getLocale("month-names.may"),
        getLocale("month-names.june"),
        getLocale("month-names.july"),
        getLocale("month-names.august"),
        getLocale("month-names.september"),
        getLocale("month-names.october"),
        getLocale("month-names.november"),
        getLocale("month-names.december"),
      ],
    };
  }

}

module.exports = LocaleService;
