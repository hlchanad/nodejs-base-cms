"use strict";

const _ = require("lodash");
const winston = require("winston");
require("winston-daily-rotate-file");

const Env = use("Env");
const Helpers = use("Helpers");

const RequestId = use("App/Base/Logger/Formats/RequestId");
const LevelSpacing = use("App/Base/Logger/Formats/LevelSpacing");

/**
 * Winston file daily rotate transport driver for @ref('Logger').
 *
 * @class FileRotate
 * @constructor
 */
class FileRotate {

  setConfig(config) {
    /**
     * Merging user config with defaults.
     */
    this.config = Object.assign({}, {
      name: "adonis-app",
      level: "info",
      dirname: "storage/logs",
      filename: "application",
      datePattern: "YYYY-MM-DD",
      maxSize: "20m",
      maxFiles: "14d",
    }, config);

    const format = this.config.format || winston.format.combine(
      new RequestId(),
      winston.format.colorize(),
      new LevelSpacing(this.levels),
      winston.format.splat(),
      winston.format.simple(),
      winston.format.timestamp(),
      winston.format.printf(({ level, timestamp, requestId, message }) => {
        return `${timestamp} | ${requestId} | ${level} | ${message}`;
      }),
    );

    delete this.config.format;

    const directory = this.config.directory.startsWith("/")
      ? this.config.directory
      : Helpers.appRoot(this.config.directory);

    const transports = [];
    Env.get("LOGGER_ROTATE_TO_CONSOLE", "false") === "true" && transports.push(new winston.transports.Console());
    Env.get("LOGGER_ROTATE_TO_FILE", "false") === "true" && transports.push(new winston.transports.DailyRotateFile({
      zippedArchive: true,
      dirname: directory,
      filename: `${this.config.filename}.%DATE%.log`,
      datePattern: this.config.datePattern,
      maxSize: this.config.maxSize,
      maxFiles: this.config.maxFiles,
    }));

    /**
     * Creating new instance of winston with file transport
     */
    this.logger = winston.createLogger({
      format: format,
      level: this.config.level,
      levels: this.levels,
      transports: transports,
    });
  }

  /**
   * A list of available log levels
   *
   * @attribute levels
   *
   * @return {Object}
   */
  get levels() {
    return {
      emerg: 0,
      alert: 1,
      crit: 2,
      error: 3,
      warning: 4,
      notice: 5,
      info: 6,
      debug: 7,
    };
  }

  /**
   * Returns the current level for the driver
   *
   * @attribute level
   *
   * @return {String}
   */
  get level() {
    return this.logger.transports[0].level;
  }

  /**
   * Update driver log level at runtime
   *
   * @param  {String} level
   *
   * @return {void}
   */
  set level(level) {
    this.logger.transports[0].level = level;
  }

  /**
   * Log message for a given level.
   *
   * @method log
   *
   * @param  {Number}    level
   * @param  {String}    msg
   * @param  {...Spread} meta
   *
   * @return {void}
   */
  log(level, msg, ...meta) {
    const levelName = _.findKey(this.levels, num => num === level);
    this.logger.log(levelName, msg, ...meta);
  }

}

module.exports = FileRotate;
