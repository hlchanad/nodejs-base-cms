const _ = require("lodash");

const ContextKey = use("App/Base/Constants/ContextKey");
const RequestIdHelper = use("App/Base/Helpers/RequestIdHelper");

class RequestId {

  transform(info, opts) {
    const sprintfPlaceholders = [ "b", "c", "d", "e", "u", "f", "o", "s", "x", "X" ].map(char => "%" + char);

    const context = use("Context");
    const contextRequestId = context.get(ContextKey.Request_ID);

    const requiredNumberOfPlaceholders = (info.message.match(new RegExp(sprintfPlaceholders.join("|"), "g")) || []).length;
    const providedNumberOfPlaceholders = (info[Symbol.for("splat")] || []).length;

    const requestId = requiredNumberOfPlaceholders === providedNumberOfPlaceholders
      ? contextRequestId : info[Symbol.for("splat")][requiredNumberOfPlaceholders];

    return {
      ...info,
      requestId: _.padStart(requestId, RequestIdHelper.length),
    };
  }

}

module.exports = RequestId;
