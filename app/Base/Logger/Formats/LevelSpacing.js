const _ = require("lodash");

class LevelSpacing {

  constructor(levels) {
    this.levels = levels;
  }

  transform(info, opts) {

    const parsed = info.level.match(new RegExp(/^(.\[3[1-4]m)(\w+)(.\[39m)$/));

    const level = _.padEnd(parsed[2], this.getLevelMaxLength());

    return {
      ...info,
      level: parsed[1] + level + parsed[3],
    };
  }

  getLevelMaxLength() {
    return Object.keys(this.levels)
      .map(level => level.length)
      .sort((a, b) => b - a)
      [0];
  }

}

module.exports = LevelSpacing;
