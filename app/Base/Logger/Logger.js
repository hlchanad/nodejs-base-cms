const _ = require("lodash");
const stackTrace = require("stack-trace");

const Env = use("Env");
const Helpers = use("Helpers");
const AdonisLogger = use("Logger");

class Logger {

  static get classIdentifierLength() {
    return parseInt(Env.get("LOGGER_CLASS_IDENTIFIER_LENGTH", 40));
  }

  static getMessageWithClassIdentifier(message) {
    try {
      const traces = stackTrace.get();
      const relativePath = traces[2].getFileName().substr(Helpers.appRoot().length + 1);
      const clazz = relativePath.substr(relativePath.lastIndexOf("/") + 1).split(".")[0];
      const packagesInShort = relativePath
        .substring(0, relativePath.lastIndexOf("/"))
        .split("/")
        .map(folder => folder.substr(0, 1))
        .join(".");

      let identifier = _.padStart(`${packagesInShort}.${clazz}`, this.classIdentifierLength);
      if (identifier.length > this.classIdentifierLength) {
        identifier = identifier.substr(0, this.classIdentifierLength);
      }

      return `${identifier} | ${message}`;
    } catch (error) {
      return message;
    }
  }

  debug(message, ...options) {
    AdonisLogger.debug(Logger.getMessageWithClassIdentifier(message), ...options);
  }

  info(message, ...options) {
    AdonisLogger.info(Logger.getMessageWithClassIdentifier(message), ...options);
  }

  notice(message, ...options) {
    AdonisLogger.notice(Logger.getMessageWithClassIdentifier(message), ...options);
  }

  warning(message, ...options) {
    AdonisLogger.warning(Logger.getMessageWithClassIdentifier(message), ...options);
  }

  error(message, ...options) {
    AdonisLogger.error(Logger.getMessageWithClassIdentifier(message), ...options);
  }

  crit(message, ...options) {
    AdonisLogger.crit(Logger.getMessageWithClassIdentifier(message), ...options);
  }

  alert(message, ...options) {
    AdonisLogger.alert(Logger.getMessageWithClassIdentifier(message), ...options);
  }

  emerg(message, ...options) {
    AdonisLogger.emerg(Logger.getMessageWithClassIdentifier(message), ...options);
  }

}

module.exports = Logger;
