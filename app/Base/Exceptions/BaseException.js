'use strict';

const _ = require("lodash");

const { LogicalException } = require('@adonisjs/generic-exceptions');

const I18nHelper = use("App/Base/Helpers/I18nHelper");
const ContextKey = use("App/Base/Constants/ContextKey");
const CmsUserPreferenceKey = use("App/Base/Constants/CmsUserPreferenceKey");
const Language = use("App/Base/Models/Common/Language");

class BaseException extends LogicalException {

  async flash(session, message, data) {
    session.flash({
      exception: {
        clazz: this.constructor.name,
        title: I18nHelper.formatMessage("exception.general.title", null, this.getLocaleCode(session)),
        message: message,
        data: { ...data },
      },
    });
    await session.commit();
  }

  getLocaleCode(session) {
    const languageCode = _.get(session.get(ContextKey.CMS_User_Preferences), CmsUserPreferenceKey.CMS_Language, Language.English.dbCode);

    const language = Language.of("dbCode", languageCode);

    return language.localeCode;
  }

}

module.exports = BaseException;
