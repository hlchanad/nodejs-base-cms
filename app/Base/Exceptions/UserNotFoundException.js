const AuthException = use("App/Base/Exceptions/AuthException");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class UserNotFoundException extends AuthException {

  constructor() {
    super();
  }

  async handle(error, { response, session }) {
    const message = I18nHelper.formatMessage("exception.user-not-found.message", null, this.getLocaleCode(session));

    await this.flash(session, message);

    return response.redirect("back");
  }

}

module.exports = UserNotFoundException;
