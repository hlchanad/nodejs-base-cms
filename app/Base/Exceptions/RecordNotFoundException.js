const BaseException = use("App/Base/Exceptions/BaseException");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class RecordNotFoundException extends BaseException {

  constructor(section, id) {
    super();
    this.section = section;
    this.id = id;
  }

  async handle(error, { response, session }) {
    const message = I18nHelper.formatMessage("exception.record-not-found.message", {
      section: I18nHelper.formatMessage(`${this.section}.title`),
      id: this.id
    }, this.getLocaleCode(session));

    await this.flash(session, message, {
      section: this.section,
      id: this.id
    });

    return response.redirect(RouteHelper.getUrl(`/${this.section}`));
  }

}

module.exports = RecordNotFoundException;
