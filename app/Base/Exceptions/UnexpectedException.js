const BaseException = use("App/Base/Exceptions/BaseException");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const ContextKey = use("App/Base/Constants/ContextKey");

class UnexpectedException extends BaseException {

  constructor(error) {
    super();
    this.error = error;
    this.message = error.message;
  }

  async handle(error, { response, session }) {
    const requestId = session.get(ContextKey.Request_ID);

    const message = I18nHelper.formatMessage("exception.unexpected.message", {
      requestId,
    }, this.getLocaleCode(session));

    await this.flash(session, message);

    return response.redirect(RouteHelper.getUrl("/dashboard"));
  }

}

module.exports = UnexpectedException;
