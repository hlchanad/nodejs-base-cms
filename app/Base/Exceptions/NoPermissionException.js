const AuthException = use("App/Base/Exceptions/AuthException");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");

class NoPermissionException extends AuthException {

  constructor() {
    super();
  }

  async handle(error, { response, session }) {
    const message = I18nHelper.formatMessage("exception.no-permission.message", null, this.getLocaleCode(session));

    await this.flash(session, message);

    return response.redirect(RouteHelper.getUrl("/dashboard"));
  }

}

module.exports = NoPermissionException;
