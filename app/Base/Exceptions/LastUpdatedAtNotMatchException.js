const BaseException = use("App/Base/Exceptions/BaseException");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class LastUpdatedAtNotMatchException extends BaseException {

  constructor(section, id, exceptFields) {
    super();
    this.section = section;
    this.id = id;
    this.exceptFields = exceptFields;
  }

  async handle(error, { response, session }) {
    const message = I18nHelper.formatMessage("exception.last-updated-at-not-match.message", {
      section: I18nHelper.formatMessage(`${this.section}.title`),
      id: this.id,
    }, this.getLocaleCode(session));

    session.flashExcept(this.exceptFields);

    await this.flash(session, message, {
      section: this.section,
      id: this.id
    });

    return response.redirect("back");
  }

}

module.exports = LastUpdatedAtNotMatchException;
