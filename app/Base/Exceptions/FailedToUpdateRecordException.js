const BaseException = use("App/Base/Exceptions/BaseException");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class FailedToUpdateRecordException extends BaseException {

  constructor(error, exceptFields) {
    super();
    this.error = error;
    this.message = error.message;
    this.exceptFields = exceptFields;
  }

  async handle(error, { response, session }) {
    const message = I18nHelper.formatMessage("exception.failed-to-update-record.message", null, this.getLocaleCode(session));

    session.flashExcept(this.exceptFields);

    await this.flash(session, message);

    return response.redirect("back");
  }

}

module.exports = FailedToUpdateRecordException;
