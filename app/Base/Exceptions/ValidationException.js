const BaseException = use("App/Base/Exceptions/BaseException");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class ValidationException extends BaseException {

  constructor(validation, exceptFields) {
    super();
    this.validation = validation;
    this.exceptFields = exceptFields;
  }

  async handle(error, { response, session }) {
    const message = I18nHelper.formatMessage("exception.validation.message", null, this.getLocaleCode(session));

    session
      .withErrors(this.validation.messages())
      .flashExcept(this.exceptFields);

    await this.flash(session, message);

    return response.redirect("back");
  }

}

module.exports = ValidationException;
