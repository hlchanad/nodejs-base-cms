const BaseException = use("App/Base/Exceptions/BaseException");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class AuthException extends BaseException {

  constructor() {
    super();
  }

  async handle(error, { response, session }) {
    const message = I18nHelper.formatMessage("exception.auth.message", null, this.getLocaleCode(session));

    await this.flash(session, message);

    return response.redirect("back");
  }

}

module.exports = AuthException;
