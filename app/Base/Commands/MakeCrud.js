"use strict";

const _ = require("lodash");
const pluralize = require("pluralize");

const { Command } = require("@adonisjs/ace");

const Helpers = use("Helpers");
const View = use("View");
const Database = use("Database");

const DatabaseHelper = use("App/Base/Helpers/DatabaseHelper");

class MakeCrud extends Command {

  static manageableFields () {
    return [
      "status",
      "is_visible",
      "is_deleted",
      "created_at",
      "updated_at",
      "deleted_at",
      "created_by",
      "updated_by",
      "deleted_by",
    ];
  }

  static detailTableDefaultFields () {
    return [
      "ref_id",
      "language",
    ];
  }

  static get signature() {
    return `
      make:crud
      { table: Name of the table }
      { --force: Override any file if exists }
      { --base: Create for base CMS }
    `;
  }

  static get description() {
    return "Create code files for basic CRUD of given table";
  }

  async handle(args, options) {
    try {
      await this.run(args, options);
      this.success("Done");
      Database.close();
      process.exit();
    } catch (error) {
      this.error(error.message);
      Database.close();
      process.exit(1);
    }
  }

  async run(args, options) {

    // ----------------------------------------------------------------------------------------------------
    //   Preparation
    // ----------------------------------------------------------------------------------------------------

    const table = args.table;
    const force = !!options.force;
    const base = !!options.base;

    const section  = _.kebabCase(table);
    const tableLowerCamel = _.camelCase(pluralize.singular(table));
    const tableUpperCamel = _.capitalize(table.substr(0, 1)) + _.camelCase(pluralize.singular(table)).substr(1);


    // ----------------------------------------------------------------------------------------------------
    //   Check table existence
    // ----------------------------------------------------------------------------------------------------

    const tableExist = await DatabaseHelper.isTableExists(table);
    if (!tableExist) {
      throw new Error("unable to find table: " + table);
    }


    // ----------------------------------------------------------------------------------------------------
    //   Check if detail table exists
    // ----------------------------------------------------------------------------------------------------

    const detailTable = pluralize.singular(table) + "_details";
    const isMultiLanguageTable = await DatabaseHelper.isTableExists(detailTable);


    // ----------------------------------------------------------------------------------------------------
    //   Check if any files already exist
    // ----------------------------------------------------------------------------------------------------

    const paths = {
      controller: Helpers.appRoot(`app/${base ? 'Base/' : ''}Controllers/Http/${tableUpperCamel}Controller.js`),
      model: Helpers.appRoot(`app/${base ? 'Base/' : ''}Models/${tableUpperCamel}.js`),
      detailModel: !isMultiLanguageTable ? null : Helpers.appRoot(`app/${base ? 'Base/' : ''}Models/${tableUpperCamel}Detail.js`),
      service: Helpers.appRoot(`app/${base ? 'Base/' : ''}Services/${tableUpperCamel}Service.js`),
      datatableService: Helpers.appRoot(`app/${base ? 'Base/' : ''}Services/Datatable/${tableUpperCamel}DatatableService.js`),
      createEdge: Helpers.viewsPath(`${base ? 'base/' : ''}${section}/create.edge`),
      editEdge: Helpers.viewsPath(`${base ? 'base/' : ''}${section}/edit.edge`),
      formEdge: Helpers.viewsPath(`${base ? 'base/' : ''}${section}/form.edge`),
      indexEdge: Helpers.viewsPath(`${base ? 'base/' : ''}${section}/index.edge`),
      showEdge: Helpers.viewsPath(`${base ? 'base/' : ''}${section}/show.edge`),
    };

    const existPaths = (await Promise.all(Object.values(paths)
      .map(async path => (await this.pathExists(path)) ? path : null)))
      .filter(path => path != null);

    if (existPaths.length > 0 && !force) {
      throw new Error("some files already exists");
    }


    // ----------------------------------------------------------------------------------------------------
    //   Get table column data
    // ----------------------------------------------------------------------------------------------------

    const tableColumns = await DatabaseHelper.getTableColumns(table);

    const mainColumns = tableColumns
      .filter(column => column.Field !== "id")
      .filter(column => !MakeCrud.manageableFields().includes(column.Field));


    const detailTableColumns = !isMultiLanguageTable ? [] : await DatabaseHelper.getTableColumns(detailTable);

    const detailTableMainColumns = detailTableColumns
      .filter(column => column.Field !== "id")
      .filter(column => !MakeCrud.detailTableDefaultFields().includes(column.Field));


    // ----------------------------------------------------------------------------------------------------
    //   Start generating files
    // ----------------------------------------------------------------------------------------------------

    await Promise.all([
      this.createModel(base, isMultiLanguageTable, paths.model, section, tableUpperCamel, mainColumns, detailTableMainColumns),
      this.createDetailModel(base, isMultiLanguageTable, paths.detailModel, tableUpperCamel, tableLowerCamel),
      this.createController(base, isMultiLanguageTable, paths.controller, section, tableUpperCamel, tableLowerCamel, table),
      this.createService(base, isMultiLanguageTable, paths.service, tableUpperCamel, tableLowerCamel, mainColumns, detailTableMainColumns),
      this.createDatatableService(base, isMultiLanguageTable, paths.datatableService, table, detailTable, tableUpperCamel, tableLowerCamel, section, mainColumns, detailTableMainColumns),
      this.createCreateEdge(base, isMultiLanguageTable, paths.createEdge, section),
      this.createEditEdge(base, isMultiLanguageTable, paths.editEdge, section),
      this.createFormEdge(base, isMultiLanguageTable, paths.formEdge, section, mainColumns, detailTableMainColumns),
      this.createIndexEdge(base, isMultiLanguageTable, paths.indexEdge),
      this.createShowEdge(base, isMultiLanguageTable, paths.showEdge, section, mainColumns, detailTableMainColumns),
    ]);


    // ----------------------------------------------------------------------------------------------------
    //   done done
    // ----------------------------------------------------------------------------------------------------

    this.info("add the following line to `/start/routes.js`");
    this.info(`> Route.resource('/${section}', '${base ? 'App/Base/Controllers/Http/' : ''}${tableUpperCamel}Controller');`);
    this.info(`> Route.post('/${section}/delete-requests', '${base ? 'App/Base/Controllers/Http/' : ''}${tableUpperCamel}Controller.batchDelete');`);

    return paths;
  }

  async createModel(base, isMultiLanguageTable, path, section, tableUpperCamel, tableColumns, detailTableColumns) {
    const tableColumnsWithValidationRules = tableColumns
      .map(column => {
        return {
          ...column,
          validationRules: this.getValidationRules(column),
        };
      });

    const detailTableColumnsWithValidationRules = detailTableColumns
      .map(column => {
        return {
          ...column,
          validationRules: this.getValidationRules(column),
        };
      });

    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/model.edge"), "utf-8");
    const content = View.renderString(template, {
      tableUpperCamel: tableUpperCamel,
      section: section,
      tableColumns: tableColumnsWithValidationRules,
      detailTableColumns: detailTableColumnsWithValidationRules,
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createDetailModel(base, isMultiLanguageTable, path, tableUpperCamel, tableLowerCamel) {
    if (!isMultiLanguageTable) return ;

    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/detail-model.edge"), "utf-8");
    const content = View.renderString(template, {
      tableUpperCamel: tableUpperCamel,
      tableLowerCamel: tableLowerCamel,
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createController(base, isMultiLanguageTable, path, section, tableUpperCamel, tableLowerCamel, table) {
    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/controller.edge"), "utf-8");
    const content = View.renderString(template, {
      tableUpperCamel: tableUpperCamel,
      tableLowerCamel: tableLowerCamel,
      table: table,
      section: section,
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createService(base, isMultiLanguageTable, path, tableUpperCamel, tableLowerCamel, tableColumns, detailTableColumns) {
    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/service.edge"), "utf-8");
    const content = View.renderString(template, {
      tableUpperCamel: tableUpperCamel,
      tableLowerCamel: tableLowerCamel,
      tableColumns: tableColumns,
      detailTableColumns: detailTableColumns,
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createDatatableService(base, isMultiLanguageTable, path, table, detailTable, tableUpperCamel, tableLowerCamel, section, tableColumns, detailTableColumns) {
    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/datatableService.edge"), "utf-8");
    const content = View.renderString(template, {
      table: table,
      detailTable: detailTable,
      tableUpperCamel: tableUpperCamel,
      tableLowerCamel: tableLowerCamel,
      tableColumns: tableColumns,
      detailTableColumns: detailTableColumns,
      section: section,
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createCreateEdge(base, isMultiLanguageTable, path, section) {
    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/views/create.edge"), "utf-8");
    const content = View.renderString(template, {
      section: section,
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createEditEdge(base, isMultiLanguageTable, path, section) {
    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/views/edit.edge"), "utf-8");
    const content = View.renderString(template, {
      section: section,
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createFormEdge(base, isMultiLanguageTable, path, section, tableColumns, detailTableColumns) {
    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/views/form.edge"), "utf-8");
    const content = View.renderString(template, {
      section: section,
      tableColumns: tableColumns,
      detailTableColumns: detailTableColumns,
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createIndexEdge(base, isMultiLanguageTable, path) {
    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/views/index.edge"), "utf-8");
    const content = View.renderString(template, {
      isMultiLanguageTable: isMultiLanguageTable,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }

  async createShowEdge(base, isMultiLanguageTable, path, section, tableColumns, detailTableColumns) {
    const template = await this.readFile(Helpers.viewsPath("base/commands/make-crud/views/show.edge"), "utf-8");
    const content = View.renderString(template, {
      tableColumns: tableColumns,
      detailTableColumns: detailTableColumns,
      isMultiLanguageTable: isMultiLanguageTable,
      section: section,
      base: base,
    });
    await this.writeFile(path, content);

    this.completed("created", path);
  }


  getValidationRules(column) {
    let rules = "";

    // Type Checking
    if (column.Type.indexOf("varchar") >= 0 || column.Type === "text") {
      rules += "string";
    } else if (column.Type.indexOf("int") >= 0) {
      rules += "integer";
    } else if (["float", "double"].includes(column.Type)) {
      rules += "float";
    } else if (column.Type === "bit(1)") {
      rules += "boolean";
    } else if (column.Type.indexOf("date") >= 0) {
      rules += "date";
    }

    // Null Checking
    if (column.Null === "NO") {
      rules += "|required";
    }

    return rules;
  }

}

module.exports = MakeCrud;
