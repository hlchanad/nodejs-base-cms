"use strict";

const { Command } = require("@adonisjs/ace");

const Database = use("Database");

const MakeCrud = use("App/Base/Commands/MakeCrud");
const MakeLocales = use("App/Base/Commands/MakeLocales");

class StartUp extends Command {

  static get signature() {
    return `
      start-up
      { table: Name of the table }
      { --force: Override if exists }
      { --base: Base CMS module }
    `;
  }

  static get description() {
    return "Create basic stuffs for given table";
  }

  async handle(args, options) {
    try {
      const table = args.table;
      const force = !!options.force;
      const base = !!options.base;

      await new MakeCrud().run({ table: table }, { force: force, base: base });
      this.success("Finished CRUD files");

      await new MakeLocales().run({ table: table }, { force: force });
      this.success("Finished locale records");

      Database.close();
      process.exit();
    } catch (error) {
      this.error(error.message);
      Database.close();
      process.exit(1);
    }
  }

}

module.exports = StartUp;
