"use strict";

const _ = require("lodash");
const pluralize = require("pluralize");

const { Command } = require("@adonisjs/ace");

const Database = use("Database");

const DatabaseHelper = use("App/Base/Helpers/DatabaseHelper");
const AwsHelper = use("App/Base/Helpers/AwsHelper");
const Locale = use("App/Base/Models/Locale");
const Language = use("App/Base/Models/Common/Language");

Array.prototype.flatten = function () {
  return _.flatten(this);
};

class MakeLocales extends Command {

  static manageableFields() {
    return [
      "status",
      "is_visible",
      "is_deleted",
      "created_at",
      "updated_at",
      "deleted_at",
      "created_by",
      "updated_by",
      "deleted_by",
    ];
  }

  static detailTableDefaultFields() {
    return [
      "ref_id",
      "language",
    ];
  }

  static get signature() {
    return `
      make:locales
      { table: Name of the table }
      { --force: Override any locales record if exists }
    `;
  }

  static get description() {
    return "Create locale records of given table";
  }

  async handle(args, options) {
    try {
      await this.run(args, options);
      this.success("Done");
      Database.close();
      process.exit();
    } catch (error) {
      this.error(error.message);
      Database.close();
      process.exit(1);
    }
  }

  async run(args, options) {

    // ----------------------------------------------------------------------------------------------------
    //   Preparation
    // ----------------------------------------------------------------------------------------------------

    const table = args.table;
    const force = !!options.force;

    const section = _.kebabCase(table);


    // ----------------------------------------------------------------------------------------------------
    //   Check table existence
    // ----------------------------------------------------------------------------------------------------

    const tableExist = await DatabaseHelper.isTableExists(table);
    if (!tableExist) {
      throw new Error("unable to find table: " + table);
    }


    // ----------------------------------------------------------------------------------------------------
    //   Check if detail table exists
    // ----------------------------------------------------------------------------------------------------

    const detailTable = pluralize.singular(table) + "_details";
    const isMultiLanguageTable = await DatabaseHelper.isTableExists(detailTable);


    // ----------------------------------------------------------------------------------------------------
    //   Get table column data
    // ----------------------------------------------------------------------------------------------------

    const tableColumns = await DatabaseHelper.getTableColumns(table);

    const mainColumns = tableColumns
      .filter(column => column.Field !== "id")
      .filter(column => !MakeLocales.manageableFields().includes(column.Field));


    const detailTableColumns = !isMultiLanguageTable ? [] : await DatabaseHelper.getTableColumns(detailTable);

    const detailTableMainColumns = detailTableColumns
      .filter(column => column.Field !== "id")
      .filter(column => !MakeLocales.detailTableDefaultFields().includes(column.Field));


    // ----------------------------------------------------------------------------------------------------
    //   Check if any locale data already exist
    // ----------------------------------------------------------------------------------------------------

    const localeKeys = [
      this.getIdLocaleKey(section),
      ...await this.getDefaultColumnKeys(section),
      ...await this.getColumnLocaleKeys(section, mainColumns),
      ...await this.getColumnLocaleKeys(section, detailTableMainColumns),
    ];

    const localeKeysWithLanguage = await this.getLocaleKeysWithLanguage(localeKeys);


    // ----------------------------------------------------------------------------------------------------
    //   Start generating locale data
    // ----------------------------------------------------------------------------------------------------

    await this.generateLocales(localeKeysWithLanguage, force);


    // ----------------------------------------------------------------------------------------------------
    //   done done
    // ----------------------------------------------------------------------------------------------------

    return true;
  }

  async getDefaultColumnKeys(section) {
    return [
      {
        group: section,
        item: "title",
        text: pluralize.singular(section).split("-").map(split => _.capitalize(split)).join(" "),
      },
    ];
  }

  getIdLocaleKey(section) {
    return {
      group: section,
      item: "fields.id",
      text: "ID",
    };
  }

  async getColumnLocaleKeys(section, tableColumns) {
    return tableColumns
      .map(column => {
        return {
          group: section,
          item: `fields.${column.Field}`,
          text: _.capitalize(_.kebabCase(column.Field).replace(new RegExp(/-/g), " ")),
        };
      });
  }

  async getLocaleKeysWithLanguage(localeKeys) {
    return [
      localeKeys.map(localeKey => {
        return {
          locale: Language.English.localeCode,
          ...localeKey,
        };
      }),
      ...await Promise.all(Language.getSupportedLanguages()
        .filter(language => !Language.English.equals(language))
        .map(async language => Promise.all(localeKeys.map(async localeKey => {
          return {
            locale: language.localeCode,
            group: localeKey.group,
            item: localeKey.item,
            text: await AwsHelper.translate(Language.English.awsCode, language.awsCode, localeKey.text),
          };
        })))),
    ].flatten();
  }

  async existsAnyLocaleRecords(localeKeysWithLanguage) {
    const locales = await Promise.all(localeKeysWithLanguage.map(key => Locale.query()
      .where("locale", key.locale)
      .where("group", key.group)
      .where("item", key.item)
      .where("is_deleted", false)
      .first())
    );

    return !locales.every(locale => locale == null);
  }

  async generateLocales(localeKeysWithLanguage, force) {
    const locales = [ ...await Promise.all(localeKeysWithLanguage.map(async key => {
      let locale = await Locale.query()
        .where("locale", key.locale)
        .where("group", key.group)
        .where("item", key.item)
        .where("is_deleted", false)
        .first();

      if (!force && locale != null) return null;

      if (locale == null) locale = new Locale();

      locale.locale = key.locale;
      locale.group = key.group;
      locale.item = key.item;
      locale.text = key.text;

      return locale;
    })) ]
      .filter(locale => locale != null);

    await Promise.all(locales.map(locale => locale.save()));
  }

}

module.exports = MakeLocales;
