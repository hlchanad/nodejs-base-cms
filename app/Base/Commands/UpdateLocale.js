"use strict";

const _ = require("lodash");

const { Command } = require("@adonisjs/ace");

const Database = use("Database");

const AwsHelper = use("App/Base/Helpers/AwsHelper");
const Locale = use("App/Base/Models/Locale");
const Language = use("App/Base/Models/Common/Language");

Array.prototype.flatten = function () {
  return _.flatten(this);
};

class UpdateLocale extends Command {

  static get LocaleDirectory() {
    return "../../../resources/locales/";
  }

  static get BaseJson() {
    return "base.json";
  }

  static get AppJson() {
    return "app.json";
  }

  static get signature() {
    return `
      update-locale
      { --base: Update for base.json }
    `;
  }

  static get description() {
    return "Update locale table from json file";
  }

  async handle(args, options) {
    try {
      await this.run(args, options);
      this.success("Done");
      Database.close();
      process.exit();
    } catch (error) {
      this.error(error.message);
      Database.close();
      process.exit(1);
    }
  }

  async run(args, options) {
    const base = !!options.base;

    const localeKeys = this.localeKeysWithLanguage(base);

    const locales = [];

    for (let i = 0; i < localeKeys.length; i++) {
      const key = localeKeys[i];

      let locale = await Locale.query()
        .where("locale", key.locale)
        .where("group", key.group)
        .where("item", key.item)
        .where("is_deleted", false)
        .first();

      if (locale != null) continue;

      locale = new Locale();
      locale.locale = key.locale;
      locale.group = key.group;
      locale.item = key.item;
      if (key.translated) {
        locale.text = key.text;
      } else {
        locale.text = await AwsHelper.translate(Language.English.awsCode, key.language.awsCode, key.text);
      }
      await locale.save();

      locales.push(locale);
    }

    this.info(`added ${locales.length} locales`);
  }

  localeKeysWithLanguage(base) {
    return this.localeKeys(base)
      .map(key => {
        const textI18n = _.isObject(key.text) ? key.text : {
          [Language.English.localeCode]: key.text,
        };

        return Language.getSupportedLanguages()
          .map(language => {
            return {
              ...key,
              language: language,
              locale: language.localeCode,
              text: textI18n[language.localeCode] || textI18n[Language.English.localeCode],
              translated: textI18n[language.localeCode] != null,
            };
          });
      })
      .flatten();
  }

  localeKeys(base) {
    const path = UpdateLocale.LocaleDirectory + (base ? UpdateLocale.BaseJson : UpdateLocale.AppJson);
    const localeJson = require(path);
    return Object.entries(localeJson)
      .map(groupEntry => Object.entries(groupEntry[1])
        .map(itemEntry => {
          return {
            group: groupEntry[0],
            item: itemEntry[0],
            text: itemEntry[1],
          };
        }))
      .flatten();
  }

}

module.exports = UpdateLocale;
