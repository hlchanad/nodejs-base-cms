'use strict';

const moment = require('moment');

const { ServiceProvider } = require('@adonisjs/fold');

class EdgeGeneralUtilProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register () {
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot () {
    const Env = this.app.use('Adonis/Src/Env');
    const View = this.app.use('Adonis/Src/View');
    const RouteHelper = this.app.use('App/Base/Helpers/RouteHelper');
    const ListGlobals = this.app.use("App/Base/Providers/Globals/ListGlobals");
    const CmsUserPreferenceKey = this.app.use("App/Base/Constants/CmsUserPreferenceKey");

    View.global('currentYear', () => moment().get('year'));
    View.global('toIso8601', (date) => moment(date).format("YYYY-MM-DD HH:mm:ss"));
    View.global('toDate', (date) => moment(date).format("YYYY-MM-DD"));
    View.global('toTime', (date) => moment(date).format("HH:mm:ss"));
    View.global('getUrl', (endpoint) => RouteHelper.getUrl(endpoint));
    View.global('GoogleMapApiKey', Env.get("GOOGLE_MAPS_API_KEY"));
    View.global('datatableOnClickRowAction', Env.get("DATATABLE_ON_CLICK_ROW_ACTION"));

    View.global('CmsUserPreferenceKey', Object.entries(CmsUserPreferenceKey)
      .reduce((acc, entry) => {
        return {
          ...acc,
          [entry[0]]: entry[1],
        };
      }, {}));

    View.global('list', new ListGlobals());
  }
}

module.exports = EdgeGeneralUtilProvider;
