'use strict';

const { ServiceProvider } = require('@adonisjs/fold');

class CustomValidatorProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    const Validator = this.app.use('Validator');

    Validator.extend('unique', this.app.use('App/Base/Validators/UniqueValidator'));
    Validator.extend('databaseRecord', this.app.use('App/Base/Validators/DatabaseRecordValidator'));
    Validator.extend('idOrImage', this.app.use('App/Base/Validators/IdOrImageValidator'));
    Validator.extend('latitude', this.app.use('App/Base/Validators/LatitudeValidator'));
    Validator.extend('longitude', this.app.use('App/Base/Validators/LongitudeValidator'));
    Validator.extend('dateTime', this.app.use('App/Base/Validators/DateTimeValidator'));
    Validator.extend('date', this.app.use('App/Base/Validators/DateValidator'));
    Validator.extend('time', this.app.use('App/Base/Validators/TimeValidator'));
  }
}

module.exports = CustomValidatorProvider;
