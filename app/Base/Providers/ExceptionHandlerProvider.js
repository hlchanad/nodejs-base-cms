'use strict';

const { ServiceProvider } = require('@adonisjs/fold');

class ExceptionHandlerProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    const Exception = this.app.use('Adonis/Src/Exception');

    const exceptionHandlers = [
      {
        exception: "InvalidSessionException",
        handler: this.app.use("App/Base/Exceptions/AuthRequiredException"),
      }
    ];

    exceptionHandlers.forEach(exceptionHandler =>
      Exception.handle(exceptionHandler.exception, async (error, context) => new exceptionHandler.handler().handle(error, context)));
  }
}

module.exports = ExceptionHandlerProvider;
