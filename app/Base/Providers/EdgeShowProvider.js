'use strict';

const { ServiceProvider } = require('@adonisjs/fold');

class EdgeShowProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    const View = this.app.use('Adonis/Src/View');
    const Helpers = this.app.use('Adonis/Src/Helpers');
    const I18nHelper = this.app.use("App/Base/Helpers/I18nHelper");
    const ShowGlobals = this.app.use("App/Base/Providers/Globals/ShowGlobals");

    View.global('show', new ShowGlobals(View, Helpers, I18nHelper));
  }
}

module.exports = EdgeShowProvider;
