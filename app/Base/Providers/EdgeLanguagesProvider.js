'use strict';

const { ServiceProvider } = require('@adonisjs/fold');

class EdgeLanguagesProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register () {
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot () {
    const View = this.app.use("Adonis/Src/View");
    const I18nHelper = this.app.use("App/Base/Helpers/I18nHelper");
    const Language = this.app.use("App/Base/Models/Common/Language");

    View.global("languages", () => Language.getSupportedLanguages());
    View.global("currentLocale", () => I18nHelper.currentLocale());
    View.global("currentLocaleTitle", () => I18nHelper.currentLocaleTitle());
    View.global("i18n", (key, props) => I18nHelper.formatMessage(key, props));
  }
}

module.exports = EdgeLanguagesProvider;
