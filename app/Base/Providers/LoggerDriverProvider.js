'use strict';

const _ = require("lodash");

const { ServiceProvider } = require('@adonisjs/fold');

class LoggerDriverProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    const FileRotateDriver = this.app.use("App/Base/Logger/Drivers/FileRotate");
    this.app.extend("Adonis/Src/Logger", "rotate", () => new FileRotateDriver());
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
  }
}

module.exports = LoggerDriverProvider;
