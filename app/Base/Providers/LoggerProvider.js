'use strict';

const { ServiceProvider } = require('@adonisjs/fold');

class LoggerProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    const Logger = this.app.use("App/Base/Logger/Logger");
    this.app.singleton("Log", () => new Logger());
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
  }
}

module.exports = LoggerProvider;
