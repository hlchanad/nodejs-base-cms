'use strict';

const _ = require("lodash");

const { ServiceProvider } = require('@adonisjs/fold');

class ArrayExtensionProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    Array.prototype.distinct = function (func) {
      return _.uniqBy(this, func);
    };
    Array.prototype.flatten = function () {
      return _.flatten(this);
    };
  }
}

module.exports = ArrayExtensionProvider;
