const _ = require("lodash");

class ListGlobals {

  contains(list, value) {
    return this.ensureList(list).some(item => item == value);
  }

  ensureList(list, delimiter = ",") {
    if (!_.isArray(list) && !_.isString(list)) throw new Error("invalid `list`");
    return Array.isArray(list) ? list : list.split(delimiter);
  }

}

module.exports = ListGlobals;
