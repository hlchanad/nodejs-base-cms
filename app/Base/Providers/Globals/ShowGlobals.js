const moment = require("moment");
const fs = require("fs");

const RouteHelper = use("App/Base/Helpers/RouteHelper");

class ShowGlobals {

  constructor(View, Helpers, I18nHelper) {
    this.View = View;
    this.Helpers = Helpers;
    this.I18nHelper = I18nHelper;
  }

  field(label, value) {
    const templateUrl = this.Helpers.viewsPath("base/components/show/show-field.edge");
    const template = fs.readFileSync(templateUrl, { encoding: "utf8" });
    return this.View.renderString(template, {
      label: label,
      value: value,
    });
  }

  text(module, model, field) {
    return this.field(this.I18nHelper.formatMessage(`${module}.fields.${field}`), model[field]);
  }

  boolean(module, model, field) {
    return this.field(this.I18nHelper.formatMessage(`${module}.fields.${field}`), this.I18nHelper.formatMessage(`general.boolean.${model[field]}`));
  }

  datetime(module, model, field) {
    return this.field(this.I18nHelper.formatMessage(`${module}.fields.${field}`), moment(model[field]).format("YYYY-MM-DD HH:mm:ss"));
  }

  date(module, model, field) {
    return this.field(this.I18nHelper.formatMessage(`${module}.fields.${field}`), moment(model[field]).format("YYYY-MM-DD"));
  }

  time(module, model, field) {
    return this.field(this.I18nHelper.formatMessage(`${module}.fields.${field}`), moment(model[field]).format("HH:mm"));
  }

  image(module, model, field) {
    const templateUrl = this.Helpers.viewsPath("base/components/show/show-image.edge");
    const template = fs.readFileSync(templateUrl, { encoding: "utf8" });
    return this.View.renderString(template, {
      label: this.I18nHelper.formatMessage(`${module}.fields.${field}`),
      value: model[`${field}_url`],
    });
  }

  multiImages(module, model, field, oldImages) {
    const templateUrl = this.Helpers.viewsPath("base/components/show/show-multi-image.edge");
    const template = fs.readFileSync(templateUrl, { encoding: "utf8" });
    return this.View.renderString(template, {
      id: field,
      label: this.I18nHelper.formatMessage(`${module}.fields.${field}`),
      oldImages: oldImages,
    });
  }

  referenceLink(label, title, url) {
    const templateUrl = this.Helpers.viewsPath("base/components/show/show-reference-section.edge");
    const template = fs.readFileSync(templateUrl, { encoding: "utf8" });
    return this.View.renderString(template, {
      label: label,
      href: url,
      title: title,
    });
  }

  referenceSection(label, section, refId, title) {
    return this.referenceLink(label, title, RouteHelper.getUrl(`/${section}/${refId}`));
  }

  coordinate(module, model, id, latitudeId, longitudeId) {
    const templateUrl = this.Helpers.viewsPath("base/components/show/show-coordinate.edge");
    const template = fs.readFileSync(templateUrl, { encoding: "utf8" });
    return this.View.renderString(template, {
      label: this.I18nHelper.formatMessage(`${module}.fields.${id}`),
      id: id,
      latitudeValue: model[latitudeId],
      longitudeValue: model[longitudeId],
    });
  }

}

module.exports = ShowGlobals;
