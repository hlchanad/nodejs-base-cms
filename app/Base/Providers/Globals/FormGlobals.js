const _ = require("lodash");
const moment = require("moment");
const fs = require("fs");

const SelectOption = use("App/Base/Models/Common/SelectOption");

class FormGlobals {

  constructor(View, Helpers, I18nHelper) {
    this.View = View;
    this.Helpers = Helpers;
    this.I18nHelper = I18nHelper;
  }

  /**
   *
   * @param label
   * @param id
   * @param model
   * @param flashMessages
   * @param options
   * @param options: {
   *   value: string,
   *   placeholder: string,
   *   type: string,
   *   clazz: string,
   *   required: boolean,
   *   disabled: boolean,
   *   hidden: boolean,
   * }
   * @returns {String}
   */
  input(label, id, model, flashMessages = {}, options = {}) {
    return this.renderString("input", {
      label: label,
      id: id,
      value: _.get(model, id, _.get(options, "value", "")),
      placeholder: _.get(options, "placeholder", ""),
      type: _.get(options, "type", "text"),
      clazz: _.get(options, "clazz", ""),
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
    });
  }

  hidden(id, value) {
    return this.text("hidden", id, null, null, {
      value: value,
      hidden: true,
    });
  }

  text(label, id, model, flashMessages = {}, options = {}) {
    return this.input(label, id, model, flashMessages, { ...options, type: "text" });
  }

  password(label, id, model, flashMessages = {}, options = {}) {
    return this.input(label, id, model, flashMessages, { ...options, type: "password" });
  }

  email(label, id, model, flashMessages = {}, options = {}) {
    return this.input(label, id, model, flashMessages, { ...options, type: "email" });
  }

  number(label, id, model, flashMessages = {}, options = {}) {
    return this.input(label, id, model, flashMessages, { ...options, type: "number" });
  }

  textArea(label, id, model, flashMessages = {}, options = {}) {
    return this.renderString("textarea", {
      label: label,
      id: id,
      value: _.get(model, id, _.get(options, "value", "")),
      placeholder: _.get(options, "placeholder", ""),
      clazz: _.get(options, "clazz", ""),
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
    });
  }

  readonly(label, id, value) {
    return this.renderString("readonly", {
      label: label,
      id: id,
      value: value,
    });
  }

  boolean(label, id, model, flashMessages = {}, options = {}) {
    const selectOptions = [
      SelectOption.builder()
        .id(`${id}-true`)
        .title(this.I18nHelper.formatMessage("general.boolean.true"))
        .value("true")
        .build(),
      SelectOption.builder()
        .id(`${id}-false`)
        .title(this.I18nHelper.formatMessage("general.boolean.false"))
        .value("false")
        .build(),
    ];
    return this.select(label, id, model, selectOptions, flashMessages, options);
  }

  select(label, id, model, selectOptions, flashMessages = {}, options = {}) {

    const multiple = _.get(options, "multiple", false);

    let updatedSelectOptions;

    if (multiple) {
      let value = _.get(model, id, _.get(options, "value", []));
      if (value === "") value = [];
      updatedSelectOptions = selectOptions
        .map(option => {
          return {
            ...option,
            selected: value.some(v => option.value === v),
          };
        });
    } else {
      const value = _.get(model, id, _.get(options, "value", ""));
      updatedSelectOptions = selectOptions
        .map(option => {
          return {
            ...option,
            selected: !option.disabled && value + "" === option.value + "",
          };
        });
    }

    return this.renderString("dropdown", {
      label: label,
      id: id,
      value: null,
      selectOptions: updatedSelectOptions,
      clazz: _.get(options, "clazz", ""),
      multiple: multiple,
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
    });
  }

  multiSelect(label, id, model, selectOptions, flashMessages = {}, options = {}) {
    return this.select(label, id, model, selectOptions, flashMessages, { ...options, multiple: true });
  }

  selectWithGroup(label, id, model, selectOptionGroups, flashMessages = {}, options = {}) {
    const multiple = _.get(options, "multiple", false);

    let updatedSelectOptionGroups;

    if (multiple) {
      let value = _.get(model, id, _.get(options, "value", []));
      if (value === "") value = [];
      updatedSelectOptionGroups = selectOptionGroups
        .map(optionGroup => {
          return {
            ...optionGroup,
            selectOptions: optionGroup.selectOptions.map(option => {
              return {
                ...option,
                selected: value.some(v => option.value === v),
              };
            }),
          };
        });
    } else {
      const value = _.get(model, id, _.get(options, "value", ""));
      updatedSelectOptionGroups = selectOptionGroups
        .map(optionGroup => {
          return {
            ...optionGroup,
            selectOptions: optionGroup.selectOptions.map(option => {
              return {
                ...option,
                selected: !option.disabled && value == option.value,
              };
            }),
          };
        });
    }

    return this.renderString("dropdown-with-group", {
      label: label,
      id: id,
      value: null,
      selectOptionGroups: updatedSelectOptionGroups,
      clazz: _.get(options, "clazz", ""),
      multiple: multiple,
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
    });
  }

  multiSelectWithGroup(label, id, model, selectOptionGroups, flashMessages = {}, options = {}) {
    return this.selectWithGroup(label, id, model, selectOptionGroups, flashMessages, { ...options, multiple: true });
  }

  image(label, id, model, flashMessages = {}, options = {}) {
    return this.renderString("image", {
      label: label,
      id: id,
      value: _.get(model, id, _.get(options, "value", "")),
      imageUrl: _.get(model, `${id}_url`),
      placeholder: _.get(options, "placeholder", ""),
      type: _.get(options, "type", "text"),
      clazz: _.get(options, "clazz", ""),
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
    });
  }

  multiImages(label, id, model, oldImages, flashMessages = {}, options = {}) {
    return this.renderString("multi-image", {
      label: label,
      id: id,
      value: _.get(model, id, _.get(options, "value", "")),
      placeholder: _.get(options, "placeholder", ""),
      type: _.get(options, "type", "text"),
      clazz: _.get(options, "clazz", ""),
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
      folder: _.get(options, "folder"),
      isPublic: _.get(options, "isPublic", true),
      oldImages: oldImages,
    });
  }

  dateTime(label, id, model, flashMessages = {}, options = {}) {
    return this.renderString("date-time", {
      label: label,
      id: id,
      value: moment(_.get(model, id, _.get(options, "value", new Date()))).format("YYYY-MM-DD HH:mm:ss"),
      clazz: _.get(options, "clazz", ""),
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
      max: _.get(options, "max"),
      min: _.get(options, "min"),
    });
  }

  date(label, id, model, flashMessages = {}, options = {}) {
    return this.renderString("date", {
      label: label,
      id: id,
      value: moment(_.get(model, id, _.get(options, "value", new Date()))).format("YYYY-MM-DD"),
      clazz: _.get(options, "clazz", ""),
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
      max: _.get(options, "max"),
      min: _.get(options, "min"),
    });
  }

  time(label, id, model, flashMessages = {}, options = {}) {
    return this.renderString("time", {
      label: label,
      id: id,
      value: moment(_.get(model, id, _.get(options, "value", new Date()))).format("HH:mm"),
      clazz: _.get(options, "clazz", ""),
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
      max: _.get(options, "max"),
      min: _.get(options, "min"),
      minuteStep: _.get(options, "minuteStep"),
    });
  }

  coordinate(label, id, latitudeId, longitudeId, model, flashMessages = {}, options = {}) {
    return this.renderString("coordinate", {
      id: id,
      label: label,
      latitudeId: latitudeId,
      longitudeId: longitudeId,
      value: _.get(model, id, _.get(options, "value", "")),
      latitudeValue: _.get(model, latitudeId, _.get(options, "latitudeValue", "")),
      longitudeValue: _.get(model, longitudeId, _.get(options, "longitudeValue", "")),
      placeholder: _.get(options, "placeholder", ""),
      type: _.get(options, "type", "text"),
      clazz: _.get(options, "clazz", ""),
      required: _.get(options, "required", true),
      disabled: _.get(options, "disabled", false),
      hidden: _.get(options, "hidden", false),
      flashMessages: flashMessages,
    });
  }

  getTemplate(viewPath) {
    const templateUrl = this.Helpers.viewsPath(viewPath);
    return fs.readFileSync(templateUrl, { encoding: "utf8" });
  }

  renderString(field, props) {
    return this.View.renderString(this.getTemplate(`base/components/form/field-${field}.edge`), props);
  }

}

module.exports = FormGlobals;
