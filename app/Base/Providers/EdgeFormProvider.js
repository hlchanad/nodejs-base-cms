'use strict';

const { ServiceProvider } = require('@adonisjs/fold');

class EdgeFormProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    const View = this.app.use('Adonis/Src/View');
    const Helpers = this.app.use('Adonis/Src/Helpers');
    const I18nHelper = this.app.use("App/Base/Helpers/I18nHelper");
    const FormGlobals = this.app.use("App/Base/Providers/Globals/FormGlobals");

    View.global('form', new FormGlobals(View, Helpers, I18nHelper));
  }
}

module.exports = EdgeFormProvider;
