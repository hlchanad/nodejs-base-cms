"use strict";
/** @typedef {import("@adonisjs/framework/src/Request")} Request */
/** @typedef {import("@adonisjs/framework/src/Response")} Response */
/** @typedef {import("@adonisjs/framework/src/View")} View */

const _ = require("lodash");
const pluralize = require("pluralize");

const Route = use('Route');

const RouteHelper = use("App/Base/Helpers/RouteHelper");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const Breadcrumb = use("App/Base/Models/Common/Breadcrumb");

class BreadcrumbHandler {

  static get inject() {
    return [ "Context" ];
  }

  constructor(context) {
    this.context = context;
  }

  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({request, view}, next) {
    const route = Route.match(request.url(), request.method());

    const packages = route.route.handler.substring(0, route.route.handler.lastIndexOf("/"));
    const controllerAndAction = route.route.handler.substring(route.route.handler.lastIndexOf("/") + 1);
    const controller = controllerAndAction.split(".")[0];
    const action = controllerAndAction.split(".")[1];
    const module = controller.substring(0, controller.indexOf("Controller"));

    const defaultBreadcrumbs = [
      new Breadcrumb(I18nHelper.formatMessage(Breadcrumb.Root.title), Breadcrumb.Root.url),
    ];

    const controllerBreadcrumb = this.getControllerBreadcrumb(packages, controller, module);
    if (controllerBreadcrumb != null) {
      defaultBreadcrumbs.push(controllerBreadcrumb);
    }

    const actionBreadcrumb = this.getActionBreadcrumb(route, action);
    if (defaultBreadcrumbs[defaultBreadcrumbs.length-1].url !== actionBreadcrumb.url) {
      defaultBreadcrumbs.push(actionBreadcrumb);
    }

    view.locals = {
      ...view.locals,
      breadcrumbs: defaultBreadcrumbs,
    };

    await next();
  }

  getControllerBreadcrumb(packages, controller, module) {
    const indexHandler = (packages === "" ? controller : packages + "/" + controller) + ".index";

    const index = Route.list().find(route => route.handler === indexHandler);

    if (index == null) return null;

    const messageKey = pluralize.plural(_.kebabCase(module)) + ".title";

    return new Breadcrumb(I18nHelper.formatMessage(messageKey), RouteHelper.getUrl(index._route));
  }

  getActionBreadcrumb(route, action) {
    let messageKey = "general.";

    if (action === "index") {
      messageKey += "list";
    } else {
      messageKey += action;
    }

    return new Breadcrumb(I18nHelper.formatMessage(messageKey), RouteHelper.getUrl(route.url))
  }

}

module.exports = BreadcrumbHandler;
