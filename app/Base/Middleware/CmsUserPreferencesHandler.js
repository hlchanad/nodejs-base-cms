"use strict";

const CmsUserPreferenceService = use("App/Base/Services/CmsUserPreferenceService");
const CmsUserPreferenceKey = use("App/Base/Constants/CmsUserPreferenceKey");
const ContextKey = use("App/Base/Constants/ContextKey");
const Language = use("App/Base/Models/Common/Language");

class CmsUserPreferencesHandler {

  static get inject() {
    return [ "Context" ];
  }

  constructor(context) {
    this.context = context;
  }

  async handle({ request, view, auth, session }, next) {
    const preferenceMap = {
      [CmsUserPreferenceKey.CMS_Language]: await CmsUserPreferenceService.getCmsUserPreference(
        auth.user, CmsUserPreferenceKey.CMS_Language, Language.English.dbCode, true),
    };
    this.context.set(ContextKey.CMS_User_Preferences, preferenceMap);
    session.put(ContextKey.CMS_User_Preferences, preferenceMap);
    await next();
  }

}

module.exports = CmsUserPreferencesHandler;
