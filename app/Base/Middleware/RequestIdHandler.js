"use strict";

const RequestIdHelper = use("App/Base/Helpers/RequestIdHelper");
const CustomHeader = use("App/Base/Constants/CustomHeader");
const ContextKey = use("App/Base/Constants/ContextKey");

class RequestIdHandler {

  static get inject() {
    return [ "Context" ];
  }

  constructor(context) {
    this.context = context;
  }

  async handle({ request, response, view, auth, session }, next) {
    const requestId = RequestIdHelper.generate();

    this.context.set(ContextKey.Request_ID, requestId);
    session.put(ContextKey.Request_ID, requestId);
    response.header(CustomHeader.Request_ID, requestId);

    await next();
  }

}

module.exports = RequestIdHandler;
