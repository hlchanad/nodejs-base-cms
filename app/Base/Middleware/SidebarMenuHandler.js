"use strict";

const SidebarMenuHelper = use("App/Base/Helpers/SidebarMenuHelper");

class SidebarMenuHandler {

  async handle({ request, view, auth }, next) {

    view.locals = {
      ...view.locals,
      sidebarMenu: await SidebarMenuHelper.getSidebarMenu(auth.user, request.url()),
    };

    await next();
  }


}

module.exports = SidebarMenuHandler;
