"use strict";
/** @typedef {import("@adonisjs/framework/src/Request")} Request */
/** @typedef {import("@adonisjs/framework/src/Response")} Response */
/** @typedef {import("@adonisjs/framework/src/View")} View */

const _ = require("lodash");

const Env = use("Env");

const Logger = use("Log");

class RequestLogHandler {

  static get inject() {
    return [ "Context" ];
  }

  constructor(context) {
    this.context = context;
  }

  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, response, auth, session }, next) {

    const doFilter = this.doFilter(request);

    if (doFilter) {
      const skipHeaders = Env.get("REQUEST_LOG_SKIP_HEADERS", "").split(",").filter(header => header);

      const headers = Object.entries(request.headers())
        .map(headerEntry => skipHeaders.includes(headerEntry[0]) ? [ headerEntry[0], "skipped ..." ] : headerEntry)
        .reduce((acc, headerEntry) => {
          return {
            ...acc,
            [headerEntry[0]]: headerEntry[1],
          };
        }, {});

      Logger.info("endpoint: %s %s", request.method(), request.originalUrl());
      Logger.info("auth.user.id: %s", _.get(auth.user, "id", "no auth.user"));
      this.printObject("headers", headers, "debug");
      this.printObject("query parameters", request.get(), "debug");
      this.printObject("request body", request.post(), "debug");
      this.printObject("files", request.files(), "debug");
    }

    await next();

    if (doFilter) {
      if (request.ajax()) {
        const logLevel = this.ifSuccessStatus(request.response.statusCode) ? "debug" : "error";
        this.printObject("response body", _.get(response.lazyBody, "content", {}), logLevel);
      } else {
        const exception = _.get(request.flashMessages, "exception");
        if (exception) {
          this.printObject("view exception", exception, "error");
        }
      }
    }
  }

  doFilter(request) {
    const method = request.method();

    return !Env.get("REQUEST_LOG_EXCLUDE_ROUTES", "").split(",")
      .filter(endpoint => endpoint)
      .map(endpoint => {
        const [ method, route ] = endpoint.split(" ");
        return { method, route };
      })
      .some(endpoint => endpoint.method === method && request.match(endpoint.route));
  }

  ifSuccessStatus(status) {
    return status >= 200 && status < 300;
  }

  printObject(tag, object, level = "info") {
    Logger[level](`${tag} ${Object.entries(object).length > 0 ? "\n" : ""}%s`, JSON.stringify(object, null, 2));
  }

}

module.exports = RequestLogHandler;
