"use strict";

const BaseSessionKey = use("App/Base/Constants/BaseSessionKey");

class CmsUserPreferencesHandler {

  static get inject() {
    return [ "Context" ];
  }

  constructor(context) {
    this.context = context;
  }

  async handle({ request, view, session, auth }, next) {
    if (!request.ajax()) {
      session.put(BaseSessionKey.LAST_ROUTE, {
        url: request.originalUrl(),
        t: new Date().getTime(),
      });
    }
    await next();
  }

}

module.exports = CmsUserPreferencesHandler;
