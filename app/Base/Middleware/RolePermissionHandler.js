"use strict";

const Route = use('Route');

const PermissionHelper = use("App/Base/Helpers/PermissionHelper");
const NoPermissionException = use("App/Base/Exceptions/NoPermissionException");

class RolePermissionHandler {

  async handle({ request, view, auth }, next) {

    const route = Route.match(request.url(), request.method());
    const handler = route.route.handler.substr(route.route.handler.lastIndexOf("/") + 1);
    const [ controller, method ] = handler.split(".");

    const allowed = await PermissionHelper.canAccess(auth.user, controller, method);

    if (allowed) {
      await next();
    } else {
      throw new NoPermissionException();
    }

  }


}

module.exports = RolePermissionHandler;
