const LatitudeValidator = async (data, field, message, args, get) => {

  const latitude = parseFloat(get(data, field));

  if (isNaN(latitude) || latitude < -90 || latitude > 90) {
    return Promise.reject(message);
  }

  return Promise.resolve();
};

module.exports = LatitudeValidator;
