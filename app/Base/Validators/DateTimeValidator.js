const _ = require("lodash");
const moment = require("moment");

const DateTimeValidator = async (data, field, message, args, get) => {

  const date = get(data, field);

  if (!_.isEmpty(date) && !moment(date, "YYYY-MM-DD HH:mm:ss").isValid()) {
    return Promise.reject(message);
  }

  return Promise.resolve();
};

module.exports = DateTimeValidator;
