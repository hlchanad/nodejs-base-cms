const Database = use("Database");

const UniqueValidator = async (data, field, message, args, get) => {
  const [ table, column, ignoreField, ignoreValue ] = args;

  if (table == null
    || column == null
    || ignoreField == null && ignoreValue != null
    || ignoreField != null && ignoreValue == null ) {
    throw new Error("usage: `unique:table,column(,ignoreField,ignoreValue)`");
  }

  const count = await Database
    .from(table)
    .where((query) => {
      query.where(field, get(data, field));
      query.where("is_deleted", false);

      if (ignoreField != null) {
        query.where(ignoreField, '<>', ignoreValue);
      }
    })
    .getCount();

  return count <= 0 ? Promise.resolve() : Promise.reject(message);
};

module.exports = UniqueValidator;
