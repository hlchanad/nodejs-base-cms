const LongitudeValidator = async (data, field, message, args, get) => {

  const longitude = parseFloat(get(data, field));

  if (isNaN(longitude) || longitude < -180 || longitude > 180) {
    return Promise.reject(message);
  }

  return Promise.resolve();
};

module.exports = LongitudeValidator;
