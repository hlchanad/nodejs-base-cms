const _ = require("lodash");
const bytes = require("bytes");

const ImageService = use("App/Base/Services/ImageService");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

const rules = ImageService.validationRules();

const isValidFile = file => file != null && file.constructor.name === "File";

const isValidFileType = file => rules.types == null || rules.types.includes(file.type);

const isValidFileSize = file => rules.size == null || bytes(rules.size) >= file.size;

const ifValidExtensions = file => rules.extnames == null || rules.extnames.includes(file.subtype);

const IdOrImageValidator = async (data, field, message, args, get) => {

  let required = args[0] === "true";

  const oldId = get(data, `${field}_old`);
  const image = get(data, field);

  if (image != null) {
    if (!isValidFile(image)) {
      return Promise.reject(I18nHelper.formatMessage("validator.error-messages.file"));
    }

    if (!isValidFileType(image)) {
      return Promise.reject(I18nHelper.formatMessage("validator.error-messages.file-types"));
    }

    if (!isValidFileSize(image)) {
      return Promise.reject(I18nHelper.formatMessage("validator.error-messages.file-size", { size: rules.size }));
    }

    if (!ifValidExtensions(image)) {
      return Promise.reject(I18nHelper.formatMessage("validator.error-messages.file-extnames"));
    }
  }

  if (required) {
    if (_.isEmpty(oldId) && image == null) {
      return Promise.reject(message);
    }
  }

  return Promise.resolve();
};

module.exports = IdOrImageValidator;
