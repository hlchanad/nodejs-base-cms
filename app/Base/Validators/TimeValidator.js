const _ = require("lodash");
const moment = require("moment");

const TimeValidator = async (data, field, message, args, get) => {

  const date = get(data, field);

  if (!_.isEmpty(date) && !moment(date, "HH:mm").isValid()) {
    return Promise.reject(message);
  }

  return Promise.resolve();
};

module.exports = TimeValidator;
