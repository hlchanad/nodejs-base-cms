const _ = require("lodash");
const moment = require("moment");

const DateValidator = async (data, field, message, args, get) => {

  const date = get(data, field);

  if (!_.isEmpty(date) && !moment(date, "YYYY-MM-DD").isValid()) {
    return Promise.reject(message);
  }

  return Promise.resolve();
};

module.exports = DateValidator;
