const _ = require("lodash");

const Database = use("Database");

const DatabaseRecordValidator = async (data, field, message, args, get) => {
  let [ table, column = "id"] = args;

  if (table == null) {
    throw new Error("usage: `db:table(,column = 'id')`");
  }

  const value = get(data, field);

  const keys = value == null ? [] : (Array.isArray(value) ? value : [value]);

  const count = await Database
    .from(table)
    .where((query) => {
      query.whereIn(column, keys);
      query.where("is_deleted", false);
      query.where("is_visible", true);
    })
    .getCount();

  return keys.length === count ? Promise.resolve() : Promise.reject(message);
};

module.exports = DatabaseRecordValidator;
