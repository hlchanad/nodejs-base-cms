"use strict";

const AdonisExceptions = require("@adonisjs/generic-exceptions");

const Env = use("Env");
const BaseExceptionHandler = use("BaseExceptionHandler");

const Logger = use("Log");
const CustomHeader = use("App/Base/Constants/CustomHeader");
const ContextKey = use("App/Base/Constants/ContextKey");
const CmsUserPreferenceKey = use("App/Base/Constants/CmsUserPreferenceKey");
const Language = use("App/Base/Models/Common/Language");
const UnexpectedException = use("App/Base/Exceptions/UnexpectedException");
const ApiResponse = use("App/Base/Models/Common/ApiResponse");
const ApiStatus = use("App/Base/Models/Common/ApiStatus");

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle(error, ctx) {

    if (this.ifExpectedError(error) || Env.get("NODE_ENV") !== "production") {
      return super.handle(error, ctx);
    }

    const { request, response, session } = ctx;

    if (request.ajax()) {
      return response.internalServerError(new ApiResponse(ApiStatus.INTERNAL_SERVER.toMultiLanguage()));
    }

    return new UnexpectedException(error).handle(error, { response, session });
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report(error, { request }) {
    const requestId = request.response.getHeader(CustomHeader.Request_ID);

    const message = (this.ifExpectedError(error) ? "" : "unexpected ") + "error: %s\n%s";
    Logger.error(message, error.message || "", error.stack, requestId);
  }

  ifExpectedError(error) {
    return Object.values(AdonisExceptions).some(exception => error instanceof exception);
  }

  getLocaleCode(session) {
    const languageCode = _.get(session.get(ContextKey.CMS_User_Preferences), CmsUserPreferenceKey.CMS_Language, Language.English.dbCode);

    const language = Language.of("dbCode", languageCode);

    return language.localeCode;
  }

}

module.exports = ExceptionHandler;
