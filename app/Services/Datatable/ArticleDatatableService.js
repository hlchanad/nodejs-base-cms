const BaseDatatableService = use("App/Base/Services/Datatable/BaseDatatableService");
const DatatableColumn = use("App/Base/Models/Common/DatatableColumn");
const I18nHelper = use("App/Base/Helpers/I18nHelper");

class ArticleDatatableService extends BaseDatatableService {

  static controller() {
    return "ArticleController";
  }

  static section() {
    return "articles";
  }

  static datatableColumns() {
    const currentLocaleTitle = I18nHelper.currentLocaleTitle();

    return [
      new DatatableColumn("id", I18nHelper.formatMessage("articles.fields.id")),
      new DatatableColumn("post_date", I18nHelper.formatMessage("articles.fields.post_date")),
      new DatatableColumn("title", `${I18nHelper.formatMessage("articles.fields.title")} (${currentLocaleTitle})`),
      new DatatableColumn("actionButtons", I18nHelper.formatMessage("general.index.datatable.action-buttons"), false),
    ];
  }

  static sql(request, user) {
    const currentLocaleDbCode = I18nHelper.currentLocaleDbCode();

    return `
      SELECT
        t.*,
        td.title,
        td.content,
        null AS actionButtons
      FROM articles t
      LEFT JOIN article_details td ON t.id = td.ref_id AND td.language = '${currentLocaleDbCode}'
      WHERE t.is_deleted = false
        AND t.is_visible = true
    `;
  }

}

module.exports = ArticleDatatableService;
