"use strict";

const BaseService = use("App/Base/Services/BaseService");
const ImageService = use("App/Base/Services/ImageService");
const Language = use("App/Base/Models/Common/Language");

const Article = use("App/Models/Article");
const ArticleImage = use("App/Models/ArticleImage");
const ArticleDetail = use("App/Models/ArticleDetail");

class ArticleService extends BaseService {

  static async upsertArticle(article, request, thumbnailId, user) {
    article.post_date = request.post_date;
    article.thumbnail_id = thumbnailId;
    article.latitude = request.latitude;
    article.longitude = request.longitude;
    await article.save(user);

    const details = await article.details().fetch();

    for (let i = 0; i < Language.getSupportedLanguages().length; i++) {
      const language = Language.getSupportedLanguages()[i];

      let detail = details.rows.find(detail => detail.language === language.dbCode) || new ArticleDetail();
      detail.ref_id = article.id;
      detail.language = language.dbCode;
      detail.title = request.details[language.dbCode].title;
      detail.content = request.details[language.dbCode].content;
      await detail.save();
    }

    const articleImages = await article.articleImages().fetch();
    await ImageService.upsertImages(articleImages, request.image_ids,
      ArticleImage, "article_id", article.id, user);
  }

}

module.exports = ArticleService;
