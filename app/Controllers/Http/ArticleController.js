"use strict";

const moment = require("moment");

const BaseController = use("App/Base/Controllers/Http/BaseController");
const I18nHelper = use("App/Base/Helpers/I18nHelper");
const RouteHelper = use("App/Base/Helpers/RouteHelper");
const DetailPageHelper = use("App/Base/Helpers/DetailPageHelper");
const ValidationHelper = use("App/Base/Helpers/ValidationHelper");
const RecordNotFoundException = use("App/Base/Exceptions/RecordNotFoundException");
const LastUpdatedAtNotMatchException = use("App/Base/Exceptions/LastUpdatedAtNotMatchException");
const FailedToUpdateRecordException = use("App/Base/Exceptions/FailedToUpdateRecordException");
const ValidationException = use("App/Base/Exceptions/ValidationException");
const ApiStatus = use("App/Base/Models/Common/ApiStatus");
const ImageService = use("App/Base/Services/ImageService");
const ArticleService = use("App/Services/ArticleService");
const ArticleDatatableService = use("App/Services/Datatable/ArticleDatatableService");
const Article = use("App/Models/Article");

class ArticleController extends BaseController {

  async index({ request, view, auth }) {
    const columns = ArticleDatatableService.datatableColumns();

    if (request.ajax()) {
      return ArticleDatatableService.datatableResponse(request.get(), auth.user);
    }

    return this.render(view, "articles.index", {
      title: I18nHelper.formatMessage("articles.title"),
      allowCreate: true,
      createUrl: RouteHelper.getUrl("/articles/create"),
      allowBatchDelete: true,
      batchDeleteMethod: "POST",
      batchDeleteUrl: RouteHelper.getUrl("/articles/delete-requests"),
      datatable: {
        tableId: "articles",
        columns: columns,
      },
    });
  }

  async show({ request, view, auth }) {
    const id = request.params.id;

    const article = await Article.getWithMutator(id);

    if (article == null) {
      throw new RecordNotFoundException("articles", id);
    }

    const images = await Promise.all((await article.images())
      .map(async image => {
        image.url = await ImageService.getImageUrl(image);
        return image;
      }));

    article.thumbnail_id_url = await ImageService.getImageUrl(article.thumbnail_id);

    return this.render(view, "articles.show", {
      title: I18nHelper.formatMessage("articles.title") + " - " + id,
      module: "articles",
      model: article,
      modelDetails: await article.getDetailsMap(),
      detailButtonsHtml: await DetailPageHelper.getDefaultDetailButtons(auth.user, "ArticleController", "articles", article),
      images: images,
    });
  }

  async create({ view }) {
    return this.render(view, "articles.create", {
      title: I18nHelper.formatMessage("articles.title") + " - " + I18nHelper.formatMessage("general.create"),
      module: "articles",
      formConfig: {
        method: "POST",
        action: RouteHelper.getUrl("/articles"),
        cancelUrl: RouteHelper.getUrl("/articles"),
      },
      model: null,
    });
  }

  async store({ request, response, auth }) {
    const article = await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/articles/${article.id}`));
  }

  async edit({ request, view }) {
    const id = request.params.id;

    const article = await Article.get(id);

    if (article == null) {
      throw new RecordNotFoundException("articles", id);
    }

    const images = await Promise.all((await article.images())
      .map(async image => {
        image.url = await ImageService.getImageUrl(image);
        return image;
      }));

    article.thumbnail_id_url = await ImageService.getImageUrl(article.thumbnail_id);

    return this.render(view, "articles.edit", {
      title: I18nHelper.formatMessage("articles.title") + " - " + id,
      module: "articles",
      formConfig: {
        method: "PUT",
        action: RouteHelper.getUrl(`/articles/${id}`),
        cancelUrl: RouteHelper.getUrl("/articles"),
      },
      model: article,
      modelDetails: await article.getDetailsMap(),
      images: images,
    });
  }

  async update({ request, response, auth }) {
    const id = request.all().id;

    const article = await Article.get(id);

    if (article == null) {
      throw new RecordNotFoundException("articles", id);
    }

    if (article.updated_at != null && request.all().updated_at == null
      || !moment(article.updated_at).isSame(moment(request.all().updated_at))) {
      throw new LastUpdatedAtNotMatchException("articles", id);
    }

    await this.upsert({ request, auth });

    return response.redirect(RouteHelper.getUrl(`/articles/${article.id}`));
  }

  async destroy({ request, response, auth }) {
    const id = request.params.id;

    const article = await Article.get(id);

    if (article == null) {
      return response.notFound(this.api(ApiStatus.NOT_FOUND));
    }

    const success = await article.delete(auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    return this.api(ApiStatus.OK);
  }

  async batchDelete({ request, response, auth }) {
    const validation = await ValidationHelper.validate(request.all(), {
      ids: "array|required|databaseRecord:articles",
    });

    if (validation.fails()) {
      return response.badRequest(this.api(ApiStatus.BAD_REQUEST), validation.messages());
    }

    const success = await Article.batchDelete(request.all().ids, auth.user);
    if (!success) {
      return response.internalServerError(this.api(ApiStatus.DATABASE_ERROR));
    }

    return this.api(ApiStatus.OK);
  }

  async upsert({ request, auth }) {
    const id = request.all().id;
    const article = id == null ? new Article : await Article.get(id);

    const validation = await ValidationHelper.validate({
      ...request.all(),
      ...request.files(),
    }, {
      ...Article.validationRules(id),
      ...Article.fileValidationRules(id),
    });

    if (validation.fails()) {
      throw new ValidationException(validation, []);
    }

    const thumbnailId = await this.handleImage(request, "thumbnail_id", "articles", auth.user, []);

    try {
      await ArticleService.upsertArticle(article, request.all(), thumbnailId, auth.user);
    } catch (e) {
      throw new FailedToUpdateRecordException(e);
    }

    return article;
  }

}

module.exports = ArticleController;
