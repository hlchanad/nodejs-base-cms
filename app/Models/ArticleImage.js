"use strict";

const Model = use("App/Base/Models/BaseModel");

class ArticleImage extends Model {

  articles() {
    return this.hasOne("App/Models/Article", "article_id", "id");
  }

  image() {
    return this.hasOne("App/Base/Models/Image", "image_id", "id");
  }

}

module.exports = ArticleImage;
