"use strict";

const Model = use("App/Base/Models/BaseDetailModel");

class ArticleDetail extends Model {

  article() {
    return this.hasOne("App/Models/Article", "ref_id", "id");
  }

}

module.exports = ArticleDetail;
