"use strict";

const validations = require('indicative/validator').validations;

const Model = use("App/Base/Models/BaseModel");

class Article extends Model {

  details() {
    return this.hasMany("App/Models/ArticleDetail", "id", "ref_id", false);
  }

  articleImages() {
    return this.hasMany("App/Models/ArticleImage");
  }

  async images() {
    const articleImages = await this.articleImages().fetch();
    return Promise.all(articleImages.rows.map(articleImage => articleImage.image().first()));
  }

  static validationRules(id) {
    return {
      post_date: [
        validations.dateFormat(['YYYY-MM-DD HH:mm:ss']),
      ],
      latitude: "latitude|required",
      longitude: "longitude|required",
      image_ids: "databaseRecord:images",
      "details.*.title": "string|required",
      "details.*.content": "string|required",
    };
  }

  static fileValidationRules(id) {
    return {
      thumbnail_id: `idOrImage:false`,
    };
  }

}

module.exports = Article;
