"use strict";

const Schema = use('App/Base/Migrations/BaseSchema');

class ArticlesSchema extends Schema {
  up() {
    this.table("articles", (table) => {
      table.latitude("latitude").after("post_date");
      table.longitude("longitude").after("latitude");
    });
  }

  down() {
    this.table("articles", (table) => {
      table.dropColumn("latitude");
      table.dropColumn("longitude");
    });
  }
}

module.exports = ArticlesSchema;
