'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class ArticleSchema extends Schema {
  up () {
    this.create('articles', (table) => {
      table.id();
      table.datetime("post_date");
      table.manageable();
    });

    this.create('article_details', (table) => {
      table.id();
      table.refId('articles');
      table.language();
      table.string('title').notNullable();
      table.string('content').notNullable();
    });
  }

  down () {
    this.drop('article_details');
    this.drop('articles');
  }
}

module.exports = ArticleSchema;
