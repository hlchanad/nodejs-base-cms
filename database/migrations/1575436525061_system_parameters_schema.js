'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class SystemParametersSchema extends Schema {
  up () {
    this.create('system_parameters', (table) => {
      table.id();
      table.string('key').notNullable();
      table.string('value', 4095).notNullable();
      table.string('description', 4095);
      table.manageable();
    });
  }

  down () {
    this.drop('system_parameters');
  }
}

module.exports = SystemParametersSchema;
