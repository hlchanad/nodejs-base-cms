'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class LocalesSchema extends Schema {
  up () {
    this.create('locales', (table) => {
      table.id();
      table.string('locale').notNullable();
      table.string('group').notNullable();
      table.string('item').notNullable();
      table.text('text').notNullable();
      table.manageable();
    })
  }

  down () {
    this.drop('locales')
  }
}

module.exports = LocalesSchema;
