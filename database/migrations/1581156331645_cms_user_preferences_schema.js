'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class CmsUserPreferenceSchema extends Schema {
  up () {
    this.create('cms_user_preferences', (table) => {
      table.id();
      table.foreignKey('cms_user_id', 'cms_users', 'id').notNullable();
      table.string('key');
      table.string('value');
      table.manageable();
    });
  }

  down () {
    this.drop('cms_user_preferences');
  }
}

module.exports = CmsUserPreferenceSchema;
