'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class CmsRoleMenuSchema extends Schema {
  up () {
    this.create('cms_role_menus', (table) => {
      table.id();
      table.foreignKey('cms_role_id', 'cms_roles', 'id').notNullable();
      table.foreignKey('cms_menu_id', 'cms_menus', 'id').notNullable();
      table.manageable();
    });
  }

  down () {
    this.drop('cms_role_menus');
  }
}

module.exports = CmsRoleMenuSchema;
