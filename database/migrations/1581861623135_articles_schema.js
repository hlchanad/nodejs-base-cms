"use strict";

const Schema = use('App/Base/Migrations/BaseSchema');

class ArticlesSchema extends Schema {
  up() {
    this.table("articles", (table) => {
      table.foreignKey("thumbnail_id", "images", "id").after("post_date");
    })
  }

  down() {
    this.table("articles", (table) => {
      table.dropColumn("thumbnail_id");
    })
  }
}

module.exports = ArticlesSchema;
