'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class ArticleImagesSchema extends Schema {
  up () {
    this.create('article_images', (table) => {
      table.id();
      table.foreignKey('article_id', 'articles', 'id').notNullable();
      table.foreignKey('image_id', 'images', 'id').notNullable();
      table.manageable();
    })
  }

  down () {
    this.drop('article_images');
  }
}

module.exports = ArticleImagesSchema;
