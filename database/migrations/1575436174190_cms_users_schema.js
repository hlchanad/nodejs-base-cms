'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');
const Status = use('App/Base/Constants/Status');

class CmsUsersSchema extends Schema {
  up () {
    this.create('cms_users', (table) => {
      table.id();
      table.string('username').notNullable();
      table.string('password', 1023).notNullable();
      table.string('email', 1023).notNullable();
      table.manageable(Status.enums, false);
    })
  }

  down () {
    this.drop('cms_users');
  }
}

module.exports = CmsUsersSchema;
