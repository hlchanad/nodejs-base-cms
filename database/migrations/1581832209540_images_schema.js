'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class ImageSchema extends Schema {
  up () {
    this.create('images', (table) => {
      table.id();
      table.string("path", 1023).notNullable();
      table.string("file_name").notNullable();
      table.string("original_file_name", 1023).notNullable();
      table.string("mime_type").notNullable();
      table.integer("width").notNullable();
      table.integer("height").notNullable();
      table.integer("file_size").notNullable();
      table.string("disk").notNullable();
      table.boolean("public").notNullable();
      table.manageable();
    });
  }

  down () {
    this.drop('images');
  }
}

module.exports = ImageSchema;
