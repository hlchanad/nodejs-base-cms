'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class CmsPermissionSchema extends Schema {
  up () {
    this.create('cms_permissions', (table) => {
      table.id();
      table.string('title').notNullable();
      table.string('action').notNullable();
      table.manageable();
    });
  }

  down () {
    this.drop('cms_permissions');
  }
}

module.exports = CmsPermissionSchema;
