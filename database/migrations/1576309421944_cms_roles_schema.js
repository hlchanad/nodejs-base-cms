'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class CmsRolesSchema extends Schema {
  up () {
    this.create('cms_roles', (table) => {
      table.id();
      table.string('code').notNullable();
      table.string('title').notNullable();
      table.string('description', 4095);
      table.boolean('selectable').notNullable();
      table.manageable();
    })
  }

  down () {
    this.drop('cms_roles');
  }
}

module.exports = CmsRolesSchema;
