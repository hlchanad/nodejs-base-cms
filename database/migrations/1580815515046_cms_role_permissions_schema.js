'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class CmsRolePermissionSchema extends Schema {
  up () {
    this.create('cms_role_permissions', (table) => {
      table.id();
      table.foreignKey('cms_role_id', 'cms_roles', 'id').notNullable();
      table.foreignKey('cms_permission_id', 'cms_permissions', 'id').notNullable();
      table.manageable();
    });
  }

  down () {
    this.drop('cms_role_permissions');
  }
}

module.exports = CmsRolePermissionSchema;
