'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');
const CmsMenuIconType = use('App/Base/Constants/CmsMenuIconType');

class CmsMenuSchema extends Schema {
  up () {
    this.create('cms_menus', (table) => {
      table.id();
      table.foreignKey('parent_id', 'cms_menus', 'id');
      table.string('title').notNullable();
      table.string('endpoint');
      table.enu('icon_type', CmsMenuIconType.enums).notNullable();
      table.string('icon').notNullable();
      table.integer('sequence').notNullable();
      table.manageable();
    });
  }

  down () {
    this.drop('cms_menus');
  }
}

module.exports = CmsMenuSchema;
