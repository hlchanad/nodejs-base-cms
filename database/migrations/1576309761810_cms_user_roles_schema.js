'use strict';

const Schema = use('App/Base/Migrations/BaseSchema');

class CmsUserRolesSchema extends Schema {
  up () {
    this.create('cms_user_roles', (table) => {
      table.id();
      table.foreignKey('cms_user_id', 'cms_users', 'id').notNullable();
      table.foreignKey('cms_role_id', 'cms_roles', 'id').notNullable();
      table.manageable();
    })
  }

  down () {
    this.drop('cms_user_roles');
  }
}

module.exports = CmsUserRolesSchema;
