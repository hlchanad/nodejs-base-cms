"use strict";

const path = require("path");
const _ = require("lodash");

/** @type {import("@adonisjs/lucid/src/Factory")} */
const Factory = use("Factory");

const UpdateLocale = use("App/Base/Commands/UpdateLocale");

const BaseSeeder = use("App/Base/Migrations/BaseSeeder");

Array.prototype.flatten = function () {
  return _.flatten(this);
};

class LocaleSeeder extends BaseSeeder {

  constructor() {
    super();
    this.filename = path.basename(__filename);
  }

  async seed() {
    await new UpdateLocale().run({}, { base: true });
  }

}

module.exports = LocaleSeeder;
