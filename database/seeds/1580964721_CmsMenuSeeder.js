"use strict";

const path = require("path");

/** @type {import("@adonisjs/lucid/src/Factory")} */
const Factory = use("Factory");

const BaseSeeder = use("App/Base/Migrations/BaseSeeder");
const CmsMenu = use("App/Base/Models/CmsMenu");
const CmsMenuIconType = use("App/Base/Constants/CmsMenuIconType");

class CmsMenuSeeder extends BaseSeeder {

  constructor() {
    super();
    this.filename = path.basename(__filename);
  }

  async seed() {

    const userManagement = await this.createCmsMenu({
      parent: null,
      title: "Users",
      endpoint: null,
      iconType: CmsMenuIconType.FONT_AWESOME,
      icon: "fa fa-users",
      sequence: 800,
    });

    const settings = await this.createCmsMenu({
      parent: null,
      title: "Settings",
      endpoint: null,
      iconType: CmsMenuIconType.FONT_AWESOME,
      icon: "fa fa-gears",
      sequence: 900,
    });

    const cmsMenu = await this.createCmsMenu({
      parent: settings,
      title: "Navigation Menus",
      endpoint: "/cms-menus",
      iconType: CmsMenuIconType.PAGES,
      icon: "pg-menu",
      sequence: 100,
    });

    const locales = await this.createCmsMenu({
      parent: settings,
      title: "Locales",
      endpoint: "/locales",
      iconType: CmsMenuIconType.TEXT,
      icon: "A/Z",
      sequence: 200,
    });

    const systemParameters = await this.createCmsMenu({
      parent: settings,
      title: "System Parameters",
      endpoint: "/system-parameters",
      iconType: CmsMenuIconType.FONT_AWESOME,
      icon: "fa fa-gear",
      sequence: 300,
    });

    const cmsUsers = await this.createCmsMenu({
      parent: userManagement,
      title: "Users",
      endpoint: "/cms-users",
      iconType: CmsMenuIconType.FONT_AWESOME,
      icon: "fa fa-users",
      sequence: 100,
    });

    const cmsRoles = await this.createCmsMenu({
      parent: userManagement,
      title: "Roles",
      endpoint: "/cms-roles",
      iconType: CmsMenuIconType.TEXT,
      icon: "Ro",
      sequence: 200,
    });

    const cmsPermissions = await this.createCmsMenu({
      parent: userManagement,
      title: "Permissions",
      endpoint: "/cms-permissions",
      iconType: CmsMenuIconType.FONT_AWESOME,
      icon: "fa fa-check",
      sequence: 300,
    });

  }

  async createCmsMenu(props) {
    const cmsMenu = new CmsMenu();
    cmsMenu.parent_id = props.parent == null ? null : props.parent.id;
    cmsMenu.title = props.title;
    cmsMenu.endpoint = props.endpoint;
    cmsMenu.icon_type = props.iconType.key;
    cmsMenu.icon = props.icon;
    cmsMenu.sequence = props.sequence;
    await cmsMenu.save();
    return cmsMenu;
  }

}

module.exports = CmsMenuSeeder;
