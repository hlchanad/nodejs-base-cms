"use strict";

const path = require("path");

/** @type {import("@adonisjs/lucid/src/Factory")} */
const Factory = use("Factory");
const Hash = use("Hash");

const BaseSeeder = use("App/Base/Migrations/BaseSeeder");
const CmsUser = use("App/Base/Models/CmsUser");

class CmsUserSeeder extends BaseSeeder {

  constructor() {
    super();
    this.filename = path.basename(__filename);
  }

  async seed() {

    const systemUser = new CmsUser();
    systemUser.id = 1;
    systemUser.username = "system";
    systemUser.password = await Hash.make("system");
    systemUser.email = "system@base-cms.com";
    systemUser.is_visible = false;
    systemUser.created_by = null;
    systemUser.updated_by = null;
    await systemUser.save();

  }

}

module.exports = CmsUserSeeder;
