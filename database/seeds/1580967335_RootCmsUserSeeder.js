"use strict";

const path = require("path");

/** @type {import("@adonisjs/lucid/src/Factory")} */
const Factory = use("Factory");
const Hash = use("Hash");

const BaseSeeder = use("App/Base/Migrations/BaseSeeder");
const CmsUser = use("App/Base/Models/CmsUser");
const CmsUserRole = use("App/Base/Models/CmsUserRole");
const CmsRole = use("App/Base/Models/CmsRole");
const CmsRolePermission = use("App/Base/Models/CmsRolePermission");
const CmsPermission = use("App/Base/Models/CmsPermission");
const CmsRoleCode = use("App/Base/Constants/CmsRoleCode");

class RootCmsUserSeeder extends BaseSeeder {

  constructor() {
    super();
    this.filename = path.basename(__filename);
  }

  async seed() {

    const permissions = {
      dashboard: await this.createCmsPermission("Access Dashboard", "DashboardController.*"),
      cmsMenus: await this.createCmsPermission("Manage CMS Menu", "CmsMenuController.*"),
      cmsUsers: await this.createCmsPermission("Manage CMS Users", "CmsUserController.*"),
      cmsRoles: await this.createCmsPermission("Manage CMS Roles", "CmsRoleController.*"),
      cmsPermissions: await this.createCmsPermission("Manage CMS Permissions", "CmsPermissionController.*"),
      systemParameters: await this.createCmsPermission("Manage System Parameters", "SystemParameterController.*"),
      updateCmsUserPreference: await this.createCmsPermission("Update CMS User Preference", "CmsUserController.updatePreference"),
    };

    const roles = {
      root: await this.createCmsRole(CmsRoleCode.ROOT, "Root Account", "Can do everything", false),
    };

    const users = {
      root: await this.createCmsUser("admin", "admin", "admin@base-cms.com"),
    };

    await Promise.all([
      ...[
        permissions.dashboard,
        permissions.cmsMenus,
        permissions.cmsUsers,
        permissions.cmsRoles,
        permissions.cmsPermissions,
        permissions.systemParameters,
      ].map(async cmsPermission => {
        return [
          this.createCmsRolePermission(roles.root, cmsPermission),
        ];
      }),
    ]);

    await Promise.all([
      this.createCmsUserRole(users.root, roles.root),
    ]);

  }

  async createCmsPermission(title, action) {
    const cmsPermission = new CmsPermission();
    cmsPermission.title = title;
    cmsPermission.action = action;
    await cmsPermission.save();
    return cmsPermission;
  }

  async createCmsRole(code, title, description, selectable = "true", isVisible = true) {
    const cmsRole = new CmsRole();
    cmsRole.code = code;
    cmsRole.title = title;
    cmsRole.description = description;
    cmsRole.selectable = selectable;
    cmsRole.is_visible = isVisible;
    await cmsRole.save();
    return cmsRole
  }

  async createCmsUser() {
    const cmsUser = new CmsUser();
    cmsUser.username = "admin";
    cmsUser.password = await Hash.make("admin");
    cmsUser.email = "admin@base-cms.com";
    cmsUser.is_visible = true;
    await cmsUser.save();
    return cmsUser;
  }

  async createCmsRolePermission(cmsRole, cmsPermission) {
    const cmsRolePermission = new CmsRolePermission();
    cmsRolePermission.cms_role_id = cmsRole.id;
    cmsRolePermission.cms_permission_id = cmsPermission.id;
    await cmsRolePermission.save();
    return cmsRolePermission
  }

  async createCmsUserRole(cmsUser, cmsRole) {
    const cmsUserRole = new CmsUserRole();
    cmsUserRole.cms_user_id = cmsUser.id;
    cmsUserRole.cms_role_id = cmsRole.id;
    await cmsUserRole.save();
    return cmsUserRole;
  }

}

module.exports = RootCmsUserSeeder;
