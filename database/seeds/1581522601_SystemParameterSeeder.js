"use strict";

const path = require("path");

/** @type {import("@adonisjs/lucid/src/Factory")} */
const Factory = use("Factory");

const BaseSeeder = use("App/Base/Migrations/BaseSeeder");
const BaseSystemParameterKey = use("App/Base/Constants/BaseSystemParameterKey");
const SystemParameter = use("App/Base/Models/SystemParameter");

class SystemParameterSeeder extends BaseSeeder {

  constructor() {
    super();
    this.filename = path.basename(__filename);
  }

  async seed() {

    const systemParametersConfig = [
      {
        key: BaseSystemParameterKey.CMS_MENUS_MAX_LEVELS,
        value: "3",
        description: "max level of cms-menus allowed (depends on UI template)",
      },
      {
        key: BaseSystemParameterKey.CMS_DEFAULT_HOME_PAGE,
        value: "/dashboard",
        description: "default homepage ",
      },
    ];

    await Promise.all(systemParametersConfig.map(async config => this.createSystemParameter(config)));
  }

  async createSystemParameter(props) {
    const systemParameter = new SystemParameter();
    systemParameter.key = props.key;
    systemParameter.value = props.value;
    systemParameter.description = props.description;
    await systemParameter.save();
    return systemParameter;
  }

}

module.exports = SystemParameterSeeder;
