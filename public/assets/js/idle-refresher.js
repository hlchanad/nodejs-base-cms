const IdleRefresher = function (maxIdleSecond, step) {

    this.idleTime = 0;
    this.maxIdleSecond = maxIdleSecond || 1800000;
    this.step = step || 60000;

    this.tick = () => {
        console.log("idled", this.idleTime);

        this.idleTime = this.idleTime + this.step;

        if (this.idleTime > this.maxIdleSecond) {
            window.location.reload();
        }
    };

    this.resetIdle = (event) => {
        this.idleTime = 0;
    };

    this.start = () => {
        this.idleInterval = setInterval(this.tick, this.step);

        // Zero the idle timer on mouse / key event
        $(document).mousemove(this.resetIdle);
        $(document).keypress(this.resetIdle);
    };
};