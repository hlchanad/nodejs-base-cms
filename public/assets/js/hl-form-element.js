const HlFormElement = function () {

  this.initFormElements = function () {

    this.initDatePicker();
    this.initTimePicker();
    this.initDateTimePicker();
    this.initHTMLEditor();
    this.initUploadFileButton();
    this.initDropzone();
    this.initMultiSelectSortable();
  };

  this.initDatePicker = function () {
    $("input.hl-datepicker").each((index, element) => {
      const max = $(element).attr("max");
      const min = $(element).attr("min");

      const options = {
        singleDatePicker: true,
        autoApply: true,
        locale: {
          format: "YYYY-MM-DD",
          cancelLabel: "Clear",
        },
      };

      if (max != null) options.maxDate = max;
      if (min != null) options.minDate = min;

      $(element).daterangepicker(options);

      $(element).on("cancel.daterangepicker", function (ev, picker) {
        $(element).val("");
      });
    });
  };

  this.initTimePicker = function () {
    $("input.hl-timepicker").timepicker({
      showMeridian: false,
      minuteStep: 1,
    });
  };

  this.initDateTimePicker = function () {
    $("input.hl-datetimepicker").daterangepicker({
      singleDatePicker: true,
      autoApply: true,
      timePicker: true,
      timePicker24Hour: true,
      locale: {
        format: "YYYY-MM-DD HH:mm:ss",
      },
    });
  };

  this.initHTMLEditor = function () {
    $("div.hl-html-editor").summernote({
      height: 200,
      onfocus: function (e) {
        $("body").addClass("overlay-disabled");
      },
      onblur: function (e) {
        $("body").removeClass("overlay-disabled");
      }
    });
  };

  this.initUploadFileButton = function () {
    $("input[type='file']").change(function (event) {
      const inputId = $(this).attr("id");
      const value = $(this).val();
      const filename = value.substr("C:\\fakepath\\".length);

      $(`div.upload-image-file-name[data-id='${inputId}']`).text(filename);

      if (value) {
        $(`button.cancel-upload-image-button[data-id='${inputId}']`).show();

        if (this.files && this.files[0]) {
          const reader = new FileReader();

          reader.onload = function(e) {
            $(`img.upload-image-preview[data-id='${inputId}']`).attr("src", e.target.result).show();
          };

          reader.readAsDataURL(this.files[0]);
        }

        $(`img.upload-image-preview[data-id='${inputId}']`).show();
      } else {
        $(`button.cancel-upload-image-button[data-id='${inputId}']`).hide();
        $(`img.upload-image-preview[data-id='${inputId}']`).attr("src", "#").hide();
      }

      $(`input[name='${inputId}_old']`).val("");
    });

    $(`button.cancel-upload-image-button`).click(function (event) {
      const inputId = $(this).attr("data-id");
      $(`input[name='${inputId}_old']`).val("");
      $(`input[id=${inputId}]`).val("").trigger("change");
    });
  };

  this.initDropzone = function () {
    $("div.images-dropzone").each((index, element) => {
      const id = $(element).attr("data-id");
      const dropzoneId = $(element).attr("id");

      const dropzone = new Dropzone(`div#${dropzoneId}`, {
        paramName: "image",
        url: $(element).attr("data-action"),
        acceptedFiles: "image/*",
        addRemoveLinks: true,
        resizeMethod: "contain",
        thumbnailMethod: "contain",
        init: function () {
          $(`div.image-dropzone-values[data-id="${id}"]`).children()
            .toArray()
            .map(element => {
              return {
                imageId: $(element).attr("value"),
                name: $(element).attr("data-name"),
                size: $(element).attr("data-size"),
                url: $(element).attr("data-url"),
              };
            })
            .forEach(mockFile => {
              this.files.push(mockFile);
              this.displayExistingFile(mockFile, mockFile.url);
            });
        },
        sending: (file, xhr, formData) => {
          xhr.setRequestHeader($(`meta[name="_csrf_header"]`).attr("content"), $(`meta[name="_csrf"]`).attr("content"));
          formData.append("folder", $(element).attr("data-folder"));
          formData.append("isPublic", $(element).attr("data-is-public") === "true");
        },
        error: function (file, message) {
          $(file.previewElement).addClass("dz-error").find(".dz-error-message").text(message.status.message);
        },
      });

      dropzone.on("success", function (file, response) {
        const imageId = response.data.imageId;

        $(`div.image-dropzone-values[data-id="${id}"]`)
          .append(`<input type="hidden" name="${id}[]" value="${imageId}">`);
      });

      dropzone.on("removedfile", function (file) {
        const imageId = file.imageId || JSON.parse(file.xhr.response).data.imageId;

        $(`input[name="${id}[]"][value="${imageId}"]`).remove();
      });
    });
  };

  this.initMultiSelectSortable = function () {
    $(".multi-select-sortable.enabled").nestable({
      maxDepth: 1,
    })
      .on("change", function (e) {
        const id = $(e.target).attr("data-id");

        // update input values
        $(`input[id='${id}']`).val($(`.multi-select-sortable.enabled.selected[data-id=${id}]`)
          .nestable("serialize")
          .map(datum => datum.id)
          .filter(value => value != null && ("" + value).trim() !== "")
          .join(","));

        // move placeholder to the bottom
        $(`.multi-select-sortable.enabled[data-id='${id}']`).each((index, nestable) => {
          const placeholder = $(nestable).find("li.dd-placeholder");
          if (!placeholder.is(":last-child")) {
            placeholder.remove();
            $(nestable).find("ol.dd-list").append(placeholder);
          }
        });
      });
  };

};
