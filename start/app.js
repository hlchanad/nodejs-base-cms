'use strict';

const path = require('path');

/*
|--------------------------------------------------------------------------
| Providers
|--------------------------------------------------------------------------
|
| Providers are building blocks for your Adonis app. Anytime you install
| a new Adonis specific package, chances are you will register the
| provider here.
|
*/
const providers = [
  '@adonisjs/framework/providers/AppProvider',
  '@adonisjs/framework/providers/ViewProvider',
  '@adonisjs/lucid/providers/LucidProvider',
  '@adonisjs/bodyparser/providers/BodyParserProvider',
  '@adonisjs/cors/providers/CorsProvider',
  '@adonisjs/shield/providers/ShieldProvider',
  '@adonisjs/redis/providers/RedisProvider',
  '@adonisjs/session/providers/SessionProvider',
  '@adonisjs/auth/providers/AuthProvider',
  '@adonisjs/antl/providers/AntlProvider',
  '@adonisjs/validator/providers/ValidatorProvider',
  '@adonisjs/drive/providers/DriveProvider',
  'adonis-context/providers/ContextProvider',
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'ExceptionHandlerProvider'),
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'EdgeGeneralUtilProvider'),
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'EdgeLanguagesProvider'),
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'EdgeShowProvider'),
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'EdgeFormProvider'),
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'CustomValidatorProvider'),
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'ArrayExtensionProvider'),
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'LoggerDriverProvider'),
  path.join(__dirname, '..', 'app', 'Base', 'Providers', 'LoggerProvider'),
];

/*
|--------------------------------------------------------------------------
| Ace Providers
|--------------------------------------------------------------------------
|
| Ace providers are required only when running ace commands. For example
| Providers for migrations, tests etc.
|
*/
const aceProviders = [
  '@adonisjs/lucid/providers/MigrationsProvider'
];

/*
|--------------------------------------------------------------------------
| Aliases
|--------------------------------------------------------------------------
|
| Aliases are short unique names for IoC container bindings. You are free
| to create your own aliases.
|
| For example:
|   { Route: 'Adonis/Src/Route' }
|
*/
const aliases = {};

/*
|--------------------------------------------------------------------------
| Commands
|--------------------------------------------------------------------------
|
| Here you store ace commands for your package
|
*/
const commands = [
  'App/Base/Commands/MakeCrud',
  'App/Base/Commands/MakeLocales',
  'App/Base/Commands/StartUp',
  'App/Base/Commands/UpdateLocale',
];

module.exports = { providers, aceProviders, aliases, commands };
