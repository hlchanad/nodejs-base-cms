'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');
const Env = use('Env');

Route.group(() => {

  // Base
  Route.get('/login', 'App/Base/Controllers/Http/LoginController.index');
  Route.post('/login', 'App/Base/Controllers/Http/LoginController.login').as('login');
  Route.post('/logout', 'App/Base/Controllers/Http/LoginController.logout').as('logout');

  Route.get('/health-check', () => "OK");

})
  // .prefix(Env.getOrFail("APP_CONTEXT_PATH"))
  .middleware([
    'requestId',
    'requestLog',
    'saveLastRoute',
  ])
;

Route.group(() => {

  // Base
  Route.get('/', 'App/Base/Controllers/Http/IndexController.index');

  Route.get('/dashboard', 'App/Base/Controllers/Http/DashboardController.index');

  Route.resource('/cms-permissions', 'App/Base/Controllers/Http/CmsPermissionController');
  Route.post('/cms-permissions/delete-requests', 'App/Base/Controllers/Http/CmsPermissionController.batchDelete');

  Route.resource('/cms-roles', 'App/Base/Controllers/Http/CmsRoleController');
  Route.post('/cms-roles/delete-requests', 'App/Base/Controllers/Http/CmsRoleController.batchDelete');

  Route.resource('/cms-users', 'App/Base/Controllers/Http/CmsUserController');
  Route.post('/cms-users/delete-requests', 'App/Base/Controllers/Http/CmsUserController.batchDelete');
  Route.put('/cms-users/:id/preferences', 'App/Base/Controllers/Http/CmsUserController.updatePreference');

  Route.resource('/cms-menus', 'App/Base/Controllers/Http/CmsMenuController');
  Route.post('/cms-menus/delete-requests', 'App/Base/Controllers/Http/CmsMenuController.batchDelete');

  Route.resource('/system-parameters', 'App/Base/Controllers/Http/SystemParameterController').except(['create', 'store', 'destroy']);

  Route.resource('/locales', 'App/Base/Controllers/Http/LocaleController').except(['create', 'store', 'destroy']);
  Route.get('/locale-jsons/:library', 'App/Base/Controllers/Http/LocaleController.getByLibrary');

  Route.post('/images', 'App/Base/Controllers/Http/ImageController.store');

  Route.resource('/articles', 'ArticleController');
  Route.post('/articles/delete-requests', 'ArticleController.batchDelete');

})
  // .prefix(Env.getOrFail("APP_CONTEXT_PATH"))
  .middleware([
    'requestId',
    'requestLog',
    'saveLastRoute',
    'auth',
    'rolePermission',
    'cmsUserPreferences',
    'breadcrumb',
    'sidebarMenu',
  ])
;
